package com.gridnev;

/**
 * Created by a.gridnev on 31/03/2016.
 * https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/
 */
public class BestTimetoBuyandSellStockII {
    public int maxProfit(int[] prices) {

        if (prices.length == 0 || prices.length == 1) {
            return 0;
        }

        Integer[] cache = new Integer[prices.length];
        return getMax(prices, 0, cache);
    }

    private int getMax(int[] prices, int startIndex, Integer[] maxCache) {
        if (startIndex >= prices.length - 1) {
            return 0;
        }
        if (maxCache[startIndex] != null) {
            return maxCache[startIndex].intValue();
        }
        if (startIndex == prices.length - 2) {

            int result = prices[startIndex + 1] - prices[startIndex];
            maxCache[startIndex] = result;
            return result;
        }

        if (startIndex == prices.length - 3) {

            int result1 = prices[startIndex + 1] - prices[startIndex];
            int result2 = prices[startIndex + 2] - prices[startIndex];
            int result3 = prices[startIndex + 2] - prices[startIndex + 1];

            int maxOfThree = Math.max(result1, Math.max(result2, result3));
            maxCache[startIndex] = maxOfThree;
            return maxOfThree;
        }

        int max = Integer.MIN_VALUE;

        for (int i = startIndex + 1; i < prices.length; i++) {

            int firstBuySell = prices[i] - prices[startIndex];
            int totalSum = firstBuySell + getMax(prices, i + 1, maxCache);
            if (totalSum > max) {
                max = totalSum;
            }
        }
        maxCache[startIndex] = max;
        return max;
    }
}
