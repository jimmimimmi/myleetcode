package com.gridnev;

/**
 * Created by a.gridnev on 01/04/2016.
 */
public class BestTimetoBuyandSellStockII2 {
    public int maxProfit(int[] prices) {

        int length = prices.length;
        if (length == 0 || length == 1) {
            return 0;
        }

        if (length == 2) {
            return Math.max(prices[1] - prices[0], 0);
        }

        int[] maxCache = new int[length];
        maxCache[length - 1] = 0;
        maxCache[length - 2] = Math.max(0, prices[length - 1] - prices[length - 2]);

        int globalMax = maxCache[length - 2];
        for (int startIndex = length - 3; startIndex >= 0; startIndex--) {

            if (startIndex == length - 3) {

                int result1 = prices[startIndex + 1] - prices[startIndex];
                int result2 = prices[startIndex + 2] - prices[startIndex];
                int result3 = prices[startIndex + 2] - prices[startIndex + 1];

                int maxOfThree = Math.max(result1, Math.max(result2, result3));
                maxCache[startIndex] = Math.max(0, maxOfThree);
                globalMax = Math.max(globalMax, maxCache[startIndex]);
                continue;
            }

            int max = Integer.MIN_VALUE;
            for (int secondIndex = length - 1; secondIndex > startIndex; secondIndex--) {
                int firstPart = prices[secondIndex] - prices[startIndex];
                int secondPart = 0;
                if (secondIndex < length - 2) {
                    secondPart = maxCache[secondIndex + 1];
                }
                max = Math.max(max, firstPart + secondPart);
            }
            maxCache[startIndex] = Math.max(0, max);
            globalMax = Math.max(globalMax, maxCache[startIndex] );

        }
        return globalMax;

    }
}
