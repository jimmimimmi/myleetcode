package com.gridnev;

/**
 * Created by a.gridnev on 01/04/2016.
 */
public class BestTimetoBuyandSellStockII3 {
    public int maxProfit(int[] prices) {

        int length = prices.length;
        if (length == 0 || length == 1) {
            return 0;
        }

        int res = 0;

        for (int i = 1; i < prices.length; i++) {
            res += Math.max(0, prices[i] - prices[i - 1]);
        }
        return res;
    }
}

