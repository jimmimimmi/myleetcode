package com.gridnev;

/**
 * Created by a.gridnev on 21/04/2016.
 */
public class BestTimetoBuyandSellStockwithCooldown {

    private Integer[][] cacheWithCooldown;
    private Integer[] cacheWithoutCooldown;
    private int[] prices;

    public int maxProfit(int[] prices) {
        this.prices = prices;
        cacheWithCooldown = new Integer[prices.length][prices.length];
        cacheWithoutCooldown = new Integer[prices.length];
        return maxProfit(0, null);
    }


    private Integer getCache(int begin, Integer forbiddenDay) {
        if (forbiddenDay == null) {
            return cacheWithoutCooldown[begin];
        }
        return cacheWithCooldown[begin][forbiddenDay.intValue()];
    }


    private void putCache(int result, int begin, Integer forbiddenDay) {
        if (forbiddenDay == null) {
            cacheWithoutCooldown[begin] = result;
            return;
        }
        cacheWithCooldown[begin][forbiddenDay.intValue()] = result;
    }

    private int maxProfit(int begin, Integer forbiddenDay) {

        Integer cache = getCache(begin, forbiddenDay);

        if (cache != null) return cache.intValue();


        if (begin == prices.length - 1) {
            putCache(0, begin, forbiddenDay);
            return 0;
        }

        if (begin == prices.length - 2) {
            if (forbiddenDay == null || forbiddenDay.intValue() < begin) {
                int maxOfLastTwo = Math.max(0, prices[begin + 1] - prices[begin]);
                putCache(maxOfLastTwo, begin, null);
                return maxOfLastTwo;
            }

            putCache(0, begin, forbiddenDay);
            return 0;

        }
        int buyTodaySellTomorrow = prices[begin + 1] - prices[begin];
        if (begin == prices.length - 3) {
            int buyTodaySellAfterTomorrow = prices[begin + 2] - prices[begin];
            int buyTomorrowSellAfterTomorrow = prices[begin + 2] - prices[begin + 1];
            if (forbiddenDay == null || forbiddenDay.intValue() < begin) {
                int result = Math.max(buyTodaySellAfterTomorrow, Math.max(buyTodaySellTomorrow, buyTomorrowSellAfterTomorrow));
                putCache(result, begin, null);
                return result;
            }
            if (forbiddenDay.intValue() == begin) {
                int result = Math.max(0, buyTomorrowSellAfterTomorrow);
                putCache(result, begin, forbiddenDay);
                return result;
            }

            if (forbiddenDay.intValue() == begin + 1) {
                int result = Math.max(0, buyTodaySellAfterTomorrow);
                putCache(result, begin, forbiddenDay);
                return result;
            }

            int result = Math.max(0, buyTodaySellTomorrow);
            putCache(result, begin, forbiddenDay);
            return result;
        }


        int sumForNextDaysWithoutCoolDown = maxProfit(begin + 1, null);


        if (buyTodaySellTomorrow < 0) {
            int result = Math.max(0, sumForNextDaysWithoutCoolDown);
            putCache(result, begin, forbiddenDay);
            return result;
        }

        if (forbiddenDay != null && forbiddenDay.intValue() == begin) {
            putCache(sumForNextDaysWithoutCoolDown, begin, forbiddenDay);
            return sumForNextDaysWithoutCoolDown;
        }
        int sumForNextDaysWithCoolDown = maxProfit(begin + 1, begin + 2);
        int result = Math.max(sumForNextDaysWithoutCoolDown, sumForNextDaysWithCoolDown + buyTodaySellTomorrow);
        putCache(result, begin, forbiddenDay);
        return result;
    }
}
