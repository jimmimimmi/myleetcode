package com.gridnev.BinarySearch;

/**
 * Created by a.gridnev on 15/02/2016.
 */

public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode(int x) {
        val = x;
    }
}


