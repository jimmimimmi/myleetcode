package com.gridnev.BinarySearch;

public class TreeNodeGeneric<T> {
    public T val;
    public TreeNodeGeneric<T> left;
    public TreeNodeGeneric<T> right;

    public TreeNodeGeneric() {
        val = null;
    }

    public TreeNodeGeneric(T x) {
        val = x;
    }
}
