package com.gridnev;

import com.gridnev.BinarySearch.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by a.gridnev on 04/04/2016.
 */
public class BinaryTreeInorderTraversal {

    public List<Integer> inorderTraversal(TreeNode root) {

        ArrayList<Integer> result = new ArrayList<Integer>();

        LinkedList<TreeNode> queue = new LinkedList<TreeNode>();
        queue.add(root);
        while (!queue.isEmpty()) {
            TreeNode treeNode = queue.removeLast();
            if (treeNode != null) {

                if (treeNode.right != null) {
                    queue.add(treeNode.right);
                }

                result.add(treeNode.val);

                if (treeNode.left != null) {
                    queue.add(treeNode.left);
                }
            }
        }

        return result;

    }

}
