package com.gridnev;

import com.gridnev.BinarySearch.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by a.gridnev on 25/02/2016.
 */
public class BinaryTreeLevelOrderTraversal {
    public List<List<Integer>> levelOrder(TreeNode root) {


        ArrayList<List<Integer>> result = new ArrayList<List<Integer>>();
        if (root == null) {
            return result;
        }
        LinkedList<TreeNode> linkedList = new LinkedList<TreeNode>();

        linkedList.addFirst(root);

        while (!linkedList.isEmpty()) {
            ArrayList<Integer> integers = new ArrayList<Integer>();
            result.add(integers);
            int size = linkedList.size();
            for (int i = 0; i < size; i++) {
                TreeNode treeNode = linkedList.removeFirst();
                integers.add(treeNode.val);
                if (treeNode.left != null) linkedList.add(treeNode.left);
                if (treeNode.right != null) linkedList.add(treeNode.right);
            }
        }
        return result;
    }
}
