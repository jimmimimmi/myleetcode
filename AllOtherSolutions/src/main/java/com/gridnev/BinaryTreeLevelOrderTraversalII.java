package com.gridnev;

import com.gridnev.BinarySearch.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by a.gridnev on 24/02/2016.
 * https://leetcode.com/problems/binary-tree-level-order-traversal-ii/
 */
public class BinaryTreeLevelOrderTraversalII {
    public List<List<Integer>> levelOrderBottom(TreeNode root) {

        ArrayList<List<Integer>> result = new ArrayList<List<Integer>>();
        LinkedList<TreeNode> queue = new LinkedList<TreeNode>();

        if (root == null)
            return result;
        queue.add(root);

        while (queue.isEmpty() == false) {
            int size = queue.size();
            List<Integer> integers = new ArrayList<Integer>(size);
            result.add(integers);
            for (int i = 0; i < size; i++) {
                TreeNode treeNode = queue.removeFirst();
                if (treeNode.left != null)
                    queue.add(treeNode.left);

                if (treeNode.right != null)
                    queue.add(treeNode.right);
                integers.add(treeNode.val);
            }
        }

        ArrayList<List<Integer>> reverseResult = new ArrayList<List<Integer>>();

        for (int i = result.size() - 1; i >= 0; i--) {
            reverseResult.add(result.get(i));
        }
        return reverseResult;
    }
}
