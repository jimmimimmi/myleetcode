package com.gridnev;

import com.gridnev.BinarySearch.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by a.gridnev on 04/03/2016.
 * https://leetcode.com/problems/binary-tree-paths/
 */
public class BinaryTreePaths {
    public List<String> binaryTreePaths(TreeNode root) {
        ArrayList<String> strings = new ArrayList<String>();

        if (root == null) {
            return strings;
        }

        print(root, "", strings);
        return strings;
    }


    private void print(TreeNode node, String previousPath, ArrayList<String> arrayList) {
        String s = previousPath + node.val;
        if (node.left == null && node.right == null) {
            arrayList.add(s);
            return;
        }

        if (node.left != null) {
            print(node.left, s + "->", arrayList);
        }

        if (node.right != null) {
            print(node.right, s + "->", arrayList);
        }
    }


}
