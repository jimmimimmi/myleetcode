package com.gridnev;

import com.gridnev.BinarySearch.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by a.gridnev on 04/04/2016.
 */
public class BinaryTreePreorderTraversal {
    public List<Integer> preorderTraversal(TreeNode root) {

        ArrayList<Integer> result = new ArrayList<Integer>();
        put(root,result);
        return result;

    }

    private void put(TreeNode node, List<Integer> result){
        if(node==null)
            return;

        put(node.left, result);

        result.add(node.val);

        put(node.right, result);
    }
}
