package com.gridnev;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by a.gridnev on 19/01/2016.
 * https://leetcode.com/problems/bulls-and-cows/
 */
public class BullsAndCows {

    public String getHint(String secret, String guess) {
        final HashMap<Character, Integer> amountOfOccuranciesForSecret = new HashMap<Character, Integer>();
        final HashMap<Character, Integer> amountOfOccuranciesForGuess = new HashMap<Character, Integer>();

        int amountOfBulls = 0;

        for (int i = 0; i < secret.length(); i++) {
            final char secretChar = secret.charAt(i);
            final char guessChar = guess.charAt(i);
            if (secretChar == guessChar) {
                amountOfBulls++;
            } else {
                amountOfOccuranciesForSecret
                        .put(secretChar,
                                amountOfOccuranciesForSecret.containsKey(secretChar) ?
                                        amountOfOccuranciesForSecret.get(secretChar) + 1 :
                                        1);
                amountOfOccuranciesForGuess
                        .put(guessChar,
                                amountOfOccuranciesForGuess.containsKey(guessChar) ?
                                        amountOfOccuranciesForGuess.get(guessChar) + 1 :
                                        1);
            }
        }

        int amountOfCows = 0;

        for (Map.Entry<Character, Integer> secretEntry : amountOfOccuranciesForSecret.entrySet()) {
            final Character key = secretEntry.getKey();

            if (amountOfOccuranciesForGuess.containsKey(key)) {
                final Integer guessValue = amountOfOccuranciesForGuess.get(key);
                final Integer secretValue = secretEntry.getValue();
                amountOfCows += (secretValue <= guessValue ? secretValue : guessValue);
            }
        }

        return Integer.toString(amountOfBulls) + "A" + Integer.toString(amountOfCows) + "B";

    }
}
