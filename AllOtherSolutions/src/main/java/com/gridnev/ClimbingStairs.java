package com.gridnev;

/**
 * Created by a.gridnev on 23/02/2016.
 */
public class ClimbingStairs {

    public int climbStairs(int n) {

        if (n == 0) return 0;
        if (n == 1) return 1;
        if (n == 2) return 2;

        int previous = 2;
        int prevPrevious = 1;
        int result = 0;

        for (int i = 3; i <= n; i++) {
            result =  previous + prevPrevious;
            prevPrevious = previous;
            previous = result;
        }

        return result;


    }
}
