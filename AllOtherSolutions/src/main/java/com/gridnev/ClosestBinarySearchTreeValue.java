package com.gridnev;

import com.gridnev.BinarySearch.TreeNode;

/**
 * Created by a.gridnev on 15/02/2016.
 * https://leetcode.com/submissions/detail/53516129/
 */
public class ClosestBinarySearchTreeValue {
    public int closestValue(TreeNode root, double target) {
        TreeNode current = root;
        int bestValue = current.val;
        double bestDist = Math.abs(target - bestValue);
        while (current != null) {


            if (target == current.val) {
                return current.val;
            }
            double dist = Math.abs(target - current.val);
            if (dist < bestDist) {
                bestDist = dist;
                bestValue = current.val;
            }

            if (target > current.val) {
                current = current.right;
            } else {
                current = current.left;
            }
        }
        return bestValue;
    }
}
