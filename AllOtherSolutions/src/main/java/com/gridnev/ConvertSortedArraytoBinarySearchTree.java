package com.gridnev;

import com.gridnev.BinarySearch.TreeNode;

/**
 * Created by a.gridnev on 08/04/2016.
 * https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/
 */
public class ConvertSortedArraytoBinarySearchTree {
    public TreeNode sortedArrayToBST(int[] nums) {

        if (nums == null || nums.length == 0) {
            return null;
        }

        if (nums.length == 1) {
            return new TreeNode(nums[0]);
        }

        return getNode(nums, 0, nums.length - 1);
    }


    private TreeNode getNode(int[] nums, int begin, int end) {
        if (begin < 0 || end < 0 ||
                end > nums.length - 1 || begin > nums.length - 1)
            return null;


        int middle = begin + (end - begin) / 2;

        TreeNode treeNode = new TreeNode(nums[middle]);

        if (middle - 1 >= begin)
            treeNode.left = getNode(nums, begin, middle - 1);
        if (middle + 1 <= end)
            treeNode.right = getNode(nums, middle + 1, end);

        return treeNode;
    }

}
