package com.gridnev;

/**
 * Created by a.gridnev on 26/02/2016.
 */
public class CountAndSay {
    public String countAndSay(int n) {
        if (n < 1) {
            return "";
        }

        String result = "1";
        if (n == 1) {
            return result;
        }
        result = "11";

        if (n == 2) {
            return result;
        }

        result = "11";

        if (n == 3) {
            return result;
        }

        result = "1211";

        if (n == 4) {
            return result;
        }

        for (int i = 3; i <= n; i++) {
            String newWord = "";
            Character currentChar = result.charAt(0);
            int currentCharCount = 1;

            for (int j = 1; j < result.length(); j++) {
                if (result.charAt(j) == currentChar) {
                    currentCharCount++;
                } else {
                    newWord += "" + currentCharCount + currentChar;
                    currentChar = result.charAt(j);
                    currentCharCount = 1;
                }

                if ( j == result.length()-1){
                    newWord += "" + currentCharCount + currentChar;
                }
            }
            result = newWord;
        }

        return result;
    }
}
