package com.gridnev;

/**
 * Created by Alena.cy on 23.01.2016.
 */
public class CountPrimes {

    public int countPrimes(int n) {

        if(n<=2){
            return 0;
        }


        if(n==3){
            return 1;
        }

        if(n==4 || n==5){
            return 2;
        }

        boolean[] markedNumbersArray = new boolean[n];


        for (int i = 2; i <= (n -1) /2; i++) {
            if (markedNumbersArray[i] == true) {
                continue;
            }
            for (int j = 2; i * j <= (n-1); j++) {
                int index = i * j;
                markedNumbersArray[index] = true;
            }
        }
        int amount = 2;

        for (int i = 5; i <=n-1 ; i+=2) {
            if(markedNumbersArray[i]==false){
                amount++;
            }
        }

        return amount;

    }
}
