package com.gridnev;

/**
 * Created by a.gridnev on 24/03/2016.
 * https://leetcode.com/problems/counting-bits/
 */
public class CountingBits {
    public int[] countBits(int num) {

        int[] ints = new int[num + 1];
        if (num == 0) return ints;
        ints[1] = 1;
        if (num == 1) {
            return ints;
        }
        ints[2] = 1;
        if (num == 2) {
            return ints;
        }

        int currentPowOf2 = 2;

        for (int i = 3; i <= num; i++) {
            if (i == currentPowOf2 * 2) {
                currentPowOf2 = i;
                ints[i] = 1;
            } else {
                ints[i] = 1 + ints[i - currentPowOf2];
            }
        }
        return ints;
    }
}
