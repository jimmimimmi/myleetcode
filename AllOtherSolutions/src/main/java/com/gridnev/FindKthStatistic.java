package com.gridnev;

public class FindKthStatistic {
    public int findKthLargest(int[] array, int k) {
        return getKthElement(array, array.length - k);
    }

    public int getKthElement(int[] array, int k) {
        return getKthElement(array, k, 0, array.length - 1);
    }

    private int getKthElement(int[] array, int k, int left, int right) {

        int partition = partition(array, left, right);
        if (partition - left + 1 == k) {
            return array[partition];
        }

        if (partition - left + 1 > k) {
            return getKthElement(array, k, left, partition - 1);
        }

        return getKthElement(array, k - (partition - left + 1), partition + 1, right);
    }

    private int partition(int[] array, int left, int right) {
        int pivotValue = array[right];
        int pivotIndex = left;

        for (int j = left; j <= right - 1; j++) {
            if (array[j] <= pivotValue) {
                swap(array, j, pivotIndex);
                pivotIndex++;
            }
        }
        swap(array, right, pivotIndex);
        return pivotIndex;
    }

    private static void swap(int[] array, int index1, int index2) {
        int temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }

}
