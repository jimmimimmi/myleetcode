package com.gridnev;

import java.util.Arrays;

/*
https://leetcode.com/problems/median-of-two-sorted-arrays/description/
There are two sorted arrays nums1 and nums2 of size m and n respectively.

Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).

 */
public class FindMedianSortedArrays {
    public static double findMedianSortedArrays(int[] array1, int[] array2) {
        int rightPart = getKth(array1, 0, array1.length - 1, array2, 0, array2.length - 1, (array1.length + array2.length + 2) / 2);
        int leftPart = getKth(array1, 0, array1.length - 1, array2, 0, array2.length - 1, (array1.length + array2.length + 1) / 2);
        return (leftPart + rightPart) / 2.0;
    }

    public static double findMedianUsingSorting(int[] array1, int[] array2) {
        int totalLength = array1.length + array2.length;
        int[] result = new int[totalLength];

        int index = 0;
        for (int i = 0; i < array1.length; i++) {
            result[index] = array1[i];
            index++;
        }
        for (int i = 0; i < array2.length; i++) {
            result[index] = array2[i];
            index++;
        }


        Arrays.sort(result);

        return (result[(totalLength - 1) / 2] + result[(totalLength) / 2]) / 2.0;
    }

    private static int getKth(int[] array1, int start1, int end1, int[] array2, int start2, int end2, int k) {
        int length1 = end1 - start1 + 1;
        int length2 = end2 - start2 + 1;

        if (length1 > length2) {
            return getKth(array2, start2, end2, array1, start1, end1, k);
        }

        if (length1 == 0) {
            return array2[start2 + k - 1];
        }

        if (k == 1) {
            return Math.min(array1[start1], array2[start2]);
        }

        int pivot1 =
                start1 + k / 2 - 1;
        if (pivot1 > end1) {
            pivot1 = end1;
        }

        int pivot2 = start2 + k / 2 - 1;
        if (pivot2 > end2) {
            pivot2 = end2;
        }

        if (array1[pivot1] > array2[pivot2]) {
            return getKth(array1, start1, end1, array2, pivot2 + 1, end2, k - (pivot2 - start2 + 1));
        }
        return getKth(array1, pivot1 + 1, end1, array2, start2, end2, k - (pivot1 - start1 + 1));
    }
}
