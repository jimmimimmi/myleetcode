package com.gridnev;

/**
 * Created by a.gridnev on 15/02/2016.
 * https://leetcode.com/problems/first-bad-version/
 */
public class FirstBadVersion {
    boolean isBadVersion(int version) {
        return version >= 1;
    }

    public int firstBadVersion(int n) {

        int left = 1;
        int right = n;


        while (left < right) {
            int middle = left + (right - left) / 2;
            // if(middle==left)return left;
            boolean badVersion = isBadVersion(middle);

            if (badVersion) {
                right = middle;
            } else {
                left = middle + 1;
            }

        }

        return left;
    }
}
