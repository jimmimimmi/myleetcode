package com.gridnev;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by a.gridnev on 15/02/2016.
 */
public class FlipGame {
    public List<String> generatePossibleNextMoves(String s) {
        ArrayList<String> strings = new ArrayList<String>();
        if (s == null || s.length() <= 1) {
            return strings;
        }

        for (int i = 0; i < s.length() - 1; i++) {
            if (s.charAt(i) == s.charAt(i + 1) && s.charAt(i + 1) == '+') {
                if (i == 0) {
                    {
                        String substring = s.substring(2);
                        strings.add("--" + substring);
                    }
                } else if (i == s.length() - 2) {
                    {
                        String substring = s.substring(0, i);
                        strings.add(substring + "--");
                    }
                } else {
                    String substring = s.substring(0, i);
                    String substring1 = s.substring(i + 2, s.length());
                    strings.add(substring  + "--" + substring1);
                }

            }
        }

        return strings;

    }
}
