package com.gridnev;

import java.util.HashMap;

/**
 * Created by a.gridnev on 24/02/2016.
 */
public class HouseRobber {


    public int rob(int[] nums) {
        if (nums == null || nums.length == 0)
            return 0;
        if (nums.length == 1)
            return nums[0];

        if (nums.length == 2) {
            return Math.max(nums[0], nums[1]);
        }


        int minusTwo = nums[0];
        int minusOne = Math.max(nums[0], nums[1]);
        int sum = 0;

        for (int i = 2; i < nums.length; i++) {
            sum = Math.max(minusTwo + nums[i], minusOne);
            minusTwo = minusOne;
            minusOne = sum;
        }

        return sum;
    }


}
