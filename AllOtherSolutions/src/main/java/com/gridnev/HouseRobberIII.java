package com.gridnev;

import com.gridnev.BinarySearch.TreeNode;

import java.util.HashMap;

/**
 * Created by a.gridnev on 12/04/2016.
 * https://leetcode.com/problems/house-robber-iii/
 */
public class HouseRobberIII {
    public int rob(TreeNode root) {
        HashMap<TreeNode, Integer> treeNodeIntegerHashMap = new HashMap<TreeNode, Integer>();
        return getMax(root, treeNodeIntegerHashMap);
    }

    private int getMax(TreeNode node, HashMap<TreeNode, Integer> treeNodeIntegerHashMap) {
        if (node == null) return 0;

        if (treeNodeIntegerHashMap.containsKey(node)) {
            return treeNodeIntegerHashMap.get(node);
        }

        int currentSum = node.val;
        int childSum = 0;


        if (node.left != null) {
            childSum += getMax(node.left,treeNodeIntegerHashMap);
            currentSum += getMax(node.left.left,treeNodeIntegerHashMap);
            currentSum += getMax(node.left.right,treeNodeIntegerHashMap);
        }

        if (node.right != null) {
            childSum += getMax(node.right,treeNodeIntegerHashMap);
            currentSum += getMax(node.right.left,treeNodeIntegerHashMap);
            currentSum += getMax(node.right.right,treeNodeIntegerHashMap);
        }

        int max = Math.max(currentSum, childSum);
        treeNodeIntegerHashMap.put(node, max);
        return max;

    }
}
