package com.gridnev;

import java.util.LinkedList;
import java.util.Queue;


public class ImplementStackUsingQueues {
    class MyStack {

        private Queue<Integer> mainQueue;
        private Queue<Integer> assistantQueue;

        /**
         * Initialize your data structure here.
         */
        public MyStack() {

            mainQueue = new LinkedList<Integer>();
            assistantQueue = new LinkedList<Integer>();
        }

        /**
         * Push element x onto stack.
         */
        public void push(int x) {
            mainQueue.add(x);
        }

        /**
         * Removes the element on top of the stack and returns that element.
         */
        public int pop() {

            while (true) {
                Integer head = mainQueue.poll();
                if (!mainQueue.isEmpty()) {
                    assistantQueue.add(head);
                } else {
                    Queue<Integer> temp = this.mainQueue;
                    mainQueue = assistantQueue;
                    assistantQueue = temp;
                    return head;
                }
            }
        }

        /**
         * Get the top element.
         */
        public int top() {
            while (true) {
                Integer head = mainQueue.poll();
                if (!mainQueue.isEmpty()) {
                    assistantQueue.add(head);
                } else {
                    assistantQueue.add(head);
                    Queue<Integer> temp = this.mainQueue;
                    mainQueue = assistantQueue;
                    assistantQueue = temp;
                    return head;
                }
            }
        }

        /**
         * Returns whether the stack is empty.
         */
        public boolean empty() {
            return mainQueue.isEmpty();
        }
    }

}
