package com.gridnev;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

public class IntersectionOfTwoArrays {
    public int[] intersection(int[] nums1, int[] nums2) {

        Arrays.sort(nums1);

        HashSet<Integer> resultSet = new HashSet<Integer>();

        int[] minArray;
        int[] maxArray;

        if (nums1.length < nums2.length) {
            minArray = nums1;
            maxArray = nums2;
        } else {
            maxArray = nums1;
            minArray = nums2;
        }

        HashSet<Integer> minSet = new HashSet<Integer>();
        for (int i = 0; i < minArray.length; i++) {
            int i1 = minArray[i];
            minSet.add(i1);
        }

        for (int i = 0; i < maxArray.length; i++) {
            int i1 = maxArray[i];
            if (minSet.contains(i1)) {
                resultSet.add(i1);
            }
        }


        int[] result = new int[resultSet.size()];
        int index = 0;
        Iterator<Integer> iterator = resultSet.iterator();
        while (iterator.hasNext()) {
            result[index] = iterator.next();
            index++;
        }


        return result;
    }
}
