package com.gridnev;

/**
 * Created by a.gridnev on 14/12/2016.
 */
public class IslandPerimeter {


    public int islandPerimeter(int[][] grid) {

        int rowAmount = grid.length;
        int columnAmount = grid[0].length;

        boolean[][] visitedCells = new boolean[rowAmount][columnAmount];
        int[] perimeter = new int[1];

        int row = -1;
        int col = -1;
        for (int i = 0; i < rowAmount; i++) {
            {
                for (int j = 0; j < columnAmount; j++) {
                    if (grid[i][j] == 1) {
                        row = i;
                        col = j;
                        break;
                    }
                }
                if (row != -1) {
                    break;
                }
            }
        }
        if (row != -1) {
            count(grid, visitedCells, perimeter, row, col);
            return perimeter[0];
        }

        return 0;
    }

    private void count(int[][] grid, boolean[][] visitedCells, int[] perimeter, int currentCellRowIndex, int currentCellColumnIndex) {

        visitedCells[currentCellRowIndex][currentCellColumnIndex] = true;
        boolean thereCellDown = isThereCellDown(grid, currentCellRowIndex, currentCellColumnIndex);
        boolean thereCellUp = isThereCellUp(grid, currentCellRowIndex, currentCellColumnIndex);
        boolean thereCellLeft = isThereCellLeft(grid, currentCellRowIndex, currentCellColumnIndex);
        boolean thereCellRight = isThereCellRight(grid, currentCellRowIndex, currentCellColumnIndex);

        if (!thereCellDown) {
            perimeter[0]++;
        }
        if (!thereCellUp) {
            perimeter[0]++;
        }
        if (!thereCellLeft) {
            perimeter[0]++;
        }
        if (!thereCellRight) {
            perimeter[0]++;
        }


        int rowDown = currentCellRowIndex + 1;
        if (thereCellDown && !visitedCells[rowDown][currentCellColumnIndex]) {
            count(grid, visitedCells, perimeter, rowDown, currentCellColumnIndex);
        }


        int rowUp = currentCellRowIndex - 1;
        if (thereCellUp && !visitedCells[rowUp][currentCellColumnIndex]) {
            count(grid, visitedCells, perimeter, rowUp, currentCellColumnIndex);
        }

        int columnLeft = currentCellColumnIndex - 1;
        if (thereCellLeft && !visitedCells[currentCellRowIndex][columnLeft]) {
            count(grid, visitedCells, perimeter, currentCellRowIndex, columnLeft);
        }

        int columnRight = currentCellColumnIndex + 1;
        if (thereCellRight && !visitedCells[currentCellRowIndex][columnRight]) {
            count(grid, visitedCells, perimeter, currentCellRowIndex, columnRight);
        }
    }

    private boolean isThereCellRight(int[][] grid, int currentCellRowIndex, int currentCellColumnIndex) {
        int columnAmount = grid[0].length;
        return currentCellColumnIndex != columnAmount - 1 && grid[currentCellRowIndex][currentCellColumnIndex + 1] == 1;
    }

    private boolean isThereCellLeft(int[][] grid, int currentCellRowIndex, int currentCellColumnIndex) {
        return currentCellColumnIndex != 0 && grid[currentCellRowIndex][currentCellColumnIndex - 1] == 1;
    }

    private boolean isThereCellUp(int[][] grid, int currentCellRowIndex, int currentCellColumnIndex) {
        return currentCellRowIndex != 0 && grid[currentCellRowIndex - 1][currentCellColumnIndex] == 1;
    }

    private boolean isThereCellDown(int[][] grid, int currentCellRowIndex, int currentCellColumnIndex) {
        int rowAmount = grid.length;
        return currentCellRowIndex != rowAmount - 1 && grid[currentCellRowIndex + 1][currentCellColumnIndex] == 1;
    }


}
