package com.gridnev;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by Alena.cy on 23.01.2016.
 */
public class IsomorphicStrings {

    public boolean isIsomorphic(String s, String t) {

        HashMap<Character, Character> map1 = new HashMap<Character, Character>();

        HashSet<Character> characters = new HashSet<Character>();

        for (int i = 0; i < s.length(); i++) {
            char firstChar = s.charAt(i);
            char secondChar = t.charAt(i);

            if (map1.containsKey(firstChar)) {
                Character secondCharacrerFromMap = map1.get(firstChar);
                if (secondCharacrerFromMap != secondChar) {
                    return false;
                }
                continue;
            } else {

                if(characters.contains(secondChar)){
                   return false;
                }
                map1.put(firstChar, secondChar);
                characters.add(secondChar);
            }
        }
        return true;
    }
}
