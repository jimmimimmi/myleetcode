package com.gridnev;

import com.gridnev.BinarySearch.TreeNode;
import com.gridnev.BinarySearch.TreeNodeGeneric;

import java.util.ArrayList;

/**
 * Created by a.gridnev on 07/04/2016.
 */
public class KthSmallestElementinaBST {

    private int amount = 0;
    private int number = 0;


    public int kthSmallest(TreeNode root, int k) {
        amount = k;
        put(root);
        return number;
    }

    private void put(TreeNode treeNode) {
        if (treeNode == null) return;
        put(treeNode.left);
        amount--;
        if (amount == 0) {
            number = treeNode.val;
            return;
        }

        put(treeNode.right);
    }


}
