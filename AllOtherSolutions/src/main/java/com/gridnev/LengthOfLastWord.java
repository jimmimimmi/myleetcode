package com.gridnev;

/**
 * Created by a.gridnev on 04/03/2016.
 * https://leetcode.com/problems/length-of-last-word/
 */
public class LengthOfLastWord {

    public int lengthOfLastWord(String s) {

        if (s == null || s.length() == 0) return 0;
        int startIndex = s.length()-1;
        if (s.charAt(s.length() - 1) == ' ') {
            for (; startIndex >= 0; startIndex--) {
                if (s.charAt(startIndex) != ' ') break;
                if(startIndex==0) return 0;
            }
        }

        int count = 0;

        for (int i = startIndex; i >= 0; i--) {
            if (s.charAt(i) != ' ')
                count++;
            else break;
        }
        return count;
    }
}
