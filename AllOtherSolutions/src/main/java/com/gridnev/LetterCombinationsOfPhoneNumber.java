package com.gridnev;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class LetterCombinationsOfPhoneNumber {

    private static String[] MAPPING = new String[]{"0", "1", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};

    public static List<String> letterCombinations(String digits) {
        Queue<String> queue = new LinkedList<>();
        if (digits.isEmpty()) return new ArrayList<>(queue);

        queue.add("");
        for (int i = 0; i < digits.length(); i++) {
            int digit = Character.getNumericValue(digits.charAt(i));
            int size = queue.size();
            for (int j = 0; j < size; j++) {
                String currentPrefix = queue.remove();
                for (char s : MAPPING[digit].toCharArray())
                    queue.add(currentPrefix + s);

            }
        }

        return new ArrayList<>(queue);
    }
}
