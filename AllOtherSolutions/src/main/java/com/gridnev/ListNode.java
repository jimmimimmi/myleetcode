package com.gridnev;

/**
 * Created by Alena.cy on 24.01.2016.
 */


public class ListNode {
    public int val;
    public ListNode next;

    public ListNode(int x) {
        val = x;
    }


    @Override
    public String toString() {
        return Integer.toString(val);
    }
}
