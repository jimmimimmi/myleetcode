package com.gridnev;

/**
 * Created by a.gridnev on 04/03/2016.
 */
public class LongestCommonPrefix {
    public String longestCommonPrefix(String[] strs) {
        if(strs==null || strs.length==0)
            return "";

        if (strs.length == 1) {
            return strs[0];
        }

        int minLength = strs[0].length();
        int minIndex = 0;

        for (int i = 0; i < strs.length; i++) {
            if (strs[i].length() < minLength) {
                minLength = strs[i].length();
                minIndex = i;
            }
        }

        String minString = strs[minIndex];

        String result = "";
        for (int i = 0; i < minString.length(); i++) {
            boolean everythingIsOk = true;
            for (int j = 0; j < strs.length; j++) {
                if (strs[j].charAt(i) != minString.charAt(i)) {
                    everythingIsOk = false;
                    break;
                }
            }
            if (everythingIsOk == false) {
                return result;
            }
            result += minString.charAt(i);
        }
        return result;


    }
}
