package com.gridnev;

/*
*
* Given a string, find the length of the longest substring without repeating characters.

Examples:

Given "abcabcbb", the answer is "abc", which the length is 3.

Given "bbbbb", the answer is "b", with the length of 1.

Given "pwwkew", the answer is "wke", with the length of 3.
Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
*
* */


public class LongestSubstringWithoutRepeatingCharacters {
    public static int lengthOfLongestSubstring_simpleSlidingWindow(String s) {
        if (s == null || s.isEmpty()) {
            return 0;
        }
        if (s.length() == 1) {
            return 1;
        }

        boolean[] set = new boolean[128];

        char[] chars = s.toCharArray();
        int beginIndex = 0;
        int endIndex = 0;
        int result = 0;
        while (endIndex < chars.length) {
            char currentCharToBeAdded = chars[endIndex];
            if (set[currentCharToBeAdded]) {
                set[chars[beginIndex]] = false;
                beginIndex++;
            } else {
                result = Math.max(result, endIndex - beginIndex + 1);
                set[currentCharToBeAdded] = true;
                endIndex++;
            }
        }
        return result;

    }

    public static int lengthOfLongestSubstring_OptimizedSlidingWindow(String s) {
        if (s == null || s.isEmpty()) {
            return 0;
        }
        if (s.length() == 1) {
            return 1;
        }

        int[] positions = new int[256];

        char[] chars = s.toCharArray();
        int beginIndex = 0;
        int endIndex = 0;
        int result = 0;
        while (endIndex < chars.length) {
            char currentCharToBeAdded = chars[endIndex];
            if (positions[currentCharToBeAdded] > 0 && positions[currentCharToBeAdded] >= beginIndex + 1) {
                beginIndex = positions[currentCharToBeAdded];
            } else {
                result = Math.max(result, endIndex - beginIndex + 1);
            }

            positions[currentCharToBeAdded] = endIndex + 1;
            endIndex++;
        }
        return result;

    }
}
