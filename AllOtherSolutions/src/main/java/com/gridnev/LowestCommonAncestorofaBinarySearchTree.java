package com.gridnev;

import com.gridnev.BinarySearch.TreeNode;

/**
 * Created by a.gridnev on 18/02/2016.
 */
public class LowestCommonAncestorofaBinarySearchTree {
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        TreeNode current = root;
        int maxChildValue = Math.max(p.val, q.val);
        int minChildValue = Math.min(p.val, q.val);
        while (current != null) {
            if (current.val == p.val) {
                return p;
            }
            if (current.val == q.val) {
                return q;
            }

            if (maxChildValue > current.val && minChildValue <= current.val) {
                return current;
            }

            if (minChildValue > current.val) {
                current = current.right;
            }
            else {
                current = current.left;
            }

        }

        return null;
    }
}
