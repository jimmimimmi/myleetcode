package com.gridnev;

public class MaxProfitOfStock {

    public int maxProfit(int[] prices) {
        if (prices.length == 0 || prices.length == 1) {
            return 0;
        }

        int currentMinValue = prices[0];
        int bestProfit = 0;
        int currentProfit;
        for (int i = 1; i < prices.length; i++) {
            int price = prices[i];
            if (price > currentMinValue) {
                currentProfit = price - currentMinValue;
                if (currentProfit > bestProfit) {
                    bestProfit = currentProfit;
                }
            } else {
                currentMinValue = price;
            }
        }
        return bestProfit;

    }
}
