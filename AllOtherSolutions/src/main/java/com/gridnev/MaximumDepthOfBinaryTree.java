package com.gridnev;

import com.gridnev.BinarySearch.TreeNode;

import java.util.LinkedList;

/**
 * Created by a.gridnev on 16/02/2016.
 */
public class MaximumDepthOfBinaryTree {

    class Pair {
        private TreeNode node;
        private int layer;

        public Pair(TreeNode node, int layer) {
            this.node = node;
            this.layer = layer;
        }

        public int getLayer() {
            return layer;
        }

        public TreeNode getNode() {
            return node;
        }
    }

    public int maxDepth(TreeNode root) {

        if (root == null) return 0;

        return 1 + Math.max(maxDepth(root.left), maxDepth(root.right));
    }
}
