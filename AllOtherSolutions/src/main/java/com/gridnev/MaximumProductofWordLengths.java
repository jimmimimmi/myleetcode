package com.gridnev;

/**
 * Created by a.gridnev on 04/04/2016.
 */
public class MaximumProductofWordLengths {
    public int maxProduct(String[] words) {

        long[] masks = new long[words.length];
        for (int wordIndex = 0; wordIndex < words.length; wordIndex++) {
            String word = words[wordIndex];
            long mask = 0;
            for (int i = 0; i < word.length(); i++) {
                char c = word.charAt(i);
                int shift = c - 'a' + 1;
                mask = mask | (1 << shift);
            }
            masks[wordIndex] = mask;
        }

        int max = 0;
        for (int i = 0; i < words.length; i++) {
            for (int j = 0; j < words.length; j++) {
                if (i == j)
                    continue;
                if ((masks[i] & masks[j]) == 0) {
                    max = Math.max(max, words[i].length() * words[j].length());
                }
            }
        }
        return max;
    }
}
