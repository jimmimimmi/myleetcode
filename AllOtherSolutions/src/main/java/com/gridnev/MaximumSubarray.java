package com.gridnev;

/**
 * Created by a.gridnev on 21/04/2016.
 */
public class MaximumSubarray {
    public int maxSubArray(int[] nums) {

        if (nums.length == 1) return nums[0];

        int bestSum = Integer.MIN_VALUE;
        boolean wasPositive = false;

        int currentSum = 0;
        for (int i = 0; i < nums.length; i++) {
            int current = nums[i];
            if (current < 0 && !wasPositive) {
                bestSum = Math.max(bestSum, current);
                continue;
            }

            currentSum += current;
            if (current >= 0) {
                wasPositive = true;
            }
            if (currentSum < 0) {
                currentSum = 0;
            }
            if (wasPositive) {
                bestSum = Math.max(bestSum, currentSum);
            }
        }
        return bestSum;
    }
}
