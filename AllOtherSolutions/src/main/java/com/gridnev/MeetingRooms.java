package com.gridnev;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by a.gridnev on 18/02/2016.
 */
class Interval {
    int start;
    int end;

    Interval() {
        start = 0;
        end = 0;
    }

    Interval(int s, int e) {
        start = s;
        end = e;
    }
}

public class MeetingRooms {
    public boolean canAttendMeetings(Interval[] intervals) {

        if (intervals == null)
            return false;

        if (intervals.length == 1) {
            return true;
        }

        Arrays.sort(intervals, new Comparator<Interval>() {
            public int compare(Interval o1, Interval o2) {
                return Integer.compare(o1.start, o2.start);
            }
        });

        for (int i = 0; i < intervals.length - 1; i++) {
            if (intervals[i + 1].start < intervals[i].end) {
                return false;
            }
        }

        return true;

    }
}
