package com.gridnev;

/**
 * Created by Alena.cy on 24.01.2016.
 */
public class MergeTwoSortedLists {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {

        if (l1 == null) {
            return l2;
        }

        if (l2 == null) {
            return l1;
        }

        ListNode first1 = l1;
        ListNode first2 = l2;
        ListNode newHead = null;
        if (first1.val <= first2.val) {
            newHead = first1;
            first1 = first1.next;
        } else {
            newHead = first2;
            first2 = first2.next;
        }
        ListNode current = newHead;


        while (first1 != null && first2 != null) {
            if (first1.val <= first2.val) {
                current.next = first1;
                current=first1;
                first1 = first1.next;

            } else {
                current.next = first2;
                current = first2;
                first2 = first2.next;
            }
        }

        if(first1!=null){
            current.next=first1;
        }
        else {
            current.next = first2;
        }

        return newHead;
    }
}
