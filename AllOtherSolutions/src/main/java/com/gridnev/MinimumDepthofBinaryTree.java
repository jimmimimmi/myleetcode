package com.gridnev;

import com.gridnev.BinarySearch.TreeNode;

/**
 * Created by a.gridnev on 25/02/2016.
 */
public class MinimumDepthofBinaryTree {

    public int minDepth(TreeNode root) {
        return getMin(root);
    }

    private int getMin(TreeNode node) {
        if (node == null) {
            return 0;
        }

        return 1 + Math.min(getMin(node.left), getMin(node.right));
    }
}
