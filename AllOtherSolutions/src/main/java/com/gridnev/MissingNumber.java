package com.gridnev;

/**
 * Created by a.gridnev on 04/04/2016.
 */
public class MissingNumber {
    public int missingNumber(int[] nums) {

        int length = nums.length;
        int perfectSum = 0;
        if (length % 2 == 0) {
            perfectSum = (length + 1) * (length / 2);
        } else {
            perfectSum = (length + 1) * (length / 2) + (length + 1) / 2;
        }

        int actualSum=0;
        for (int num:nums){
            actualSum+=num;
        }

        return perfectSum - actualSum;


    }
}
