package com.gridnev;

/**
 * Created by a.gridnev on 04/03/2016.
 * https://leetcode.com/problems/range-sum-query-immutable/
 */
public class NumArray {

    private int[] numsss = new int[0];


    public NumArray(int[] nums) {

        if(nums==null || nums.length==0) return;
        this.numsss = new int[nums.length];

        this.numsss[0]=nums[0];
        for (int i  = 1; i < nums.length; i++) {
            this.numsss[i] = nums[i]+ this.numsss[i-1];
        }
    }

    public int sumRange(int i, int j) {



       if(i==0){
           return numsss[j];
       }

        return numsss[j]-numsss[i-1];
    }
}
