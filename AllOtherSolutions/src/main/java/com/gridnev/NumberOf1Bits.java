package com.gridnev;

/**
 * Created by a.gridnev on 23/02/2016.
 */
public class NumberOf1Bits {


    public int hammingWeight(int n) {
        if (n == 0) return 0;

        int count = 0;
        for (; n != 0; n = n & (n - 1)) {
            count++;
        }
        return count;
    }
}
