package com.gridnev;

/**
 * Created by Alena.cy on 26.01.2016.
 */
public class OddEvenLinkedList {
    public ListNode oddEvenList(ListNode head) {

        if (head == null || head.next == null || head.next.next == null) return head;

        ListNode odd = head, even = odd.next, evenHead = odd.next;

        while (even != null && even.next != null) {
            odd.next = odd.next.next;
            even.next = even.next.next;

            odd = odd.next;
            even = even.next;
        }
        odd.next = evenHead;
        return head;

    }

}
