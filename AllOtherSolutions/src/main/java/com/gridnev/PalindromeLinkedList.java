package com.gridnev;

public class PalindromeLinkedList {

    public boolean isPalindrome(ListNode head) {


        if (head == null) return true;
        if (head.next == null) return true;
        if (head.next.next == null) {
            return head.val == head.next.val;
        }

        ListNode slow = head;
        ListNode fast = head.next;

        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }


        ListNode first = slow.next;
        ListNode prev = slow;

        slow.next = null;
        while (first != null) {
            ListNode next = first.next;
            first.next = prev;
            prev = first;
            first = next;
        }


        ListNode left = head;
        ListNode right = prev;

        while (left != null && right != null) {
            if (left.val != right.val)
                return false;

            left = left.next;
            right = right.next;

        }
        return true;
    }
}
