package com.gridnev;

/**
 * Created by a.gridnev on 25/02/2016.
 * https://leetcode.com/problems/palindrome-number/
 */
public class PalindromeNumber {
    public boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }
        if (x < 10) {
            return true;
        }
        if (x % 10 == 0) {
            return false;
        }

        int y = x;
        int revert = 0;


        for (; y > 0; ) {

            long v = revert * 10 + (y % 10);
            if (v > Integer.MAX_VALUE) {
                return false;
            }
            revert = (int) v;

            y /= 10;

        }
        return revert == x;
    }
}
