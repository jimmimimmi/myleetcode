package com.gridnev;

import java.util.Collection;
import java.util.HashMap;

/**
 * Created by Alena.cy on 23.01.2016.
 */
public class PalindromePermutation {

    public boolean canPermutePalindrome(String s) {

        HashMap<Character, Integer> map = new HashMap<Character, Integer>();

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if(map.containsKey(c)==false){
                map.put(c,1);
            }
            else map.put(c,map.get(c)+1);
        }

        boolean thereIsOddNumber = false;

        Collection<Integer> values = map.values();
        for (Integer value:values) {
            if(value%2==1){
                if(thereIsOddNumber){
                    return false;
                }
                else thereIsOddNumber = true;
            }
        }
        return true;
    }
}
