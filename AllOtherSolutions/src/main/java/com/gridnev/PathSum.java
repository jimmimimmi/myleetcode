package com.gridnev;

import com.gridnev.BinarySearch.TreeNode;

/**
 * Created by a.gridnev on 25/02/2016.
 */
public class PathSum {
    public boolean hasPathSum(TreeNode root, int sum) {
        return hasSum(root, sum);
    }

    private boolean hasSum(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }

        if (root.left == null && root.right == null) {
            return root.val == sum;
        }

        int subStructSum = sum - root.val;
        boolean leftHasSum = false;
        boolean rightHasSum = false;

        if (root.left != null) {
            leftHasSum = hasSum(root.left, subStructSum);
        }

        if (root.right != null) {
            rightHasSum = hasSum(root.right, subStructSum);
        }

        return leftHasSum || rightHasSum;
    }
}
