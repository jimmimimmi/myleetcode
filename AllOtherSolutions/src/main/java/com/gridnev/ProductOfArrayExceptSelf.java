package com.gridnev;

/**
 * Created by a.gridnev on 31/03/2016.
 * https://leetcode.com/problems/product-of-array-except-self/
 */
public class ProductOfArrayExceptSelf {
    public int[] productExceptSelf(int[] nums) {

        if (nums.length == 2) {
            return new int[]{nums[1], nums[0]};
        }

        int[] result = new int[nums.length];
        result[0] = 1;

        int firstProduct = 1;
        for (int i = 0; i < nums.length - 1; i++) {
            firstProduct *= nums[i];

            result[i + 1] = firstProduct;
        }

        int secondProduct = 1;
        for (int i = nums.length - 1; i > 0; i--) {
            secondProduct *= nums[i];
            result[i - 1] *= secondProduct;
        }
        return result;
    }

}
