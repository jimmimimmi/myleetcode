package com.gridnev;

/**
 * Created by a.gridnev on 26/02/2016.
 */
public class RectangleArea {

    public int computeArea(int A, int B, int C, int D,
                           int E, int F, int G, int H) {

        int xLeftOfFirst = A < E ? A : E;
        int xRighTOfFirst = A < E ? C : G;
        int xLeftOfSecond = A < E ? E : A;
        int xRighTOfSecond = A < E ? G : C;


        int yLeftOfFirst =  A < E ? B : F;
        int yRighTOfFirst =  A < E ? D : H;
        int yLeftOfSecond =  A < E ? F : B;
        int yRighTOfSecond = A < E ? H : D;

        int totalFirstArea = getSquareArea(xLeftOfFirst, xRighTOfFirst, yLeftOfFirst, yRighTOfFirst);
        int totalSecondArea = getSquareArea(xLeftOfSecond, xRighTOfSecond, yLeftOfSecond, yRighTOfSecond);

        int union = totalFirstArea + totalSecondArea;
        if (xLeftOfSecond > xRighTOfFirst) {
            return union;
        }

        if (yLeftOfFirst > yRighTOfSecond || yRighTOfFirst <= yLeftOfSecond) {
            return union;
        }

        int xLeftOverLapping = xLeftOfSecond;
        int xRightOverLapping = Math.min(xRighTOfFirst, xRighTOfSecond);

        int yLeftOverLapping = Math.max(yLeftOfFirst, yLeftOfSecond);

        int yRightOverLapping = Math.min(yRighTOfFirst, yRighTOfSecond);

        int overlapArea = getSquareArea(xLeftOverLapping, xRightOverLapping, yLeftOverLapping, yRightOverLapping);
        return union - overlapArea;
    }

    private int getSquareArea(int xLeftOfFirst, int xRighTOfFirst, int yLeftOfFirst, int yRighTOfFirst) {
        return Math.abs(xLeftOfFirst - xRighTOfFirst) * Math.abs(yLeftOfFirst - yRighTOfFirst);
    }
}
