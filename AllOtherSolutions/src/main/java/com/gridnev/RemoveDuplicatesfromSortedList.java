package com.gridnev;

/**
 * Created by Alena.cy on 26.01.2016.
 */
public class RemoveDuplicatesfromSortedList {

    public ListNode deleteDuplicates(ListNode head) {

        if (head == null || head.next == null) {
            return head;
        }

        ListNode current = head.next, previousUniq = head;


        while (current != null) {
            if (current.val != previousUniq.val) {
                previousUniq.next=current;
                previousUniq=previousUniq.next;
            }
            current = current.next;
        }
        previousUniq.next = null;
        return head;
    }
}
