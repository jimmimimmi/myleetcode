package com.gridnev;

/**
 * Created by Alena.cy on 26.01.2016.
 */
public class RemoveLinkedListElements {
    public ListNode removeElements(ListNode head, int val) {

        ListNode prev = null;
        ListNode current = head;
        while (current != null) {
            if (current.val == val) {
                if (prev != null) {
                    prev.next = current.next;
                }
                else {
                    head = current.next;
                }
            }
            else {
                prev = current;
            }
            current = current.next;
        }
        return head;
    }

}
