package com.gridnev;

/**
 * Created by Alena.cy on 24.01.2016.
 */
public class RemoveNthNodeFromEndofList {

    public ListNode removeNthFromEnd(ListNode head, int n) {

        if (n == 0) {
            return head;
        }

        // list = [1], n=1
        if (head.next == null) {
            return null;
        }

        ListNode slow = head;
        ListNode fast = head.next;
        int slowNumber = 1;

        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
            slowNumber++;
        }
        int length = slowNumber * 2;
        if (fast == null) {
            length--;
        }

        int numberForRemove = length - n + 1;
        if (numberForRemove == 1) {
            return head.next;
        }

        if (slowNumber < numberForRemove) {
            while (slowNumber != numberForRemove - 1) {
                slow = slow.next;
                slowNumber++;
            }
            slow.next = slow.next.next;
            return head;
        } else {
            slow = head;
            slowNumber = 1;
            while (slowNumber != numberForRemove - 1) {
                slow = slow.next;
                slowNumber++;
            }
            slow.next = slow.next.next;
            return head;
        }
    }
}
