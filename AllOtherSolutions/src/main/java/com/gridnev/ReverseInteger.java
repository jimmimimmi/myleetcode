package com.gridnev;

//Input:
//        [4,3,2,7,8,2,3,1]
//
//        Output:
//        [5,6]

//[]


//321  0


public class ReverseInteger {
    public int reverse(int x) {

        int resultOfDividing = x;
        int result = 0;
        while (resultOfDividing != 0) {
            int lastNumber = resultOfDividing % 10;
            resultOfDividing = resultOfDividing / 10;

            int newResult = result * 10 + lastNumber;
            if ((newResult - lastNumber) / 10 != result) {
                return 0;
            }
            result = newResult;
        }


        return result;
    }
}