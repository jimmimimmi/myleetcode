package com.gridnev;

public class ReverseVowels {
    private static boolean[] vowels;

    static {
        vowels = new boolean[200];
        vowels['a'] = true;
        vowels['e'] = true;
        vowels['i'] = true;
        vowels['o'] = true;
        vowels['u'] = true;

        vowels['A'] = true;
        vowels['E'] = true;
        vowels['I'] = true;
        vowels['O'] = true;
        vowels['U'] = true;
    }

    public String reverseVowels(String s) {
        if (s == null || s.length() <= 1) {
            return s;
        }
        char[] chars = s.toCharArray();

        int leftIndex = 0;
        int rightIndex = chars.length - 1;
        while (leftIndex < rightIndex) {
            while (!vowels[chars[leftIndex]] && leftIndex < rightIndex) {
                leftIndex++;
            }
            if (leftIndex == rightIndex) {
                break;
            }
            while (!vowels[chars[rightIndex]] && leftIndex < rightIndex) {
                rightIndex--;
            }
            if (leftIndex == rightIndex) {
                break;
            }

            char temp = chars[leftIndex];
            chars[leftIndex] = chars[rightIndex];
            chars[rightIndex] = temp;
            leftIndex++;
            rightIndex--;
        }

        return String.copyValueOf(chars);
    }
}
