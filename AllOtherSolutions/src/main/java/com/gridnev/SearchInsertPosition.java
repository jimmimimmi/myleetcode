package com.gridnev;

/**
 * Created by a.gridnev on 08/04/2016.
 * https://leetcode.com/problems/search-insert-position/
 */
public class SearchInsertPosition {
    public int searchInsert(int[] nums, int target) {
        int length = nums.length;
        if (length == 0) {
            return 0;
        }

        if (nums.length == 1) {
            return target <= nums[0] ? 0 : 1;
        }


        if (target == nums[length - 1]) {
            return length - 1;
        }

        if (target > nums[length - 1]) {
            return length;
        }

        if (target <= nums[0]) {
            return 0;
        }

        int begin = 0;
        int end = length - 1;

        while (begin < end) {

            if (nums[begin] == target)
                return begin;

            if (nums[end] == target)
                return end;

            int middle = begin + (end - begin) / 2;

            int middleValue = nums[middle];
            if (middleValue == target)
                return middle;

            if (middleValue < target) {
                if (nums[middle + 1] > target) {
                    return middle + 1;
                }
                begin = middle + 1;
                continue;
            }

            // middleValue > target

            if (nums[middle - 1] < target) {
                return middle;
            }
            end = middle - 1;
        }

        return 0;
    }
}
