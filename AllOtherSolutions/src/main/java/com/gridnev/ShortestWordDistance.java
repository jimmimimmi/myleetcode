package com.gridnev;

/**
 * Created by a.gridnev on 16/02/2016.
 */
public class ShortestWordDistance {
    public int shortestDistance(String[] words, String word1, String word2) {

        int index = -1;
        int distance = Integer.MAX_VALUE;

        for (int i = 0; i < words.length; i++) {
            String word = words[i];
            if(word.equals(word1) || word.equals(word2)){
                if(index!=-1 && !word.equals(words[index])){
                    distance = Math.min(distance,i-index);
                }

                index = i;
            }
        }
        return distance;
    }
}
