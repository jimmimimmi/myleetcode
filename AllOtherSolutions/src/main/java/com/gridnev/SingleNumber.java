package com.gridnev;

/**
 * Created by a.gridnev on 24/03/2016.
 */
public class SingleNumber {
    public int singleNumber(int[] nums) {


        int result = 0;
        for (int num : nums) {
            result = result ^ num;
        }
        return result;
    }
}
