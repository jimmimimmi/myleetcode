package com.gridnev;

/**
 * Created by a.gridnev on 06/04/2016.
 */
public class SingleNumberII {
    public int singleNumber(int[] nums) {

        if(nums.length==1){
            return nums[0];
        }

        int[] amountOfCorrespondedBits = new int[32];
        for (int i = 0; i < amountOfCorrespondedBits.length; i++) {
            int sum = 0;
            for (int currentNum : nums) {
                int shift = currentNum >> i;
                int currentSum = shift & 1;
                sum += currentSum;
            }
            amountOfCorrespondedBits[i] = sum;
        }

        int result = 0;

        for (int i = 0; i < amountOfCorrespondedBits.length; i++) {
            int currentAmount = amountOfCorrespondedBits[i];
            if (currentAmount % 3 != 0) {
                result += 1 << i;
            }
        }

        return result;
    }
}
