package com.gridnev;

import java.util.*;

/**
 * Created by a.gridnev on 25/03/2016.
 * https://leetcode.com/problems/single-number-iii/
 */
public class SingleNumberIII {
    public int[] singleNumber(int[] nums) {

        int x_xor_y = 0;
        for (int num : nums) {
            x_xor_y ^= num;
        }

        int mask = x_xor_y & ~(x_xor_y - 1);
        int x = 0;
        int y = 0;

        for (int num : nums) {
            if ((num & mask) == 0) {
                x = x ^ num;
            } else {
                y = y ^ num;
            }
        }
        return new int[]{x, y};
    }
}
