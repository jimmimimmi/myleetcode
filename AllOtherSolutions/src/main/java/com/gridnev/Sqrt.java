package com.gridnev;

public class Sqrt {
    public int getSqrt(int x) {
        if (x == 0 || x == 1) {
            return x;
        }
        int left = 1;
        int right = x;

        while (left < right) {
            int middle = left + (right - left) / 2;
            int i = middle * middle;
            if (middle == left) {
                return middle;
            }

            if (i == x) {
                return middle;
            }
            if (i / middle != middle || i > x) {
                right = middle;
                continue;
            } else {
                left = middle;
            }
        }
        return left;

    }


}
