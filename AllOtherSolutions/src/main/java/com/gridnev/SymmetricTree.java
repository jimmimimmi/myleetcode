package com.gridnev;

import com.gridnev.BinarySearch.TreeNode;

/**
 * Created by a.gridnev on 23/02/2016.
 * https://leetcode.com/problems/symmetric-tree/
 */
public class SymmetricTree {

    public boolean isSymmetric(TreeNode root) {
        if (root == null) return true;

        return isTheySymmetric(root.left, root.right);
    }

    private boolean isTheySymmetric(TreeNode left, TreeNode right) {
        if (left == null && right == null) {
            return true;
        }

        if(left==null || right == null){
            return false;
        }

        if (left.val != right.val) {
            return false;
        }

        boolean theySymmetric1 = isTheySymmetric(left.left, right.right);
        boolean theySymmetric2 = isTheySymmetric(left.right, right.left);

        return theySymmetric1 && theySymmetric2;
    }
}
