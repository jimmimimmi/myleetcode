package com.gridnev;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TopKFrequentElements {
    public List<Integer> topKFrequent(int[] initialArray, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        for(int currentNumber: initialArray){
            map.put(currentNumber, map.getOrDefault(currentNumber,0)+1);
        }

        // corner case: if there is only one number in initialArray, we need the bucket has index 1.
        List<Integer>[] bucket = new List[initialArray.length+1];
        for(int numberFromInitialArray:map.keySet()){
            int freq = map.get(numberFromInitialArray);
            if(bucket[freq]==null)
                bucket[freq] = new LinkedList<>();
            bucket[freq].add(numberFromInitialArray);
        }

        List<Integer> res = new LinkedList<>();
        for(int i=bucket.length-1; i>0 && k>0; --i){
            if(bucket[i]!=null){
                List<Integer> list = bucket[i];
                res.addAll(list);
                k-= list.size();
            }
        }

        return res;
    }
}
