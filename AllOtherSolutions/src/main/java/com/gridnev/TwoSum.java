package com.gridnev;

import java.util.*;

/**
 * Created by a.gridnev on 10/03/2016.
 */
public class TwoSum {
    public int[] twoSum(int[] nums, int target) {

        HashMap<Integer, List<Integer>> map = new HashMap<Integer, List<Integer>>();

        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            if (map.containsKey(num)) {
                List<Integer> list = map.get(num);
                list.add(i);
            } else {
                ArrayList<Integer> integers = new ArrayList<Integer>();
                integers.add(i);
                map.put(num, integers);
            }
        }

        Set<Integer> integers = map.keySet();

        for (Integer key : integers) {
            int otherKey = target - key;

            if (map.containsKey(otherKey)) {
                List<Integer> currentList = map.get(key);
                List<Integer> otherList = map.get(otherKey);

                if (otherKey == key) {
                    return new int[]{currentList.get(0), currentList.get(1)};
                }

                return new int[]{currentList.get(0), otherList.get(0)};
            }
        }
        return null;
    }
}
