package com.gridnev;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by a.gridnev on 19/01/2016.
 * https://leetcode.com/problems/two-sum-iii-data-structure-design/
 */
public class TwoSumIII {


    private HashMap<Integer, Integer> amountOfOccuranciesMap = new HashMap<Integer, Integer>();

    // Add the number to an internal data structure.
    public void add(int number) {
        final int value = amountOfOccuranciesMap.containsKey(number) ? amountOfOccuranciesMap.get(number) + 1 : 1;
        amountOfOccuranciesMap.put(number, value);

    }

    // Find if there exists any pair of numbers which sum is equal to the value.
    public boolean find(int value) {
        for (Map.Entry<Integer, Integer> pair : amountOfOccuranciesMap.entrySet()) {
            final Integer key = pair.getKey();
            final Integer amountOfOccurancies = pair.getValue();

            final int anotherPossibleKey = value - key;

            if (anotherPossibleKey == key) {
                if (amountOfOccurancies > 1){
                    return true;
                }
            } else {
                if (amountOfOccuranciesMap.containsKey(anotherPossibleKey)) {
                    return true;
                }
            }


        }

        return false;
    }
}
