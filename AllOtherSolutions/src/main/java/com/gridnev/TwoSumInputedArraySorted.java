package com.gridnev;

public class TwoSumInputedArraySorted {

    public int[] twoSum(int[] numbers, int target) {
        int[] res = new int[2];
        int leftIndex = 0;
        int rightIndex = numbers.length - 1;
        while (leftIndex < rightIndex) {
            int sum = numbers[leftIndex] + numbers[rightIndex];
            if (sum < target) {
                leftIndex = binarySearch(numbers, target - numbers[rightIndex], leftIndex + 1, rightIndex - 1);
            } else if (sum > target) {
                rightIndex = binarySearch(numbers, target - numbers[leftIndex], leftIndex + 1, rightIndex - 1);
            } else {
                res[0] = leftIndex + 1;
                res[1] = rightIndex + 1;
                break;
            }
        }
        return res;
    }

    public int binarySearch(int[] numbers, int target, int left, int right) {
        while (left + 1 < right) {
            int mid = left + (right - left) / 2;
            if (numbers[mid] < target) left = mid;
            else if (numbers[mid] > target) right = mid;
            else return mid;
        }

        if (numbers[left] >= target) return left;
        else return right;

    }


}
