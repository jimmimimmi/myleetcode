package com.gridnev;

/**
 * Created by a.gridnev on 06/04/2016.
 */
public class UniqueBinarySearchTrees {
    public int numTrees(int n) {

        if (n == 1) return 1;
        if (n == 2) return 2;
        if (n == 3) return 5;

        int[] cache = new int[n + 1];
        cache[1] = 1;
        cache[2] = 2;
        cache[3] = 5;

        for (int i = 4; i <= n; i++) {
            cache[i] = cache[i - 1] * 2;
            for (int firstValue = 1, lastValue = i - 1; firstValue <= i - 1; firstValue++, lastValue--) {
                cache[i] += (cache[firstValue] + cache[lastValue]);
            }
        }
        return cache[n];

    }
}
