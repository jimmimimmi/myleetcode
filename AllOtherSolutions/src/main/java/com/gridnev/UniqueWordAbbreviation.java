package com.gridnev;

import java.util.HashMap;

/**
 * Created by a.gridnev on 19/01/2016.
 */
public class UniqueWordAbbreviation {

    public class ValidWordAbbr {

        HashMap<String, String> map;
        public ValidWordAbbr(String[] dictionary) {
            map = new HashMap<String, String>();
            for(String str:dictionary){
                String key = getKey(str);
                // If there is more than one string belong to the same key
                // then the key will be invalid, we set the value to ""
                if(map.containsKey(key)){
                    if(!map.get(key).equals(str)){
                        map.put(key, "");
                    }
                }
                else{
                    map.put(key, str);
                }
            }
        }

        public boolean isUnique(String word) {
            final String abbreviation = getKey(word);
            return !map.containsKey(abbreviation) ||
                    map.get(abbreviation).equals(word);
        }

        String getKey(String str){
            if(str.length()<=2) return str;
            return str.charAt(0)+Integer.toString(str.length()-2)+str.charAt(str.length()-1);
        }
    }
}
