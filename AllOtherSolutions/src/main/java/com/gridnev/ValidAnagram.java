package com.gridnev;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by a.gridnev on 19/01/2016.
 * https://leetcode.com/problems/valid-anagram/
 */
public class ValidAnagram {

    public boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }

        final HashMap<Character, Integer> tHashMap = new HashMap<Character, Integer>();
        final HashMap<Character, Integer> sHashMap = new HashMap<Character, Integer>();

        for (int i = 0; i < s.length(); i++) {
            final char sChar = s.charAt(i);
            final char tChar = t.charAt(i);

            tHashMap.put(tChar, tHashMap.containsKey(tChar) ? tHashMap.get(tChar) + 1 : 1);
            sHashMap.put(sChar, sHashMap.containsKey(sChar) ? sHashMap.get(sChar) + 1 : 1);
        }

        for (Map.Entry<Character, Integer> tEntry : tHashMap.entrySet()) {
            if (!sHashMap.containsKey(tEntry.getKey())) {
                return false;
            }

            final Integer sValue = sHashMap.get(tEntry.getKey());
            if (!sValue.equals(tEntry.getValue())) {
                return false;
            }
        }

        return true;

    }
}
