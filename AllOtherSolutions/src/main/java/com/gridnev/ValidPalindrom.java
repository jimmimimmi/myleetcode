package com.gridnev;

public class ValidPalindrom {

    public boolean isPalindrome(String s) {
        if (s == null || s.isEmpty() || s.length() == 1) {
            return true;
        }

        int leftPosition = 0;
        int rightPosition = s.length() - 1;

        while (leftPosition < rightPosition) {
            while (!isAlphaNumeric(s.charAt(leftPosition)) && leftPosition < rightPosition) {
                leftPosition++;
            }

            while (!isAlphaNumeric(s.charAt(rightPosition)) && leftPosition < rightPosition) {
                rightPosition--;
            }

            if (leftPosition == rightPosition) {
                return compare(s.charAt(leftPosition), s.charAt(rightPosition));
            }

            if (leftPosition > rightPosition) {
                return true;
            }

            if (!compare(s.charAt(leftPosition), s.charAt(rightPosition))) {
                return false;
            }
            leftPosition++;
            rightPosition--;
        }
        return true;
    }

    private boolean isAlphaNumeric(char left) {
        if (left >= '0' && left <= '9') {
            return true;
        } else if (left >= 'A' && left <= 'Z') {
            return true;
        } else if (left >= 'a' && left <= 'z') {
            return true;
        } else {
            return false;
        }
    }

    private boolean compare(char left, char right) {
        if (left >= '0' && left <= '9') {
            return left == right;
        } else if (left >= 'A' && left <= 'Z') {
            return left - 'A' == right - 'A' || left - 'A' == right - 'a';
        } else if (left >= 'a' && left <= 'z') {
            return left - 'a' == right - 'A' || left - 'a' == right - 'a';
        } else return !isAlphaNumeric(right);
    }
}
