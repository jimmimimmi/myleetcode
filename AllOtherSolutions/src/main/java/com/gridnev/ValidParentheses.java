package com.gridnev;

import java.util.LinkedList;

/**
 * Created by a.gridnev on 04/03/2016.
 */
public class ValidParentheses {
    public boolean isValid(String s) {

        LinkedList<Character> characters = new LinkedList<Character>();


        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '(' || c == '{' || c== '[') {
                characters.addFirst(c);
            }
            else {
                char expectedOpenSymbol = getExpectedOpenSymbol(c);
                if(characters.isEmpty()) return false;
                Character character = characters.removeFirst();
                if(character.charValue()!=expectedOpenSymbol) return false;
            }

        }
        return characters.isEmpty();
    }

    private char getExpectedOpenSymbol(char close){
        if(close == ')') return '(';
        if(close == '}') return '{';
        if(close == ']') return '[';
        throw new IllegalArgumentException();
    }
}
