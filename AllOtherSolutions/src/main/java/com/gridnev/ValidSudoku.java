package com.gridnev;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by a.gridnev on 19/01/2016.
 */
public class ValidSudoku {

    private static HashSet<Character> validChars = new HashSet<Character>();

    static {
        validChars.add('0');
        validChars.add('1');
        validChars.add('2');
        validChars.add('3');
        validChars.add('4');
        validChars.add('5');
        validChars.add('6');
        validChars.add('7');
        validChars.add('8');
        validChars.add('9');
        validChars.add('.');
    }

    public boolean isValidSudoku(char[][] board) {

        final int length = board.length;
        if (board[0].length != length) {
            return false;
        }

        if (length % 3 != 0) {
            return false;
        }

        final ArrayList<HashSet<Character>> rowHashSets = new ArrayList<HashSet<Character>>();
        final ArrayList<HashSet<Character>> columnHashSets = new ArrayList<HashSet<Character>>();
        final ArrayList<HashSet<Character>> rectangleHashSets = new ArrayList<HashSet<Character>>();


        for (int i = 0; i < length; i++) {
            rowHashSets.add(new HashSet<Character>());
            columnHashSets.add(new HashSet<Character>());
            rectangleHashSets.add(new HashSet<Character>());
        }


        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                final char currentChar = board[i][j];
                if (!validChars.contains(currentChar)) {
                    return false;
                }

                if (currentChar == '.') {
                    continue;
                }

                if (rowHashSets.get(i).contains(currentChar)) {
                    return false;
                }
                rowHashSets.get(i).add(currentChar);

                if (columnHashSets.get(j).contains(currentChar)) {
                    return false;
                }
                columnHashSets.get(j).add(currentChar);

                final int rectangleIndex = (i / 3) * (length / 3) + (j / 3);
                final HashSet<Character> rectangleHashSet = rectangleHashSets.get(rectangleIndex);
                if (rectangleHashSet.contains(currentChar)) {
                    return false;
                }

                rectangleHashSet.add(currentChar);
            }
        }

        return true;

    }
}
