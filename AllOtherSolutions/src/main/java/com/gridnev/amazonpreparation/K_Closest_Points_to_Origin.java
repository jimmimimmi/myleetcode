package com.gridnev.amazonpreparation;

/*
https://leetcode.com/problems/k-closest-points-to-origin/
973. K Closest Points to Origin
We have a list of points on the plane.  Find the K closest points to the origin (0, 0).

(Here, the distance between two points on a plane is the Euclidean distance.)

You may return the answer in any order.  The answer is guaranteed to be unique (except for the order that it is in.)



Example 1:

Input: points = [[1,3],[-2,2]], K = 1
Output: [[-2,2]]
Explanation:
The distance between (1, 3) and the origin is sqrt(10).
The distance between (-2, 2) and the origin is sqrt(8).
Since sqrt(8) < sqrt(10), (-2, 2) is closer to the origin.
We only want the closest K = 1 points from the origin, so the answer is just [[-2,2]].
Example 2:

Input: points = [[3,3],[5,-1],[-2,4]], K = 2
Output: [[3,3],[-2,4]]
(The answer [[-2,4],[3,3]] would also be accepted.)

 */

import java.util.Arrays;
import java.util.Random;

public class K_Closest_Points_to_Origin {
    Random random = new Random();

    public int[][] kClosest(int[][] points, int K) {
        partition(points, K, 0, points.length - 1);
        return Arrays.copyOfRange(points, 0, K);
    }

    private void partition(int[][] points, int K, int left, int right) {
        if (left >= right) return;
        int pivot = left;
        int pivotValue = dist(points[left + random.nextInt(right - left + 1)]);

        for (int i = left; i <= right; i++) {
            if (dist(points[i]) <= pivotValue) {
                swap(points, i, pivot);
                pivot++;
            }
        }

        int smallest = pivot - left;
        if (smallest == K) {
            return;
        } else if (smallest > K) {
            partition(points, K, left, pivot - 1);
        } else partition(points, K - smallest, pivot, right);

    }

    private void swap(int[][] points, int i, int j) {
        int x = points[i][0];
        int y = points[i][1];

        points[i][0] = points[j][0];
        points[i][1] = points[j][1];

        points[j][0] = x;
        points[j][1] = y;
    }

    private int dist(int[] point) {
        return point[0] * point[0] + point[1] * point[1];
    }
}
