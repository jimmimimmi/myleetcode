package com.gridnev.amazonpreparation;

public class Longest_Palindromic_Substring {
    public String longestPalindrome(String s) {
        if (s == null) return null;
        int length = s.length();
        if (length <= 1) return s;
        Boolean[][] pal = new Boolean[length][length];

        for (int i = 0; i < length; i++) {
            pal[i][i] = true;
        }

        for (int i = 0; i < length - 1; i++) {
            boolean p = s.charAt(i) == s.charAt(i + 1);
            pal[i][i + 1] = p;
            pal[i + 1][i] = p;
        }

        if (length > 2) {
            for (int i = 0; i < length; i++) {
                for (int j = 0; j < length; j++) {
                    fill(s, pal, i, j);
                }
            }
        }

        int start = 0;
        int end = 0;
        int len = 0;

        for (int i = 0; i < length; i++) {
            for (int j = i; j < length; j++) {
                if (pal[i][j] && (j - i + 1) > len) {
                    len = j - i + 1;
                    start = i;
                    end = j;

                }
            }
        }

        return s.substring(start, end + 1);
    }


    private void fill(String s, Boolean[][] pal, int i, int j) {
        int left = i < j ? i : j;
        int right = i < j ? j : i;
        if (pal[left][right] != null) return;
        fill(s, pal, left + 1, right - 1);
        boolean val = pal[left + 1][right - 1] && s.charAt(left) == s.charAt(right);
        pal[left][right] = val;
        pal[right][left] = val;
    }

}
