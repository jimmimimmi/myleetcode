package com.gridnev.amazonpreparation;

import java.util.Stack;

/*
https://leetcode.com/problems/merge-two-binary-trees/
617. Merge Two Binary Trees
Given two binary trees and imagine that when you put one of them to cover the other, some nodes of the two trees are overlapped while the others are not.

You need to merge them into a new binary tree. The merge rule is that if two nodes overlap, then sum node values up as the new value of the merged node. Otherwise, the NOT null node will be used as the node of new tree.

Example 1:

Input:
	Tree 1                     Tree 2
          1                         2
         / \                       / \
        3   2                     1   3
       /                           \   \
      5                             4   7
Output:
Merged tree:
	     3
	    / \
	   4   5
	  / \   \
	 5   4   7

 */



public class Merge_Two_Binary_Trees {
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    public TreeNode mergeTrees(TreeNode t1, TreeNode t2) {
        return merge(t1, t2);
    }

    private TreeNode merge(TreeNode t1, TreeNode t2) {
        if (t1 == null && t2 == null) {
            return null;
        }
        TreeNode newNode = new TreeNode(-1);

        if (t1 == null) {
            newNode.val = t2.val;
        } else if (t2 == null) {
            newNode.val = t1.val;
        } else newNode.val = t1.val + t2.val;

        TreeNode left = merge(t1 != null ? t1.left : null, t2 != null ? t2.left : null);
        TreeNode right = merge(t1 != null ? t1.right : null, t2 != null ? t2.right : null);
        newNode.left = left;
        newNode.right = right;
        return newNode;
    }


    public TreeNode mergeTrees2(TreeNode t1, TreeNode t2) {
        if (t1 == null) return t2;
        if (t2 == null) return t1;

        TreeNode head = new TreeNode(-1);

        Stack<TreeNode[]> stack = new Stack<>();
        stack.push(new TreeNode[]{t1, t2, head});

        while (!stack.isEmpty()) {
            TreeNode[] tuple = stack.pop();
            TreeNode left = tuple[0];
            TreeNode right = tuple[1];
            TreeNode node = tuple[2];

            node.val = left.val + right.val;
            if (left.left != null && right.left != null) {
                TreeNode newLeft = new TreeNode(-1);
                node.left = newLeft;
                stack.push(new TreeNode[]{left.left, right.left, newLeft});
            } else if (left.left != null) {
                node.left = left.left;
            } else node.left = right.left;

            if (left.right != null && right.right != null) {
                TreeNode newRight = new TreeNode(-1);
                node.right = newRight;
                stack.push(new TreeNode[]{left.right, right.right, newRight});
            } else if (left.right != null) {
                node.right = left.right;
            } else node.right = right.right;
        }

        return head;
    }

}
