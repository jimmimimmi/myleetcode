package com.gridnev.amazonpreparation;

/*
https://leetcode.com/problems/range-sum-of-bst/

938. Range Sum of BST

Given the root node of a binary search tree, return the sum of values of all nodes with value between L and R (inclusive).

The binary search tree is guaranteed to have unique values.

 Example 1:

Input: root = [10,5,15,3,7,null,18], L = 7, R = 15
Output: 32
Example 2:

Input: root = [10,5,15,3,7,13,18,1,null,6], L = 6, R = 10
Output: 23

Note:

The number of nodes in the tree is at most 10000.
The final answer is guaranteed to be less than 2^31.


 */
public class Range_Sum_of_BST {

    /*
     * Definition for a binary tree node.   */
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }


    public int rangeSumBST(TreeNode root, int L, int R) {
        return getSum(root, L, R, 0);
    }

    private int getSum(TreeNode root, int L, int R, int sum) {
        if (root == null) return sum;
        int left = getSum(root.left, L, R, sum);
        int right = getSum(root.right, L, R, sum);
        int result = left + right;
        if (root.val >= L && root.val <= R) {
            result += root.val;
        }
        return result;
    }
}
