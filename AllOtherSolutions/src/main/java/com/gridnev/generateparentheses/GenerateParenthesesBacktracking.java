package com.gridnev.generateparentheses;

import java.util.ArrayList;
import java.util.List;

public class GenerateParenthesesBacktracking {
    public static List<String> generate(int n) {
        char[] currentPermutation = new char[2 * n];
        ArrayList<String> result = new ArrayList<>();
        generate(currentPermutation, 0, 0, 0, result);

        return result;
    }

    private static void generate(char[] currentPermutation, int currentIndex, int openCount, int closeCount, List<String> result) {
        if (currentIndex == currentPermutation.length) {
            result.add(String.valueOf(currentPermutation));
        } else {
            if (openCount < currentPermutation.length / 2) {
                currentPermutation[currentIndex] = '(';
                generate(currentPermutation, currentIndex + 1, openCount + 1, closeCount, result);
            }
            if (closeCount < openCount) {
                currentPermutation[currentIndex] = ')';
                generate(currentPermutation, currentIndex + 1, openCount, closeCount + 1, result);
            }
        }
    }

    private boolean isValid(char[] s) {
        if (s == null || s.length == 0 || s.length % 2 == 1) {
            return false;
        }

        int counter = 0;
        for (int i = 0; i < s.length; i++) {
            char currentChar = s[i];
            if (currentChar == '(') {
                counter++;
            } else {
                counter--;
            }
            if (counter < 0) {
                return false;
            }
        }
        return counter == 0;
    }
}
