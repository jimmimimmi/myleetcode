package com.gridnev.generateparentheses;

import java.util.ArrayList;
import java.util.List;

public class GenerateParenthesesBruteForce {
    public static List<String> generate(int n) {
        char[] currentPermutation = new char[2 * n];
        ArrayList<String> result = new ArrayList<>();
        generate(currentPermutation, 0, result);

        return result;
    }

    private static void generate(char[] currentPermutation, int currentIndex, List<String> result) {
        if (currentIndex == currentPermutation.length) {
            if (isValid(currentPermutation)) {
                result.add(String.valueOf(currentPermutation));
            }
        } else {
            currentPermutation[currentIndex] = '(';
            generate(currentPermutation, currentIndex + 1, result);
            currentPermutation[currentIndex] = ')';
            generate(currentPermutation, currentIndex + 1, result);
        }
    }

    private static boolean isValid(char[] s) {
        if (s == null || s.length == 0 || s.length % 2 == 1) {
            return false;
        }

        int counter = 0;
        for (int i = 0; i < s.length; i++) {
            char currentChar = s[i];
            if (currentChar == '(') {
                counter++;
            } else {
                counter--;
            }
            if (counter < 0) {
                return false;
            }
        }
        return counter == 0;
    }
}
