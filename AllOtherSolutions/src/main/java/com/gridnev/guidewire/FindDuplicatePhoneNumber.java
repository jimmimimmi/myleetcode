package com.gridnev.guidewire;

public class FindDuplicatePhoneNumber {

    public boolean isThereAnyDuplicates(long[] phoneNumbers) {
        if (phoneNumbers == null) {
            return false;
        }

        long mask = 0L;
        for (long phoneNumber : phoneNumbers) {
            if (((mask >> phoneNumber) & 1L) == 1L) {
                return true;
            }

            mask = mask | (1L << phoneNumber);
        }

        return false;
    }

    public boolean isThereAnyDuplicates(String[] phoneNumbers) {
        if (phoneNumbers == null) {
            return false;
        }

        long mask = 0L;
        for (String phoneNumber : phoneNumbers) {
            long phone = Long.parseLong(phoneNumber.replaceAll("^[0-9]]", ""));
            if (((mask >> phone) & 1L) == 1L) {
                return true;
            }

            mask = mask | (1L << phone);
        }

        return false;
    }
}
