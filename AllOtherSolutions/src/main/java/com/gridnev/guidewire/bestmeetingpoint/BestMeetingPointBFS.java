package com.gridnev.guidewire.bestmeetingpoint;

import java.util.LinkedList;
import java.util.Queue;

public class BestMeetingPointBFS {
    private static final int[] rowNeighbours = new int[]{0, -1, 0, 1};
    private static final int[] columnNeighbours = new int[]{-1, 0, 1, 0};

    public static int minTotalDistance(int[][] grid) {

        int minimum = Integer.MAX_VALUE;
        for (int row = 0; row < grid.length; row++) {
            for (int column = 0; column < grid[0].length; column++) {
                minimum = Math.min(getMinimum(grid, row, column), minimum);
            }
        }
        return minimum;
    }

    private static int getMinimum(int[][] grid, int row, int column) {
        boolean[][] visited = new boolean[grid.length][grid[0].length];
        int distance = 0;
        Queue<Integer> rowsQueue = new LinkedList<>();
        Queue<Integer> columnsQueue = new LinkedList<>();
        Queue<Integer> distanceQueue = new LinkedList<>();

        rowsQueue.offer(row);
        columnsQueue.offer(column);
        distanceQueue.offer(0);

        while (!distanceQueue.isEmpty()) {
            Integer currentDistance = distanceQueue.poll();
            Integer currentRow = rowsQueue.poll();
            Integer currentColumn = columnsQueue.poll();

            if (currentRow < 0 ||
                    currentRow == grid.length ||
                    currentColumn < 0 ||
                    currentColumn == grid[0].length ||
                    visited[currentRow][currentColumn]) {
                continue;
            }
            if (grid[currentRow][currentColumn] == 1) {
                distance += currentDistance;
            }
            visited[currentRow][currentColumn] = true;

            for (int i = 0; i < rowNeighbours.length; i++) {
                int nextRow = currentRow + rowNeighbours[i];
                int nextColumn = currentColumn + columnNeighbours[i];

                rowsQueue.offer(nextRow);
                columnsQueue.offer(nextColumn);
                distanceQueue.offer(currentDistance + 1);
            }
        }

        return distance;

    }
}
