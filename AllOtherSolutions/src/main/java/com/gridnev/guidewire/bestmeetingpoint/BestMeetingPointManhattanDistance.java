package com.gridnev.guidewire.bestmeetingpoint;

import java.util.ArrayList;
import java.util.List;

public class BestMeetingPointManhattanDistance {
    static class Point {
        private int row;
        private int column;

        public Point(int row, int column) {
            this.row = row;
            this.column = column;
        }
    }

    public static int minTotalDistance(int[][] grid) {

        List<Point> allNonZeroPoints = getAllNonZeroPoints(grid);

        int minDistance = Integer.MAX_VALUE;

        for (int row = 0; row < grid.length; row++) {
            for (int column = 0; column < grid[0].length; column++) {
                int totalDistanceToAllNonZeroPoints = 0;
                for (Point point : allNonZeroPoints) {
                    totalDistanceToAllNonZeroPoints +=
                            Math.abs(row - point.row) + Math.abs(column - point.column);
                }
                minDistance = Math.min(minDistance, totalDistanceToAllNonZeroPoints);
            }
        }
        return minDistance;
    }

    private static List<Point> getAllNonZeroPoints(int[][] grid) {
        ArrayList<Point> points = new ArrayList<>();
        for (int row = 0; row < grid.length; row++) {
            for (int column = 0; column < grid[0].length; column++) {
                if (grid[row][column] == 1) {
                    points.add(new Point(row, column));
                }
            }
        }
        return points;
    }
}
