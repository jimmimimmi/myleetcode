package com.gridnev.guidewire.bestmeetingpoint;

import java.util.ArrayList;
import java.util.List;

public class BestMeetingPointManhattanDistanceImproved {
    public static int minTotalDistance(int[][] grid) {
        List<Integer> nonZeroColumns = getNonZeroColumns(grid);
        List<Integer> nonZeroRows = getNonZeroRows(grid);

        Integer firstRowContainsNonZero = nonZeroRows.get(0);
        Integer lastRowContainsNonZero = nonZeroRows.get(nonZeroRows.size() - 1);

        Integer firstColumnContainsNonZero = nonZeroColumns.get(0);
        Integer lastColumnContainsNonZero = nonZeroColumns.get(nonZeroColumns.size() - 1);

        int minDistance = Integer.MAX_VALUE;

        for (int row = firstRowContainsNonZero; row <= lastRowContainsNonZero; row++) {
            for (int column = firstColumnContainsNonZero; column <= lastColumnContainsNonZero; column++) {
                int totalDistanceToAllNonZeroPoints = 0;

                for (int pointIndex = 0; pointIndex < nonZeroColumns.size(); pointIndex++) {
                    totalDistanceToAllNonZeroPoints +=
                            Math.abs(row - nonZeroRows.get(pointIndex)) +
                                    Math.abs(column - nonZeroColumns.get(pointIndex));
                }

                minDistance = Math.min(minDistance, totalDistanceToAllNonZeroPoints);
            }
        }

        return minDistance;
    }

    private static List<Integer> getNonZeroRows(int[][] grid) {
        ArrayList<Integer> rows = new ArrayList<>();
        for (int row = 0; row < grid.length; row++) {
            for (int column = 0; column < grid[0].length; column++) {
                if (grid[row][column] == 1) {
                    rows.add(row);
                }
            }
        }
        return rows;
    }

    private static List<Integer> getNonZeroColumns(int[][] grid) {
        ArrayList<Integer> columns = new ArrayList<>();
        for (int column = 0; column < grid[0].length; column++) {
            for (int row = 0; row < grid.length; row++) {
                if (grid[row][column] == 1) {
                    columns.add(column);
                }
            }
        }
        return columns;
    }
}
