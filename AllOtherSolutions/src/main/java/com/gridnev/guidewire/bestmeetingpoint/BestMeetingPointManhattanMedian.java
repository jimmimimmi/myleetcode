package com.gridnev.guidewire.bestmeetingpoint;

import java.util.ArrayList;
import java.util.List;

public class BestMeetingPointManhattanMedian {
    public static int minTotalDistance(int[][] grid) {
        List<Integer> nonZeroColumns = getNonZeroColumns(grid);
        List<Integer> nonZeroRows = getNonZeroRows(grid);

        int bestMeetingPointRow = nonZeroRows.get(nonZeroRows.size() / 2);
        int bestMeetingPointColumn = nonZeroColumns.get(nonZeroColumns.size() / 2);

        int totalDistance = 0;
        for (int pointIndex = 0; pointIndex < nonZeroColumns.size(); pointIndex++) {
            totalDistance +=
                    Math.abs(bestMeetingPointRow - nonZeroRows.get(pointIndex)) +
                            Math.abs(bestMeetingPointColumn - nonZeroColumns.get(pointIndex));
        }

        return totalDistance;
    }

    private static List<Integer> getNonZeroRows(int[][] grid) {
        ArrayList<Integer> rows = new ArrayList<>();
        for (int row = 0; row < grid.length; row++) {
            for (int column = 0; column < grid[0].length; column++) {
                if (grid[row][column] == 1) {
                    rows.add(row);
                }
            }
        }
        return rows;
    }

    private static List<Integer> getNonZeroColumns(int[][] grid) {
        ArrayList<Integer> columns = new ArrayList<>();
        for (int column = 0; column < grid[0].length; column++) {
            for (int row = 0; row < grid.length; row++) {
                if (grid[row][column] == 1) {
                    columns.add(column);
                }
            }
        }
        return columns;
    }
}
