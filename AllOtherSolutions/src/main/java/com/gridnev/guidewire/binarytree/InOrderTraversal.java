package com.gridnev.guidewire.binarytree;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class InOrderTraversal {

    public List<Integer> inOrderRecursive(TreeNode node) {

        ArrayList<Integer> integers = new ArrayList<>();
        inOrderRecursive(node, integers);
        return integers;
    }

    public void inOrderRecursive(TreeNode node, ArrayList<Integer> result) {
        if (node != null) {
            inOrderRecursive(node.left, result);
            result.add(node.val);
            inOrderRecursive(node.right, result);
        }
    }

    public List<Integer> inOrderIterative(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();

        TreeNode current = root;
        while (!stack.empty() || current != null) {
            if (current != null) {
                stack.push(current);
                current = current.left;
            } else if (!stack.empty()) {
                current = stack.pop();
                result.add(current.val);
                current = current.right;
            }
        }

        return result;
    }
}
