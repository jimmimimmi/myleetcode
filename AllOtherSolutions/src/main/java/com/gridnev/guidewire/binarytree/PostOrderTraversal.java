package com.gridnev.guidewire.binarytree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class PostOrderTraversal {

    public List<Integer> postOrderRecursive(TreeNode node) {

        ArrayList<Integer> integers = new ArrayList<>();
        postOrderRecursive(node, integers);
        return integers;
    }

    public void postOrderRecursive(TreeNode node, ArrayList<Integer> result) {
        if (node != null) {
            postOrderRecursive(node.left, result);
            postOrderRecursive(node.right, result);
            result.add(node.val);
        }
    }

    public List<Integer> postOrderIterative(TreeNode root) {
        LinkedList<Integer> result = new LinkedList<>();
        Stack<TreeNode> stack = new Stack<>();

        TreeNode current = root;
        while (!stack.empty() || current != null) {
            if (current != null) {
                result.addFirst(current.val);
                stack.push(current);
                current = current.right;
            } else if (!stack.empty()) {
                current = stack.pop();
                current = current.left;
            }
        }

        return result;
    }
}
