package com.gridnev.guidewire.binarytree;

import java.util.*;

public class PreOrderTravesral {
    public List<Integer> preOrderRecursive(TreeNode node) {

        ArrayList<Integer> integers = new ArrayList<>();
        preOrderRecursive(node, integers);
        return integers;
    }

    public void preOrderRecursive(TreeNode node, ArrayList<Integer> result) {
        if (node != null) {
            result.add(node.val);
            preOrderRecursive(node.left, result);
            preOrderRecursive(node.right, result);
        }
    }

    public List<Integer> preOrderIterative(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();

        TreeNode current = root;
        while (!stack.empty() || current != null) {
            if (current != null) {
                result.add(current.val);
                stack.push(current);
                current = current.left;
            } else if (!stack.empty()) {
                current = stack.pop();
                current = current.right;
            }
        }

        return result;
    }
}
