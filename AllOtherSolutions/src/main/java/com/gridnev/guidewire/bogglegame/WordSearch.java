package com.gridnev.guidewire.bogglegame;

public class WordSearch {
    private static final int[] rowNeighbours = new int[]{0, -1, 0, 1};
    private static final int[] columnNeighbours = new int[]{-1, 0, 1, 0};
    private static final char specialCharToMarkVisitedCells = (char) 1;

    public static boolean exist(char[][] board, String word) {

        if (word == null ||
                word.length() == 0 ||
                board == null ||
                board.length == 0 ||
                board[0].length == 0) {
            return false;
        }

        char[] charArray = word.toCharArray();
        for (int row = 0; row < board.length; row++) {
            for (int column = 0; column < board[0].length; column++) {
                boolean exists = exists(board, row, column, charArray, 0);
                if (exists) {
                    return true;
                }
            }
        }

        return false;
    }

    private static boolean exists(char[][] board, int row, int column, char[] charArray, int index) {
        char currentLetter = board[row][column];

        if (currentLetter != charArray[index]) {
            return false;
        }

        if (index == charArray.length - 1) {
            return true;
        }
        board[row][column] = specialCharToMarkVisitedCells;
        boolean anyOfNeighbourLeadsToSuccess = false;
        for (int i = 0; i < rowNeighbours.length; i++) {
            int nextRow = row + rowNeighbours[i];
            int nextColumn = column + columnNeighbours[i];
            if (nextRow >= 0 &&
                    nextRow < board.length &&
                    nextColumn >= 0 &&
                    nextColumn < board[0].length &&
                    board[nextRow][nextColumn] != specialCharToMarkVisitedCells) {
                boolean neighbourResult = exists(board, nextRow, nextColumn, charArray, index + 1);
                if (neighbourResult) {
                    anyOfNeighbourLeadsToSuccess = true;
                    break;
                }
            }
        }

        board[row][column] = currentLetter;
        return anyOfNeighbourLeadsToSuccess;
    }
}
