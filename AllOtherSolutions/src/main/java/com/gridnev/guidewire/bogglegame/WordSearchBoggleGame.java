package com.gridnev.guidewire.bogglegame;

import java.util.ArrayList;
import java.util.List;

public class WordSearchBoggleGame {

    private static final int[] nextMovementByXAxis = new int[]{0, -1, 0, 1};
    private static final int[] nextMovementByYAxis = new int[]{-1, 0, 1, 0};
    private static final char specialSymbolWhichMarksVisitedCells = (char) 1;

    private static class TrieNode {
        private final TrieNode[] children = new TrieNode[26];
        private final String prefix;
        private boolean isCompletedWord = false;


        private TrieNode(String prefix) {
            this.prefix = prefix;
        }

    }

    private static class TrieFactory {
        public static TrieNode createTrie(String[] strings) {

            TrieNode head = new TrieNode("");
            for (String currentString : strings) {
                if (currentString != null && currentString.length() > 0) {
                    TrieNode current = head;
                    for (int i = 0; i < currentString.length(); i++) {
                        char currentChar = currentString.charAt(i);
                        TrieNode child = current.children[getAlphabeticOffset(currentChar)];
                        if (child == null) {
                            child = new TrieNode(current.prefix + currentChar);
                            current.children[getAlphabeticOffset(currentChar)] = child;
                        }
                        if (i == currentString.length() - 1) {
                            child.isCompletedWord = true;
                        }
                        current = child;
                    }
                }
            }

            return head;
        }
    }

    public static List<String> findWords(char[][] board, String[] words) {
        TrieNode trie = TrieFactory.createTrie(words);
        ArrayList<String> result = new ArrayList<String>();

        for (int row = 0; row < board.length; row++) {
            for (int column = 0; column < board[0].length; column++) {
                char currentChar = board[row][column];
                TrieNode child = trie.children[getAlphabeticOffset(currentChar)];
                if (child != null) {
                    depthSearchFromParticularCell(board, row, column, child, result);
                }
            }
        }
        return result;
    }

    private static void depthSearchFromParticularCell(char[][] board, int row, int column, TrieNode trie, List<String> result) {

        TrieNode currentNode = trie;
        if (currentNode.isCompletedWord) {
            currentNode.isCompletedWord = false;
            result.add(currentNode.prefix);
        }

        char currentLetter = board[row][column];
        board[row][column] = specialSymbolWhichMarksVisitedCells;
        for (int i = 0; i < nextMovementByXAxis.length; i++) {
            int nextRow = row + nextMovementByXAxis[i];
            int nextColumn = column + nextMovementByYAxis[i];
            if (nextRow >= 0 &&
                    nextRow < board.length &&
                    nextColumn >= 0 &&
                    nextColumn < board[0].length &&
                    board[nextRow][nextColumn] != specialSymbolWhichMarksVisitedCells) {
                TrieNode child = currentNode.children[getAlphabeticOffset(board[nextRow][nextColumn])];
                if (child != null) {
                    depthSearchFromParticularCell(board, nextRow, nextColumn, child, result);
                }
            }
        }
        board[row][column] = currentLetter;
    }

    private static int getAlphabeticOffset(char c) {
        return c - 'a';
    }
}
