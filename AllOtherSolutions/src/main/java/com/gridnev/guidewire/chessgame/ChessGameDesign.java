package com.gridnev.guidewire.chessgame;

import java.util.List;

public class ChessGameDesign {
    enum Color {WHITE, BLACK}

    enum PieceType {PAWN, ROOK, KNIGHT, BISHOP, KING, QUEEN}

    class Piece {
        Color color;
        PieceType pieceType;
    }

    class Coordinate {
        final int rowNumber = 0;
        final int columnNumber = 0;
    }

    class Point {
        Piece piece = null;
        final Coordinate coordinate = null;
    }

    class Board {
        List<Point> points;
    }

    class Movement {
        Coordinate from;
        Coordinate to;
        Color color;
    }

    class History {
        List<Movement> allConsecutiveMovements;
    }


    class PlayerMovesPieceWithRespectedColorValidator {
        boolean isItValidMovement(Color color, Board board, Coordinate from, Coordinate to) {
            return false;
        }
    }

    class MovementUnderCheckValidator {
        boolean isItValidMovement(Board board, Coordinate from, Coordinate to) {
            return false;
        }
    }

    class MovementAccordingBasicRulesValidator {
        EnPassantChecker enPassantChecker;

        boolean IsItValidMovement(Board board, History history, Coordinate from, Coordinate to) {
            return false;
        }
    }

    class RespectedColorTurnValidator {
        boolean canThatColorMoveNow(Color color, History history) {
            return false;
        }
    }

    class EnPassantChecker {
        //canPawnCaptureAnotherPawnBecauseOfEnPassant
        public Point getCapturedPawn(
                Board board, Color pawnColor, Coordinate from, Coordinate to, History history) {
            return null; // null means non valid movement from from to to
        }
    }

    class MovementValidator {
        RespectedColorTurnValidator respectedColorTurnValidator;
        PlayerMovesPieceWithRespectedColorValidator playerMovesPieceWithRespectedColorValidator;
        MovementAccordingBasicRulesValidator movementAccordingBasicRulesValidator;
        MovementUnderCheckValidator movementUnderCheckValidator;

        boolean isItValidMove(Color color, Coordinate from, Coordinate to, Board board, History history) {
            return false;
        }
    }


    class CheckMateChecker {
        boolean isItCheckMate(Board board, Color colorOfKing) {
            return false;
        }
    }

    class StaleMateChecker {
        boolean isItStaleMate(Board board, Color playerToMove) {
            return false;
        }
    }

    class GameState {
        boolean isGameOver;
        Color winner = null;
    }

    class MovementApplier {
        //updates board internal state
        //updates history
        void move(Color playerColor, Coordinate from, Coordinate to, Board board, History history) {

        }
    }

    class GameStateManager {
        CheckMateChecker checkMateChecker;
        StaleMateChecker staleMateChecker;

        void updateGameState(GameState gameState, Board board, History history) {
        }
    }

    class Initializer {
        Board createInitializedBoard() {
            return null;
        }
    }

    class Game {
        GameState gameState;
        History history;
        Board board;
        MovementValidator movementValidator;
        MovementApplier movementApplier;
        GameStateManager gameStateManager;
        Initializer initializer;

        void initialize() {
            initializer.createInitializedBoard();
        }

        void move(Color playerColor, Coordinate from, Coordinate to) {
            if (movementValidator.isItValidMove(playerColor, from, to, board, history)) {
                movementApplier.move(playerColor, from, to, board, history);
                gameStateManager.updateGameState(gameState, board, history);
            }
        }

        void move(Color playerColor, Coordinate from, Coordinate to, PieceType newPieceType) {
        }

        void castlingExchange(Color playerColor, Coordinate[] pointsToExchange) {
        }

        void resign(Color playerColor) {
        }
    }
}
