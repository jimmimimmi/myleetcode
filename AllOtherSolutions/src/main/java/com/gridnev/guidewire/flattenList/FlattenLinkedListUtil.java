package com.gridnev.guidewire.flattenList;

import java.util.ArrayList;
import java.util.List;

public class FlattenLinkedListUtil {
    public static Node createList(int arr[]) {
        Node head = null;
        Node currentNode = null;

        for (int i = 0; i < arr.length; ++i) {
            Node newNode = new Node(arr[i]);
            if (head == null) {
                head = newNode;
                currentNode = head;
            } else {
                currentNode.setNext(newNode);
                currentNode = newNode;
            }
        }
        return head;
    }

    public static List<Integer> printList(Node node) {
        ArrayList<Integer> result = new ArrayList<>();
        while (node != null) {
            System.out.print(node.getData() + " ");
            result.add(node.getData());
            node = node.getNext();
        }
        System.out.println("");
        return result;
    }
}
