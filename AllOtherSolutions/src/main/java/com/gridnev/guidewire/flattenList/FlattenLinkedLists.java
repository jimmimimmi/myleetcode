package com.gridnev.guidewire.flattenList;

import java.util.LinkedList;
import java.util.Queue;

public class FlattenLinkedLists {
    public static void flattenListNoQueue(Node head) {

        if (head == null || (head.getChild() == null && head.getNext() == null)) {
            return;
        }


        Node lastNode = head;

        while (lastNode.getNext() != null) {
            lastNode = lastNode.getNext();
        }

        Node currentNode = new Node(-1);
        currentNode.setNext(head);

        while (currentNode != lastNode) {
            currentNode = currentNode.getNext();
            Node child = currentNode.getChild();
            if (child != null) {
                lastNode.setNext(child);

                while (lastNode.getNext() != null) {
                    lastNode = lastNode.getNext();
                }
                currentNode.setChild(null);
            }
        }

    }

    public static void flattenListWithQueue(Node head) {

        if (head == null || (head.getChild() == null && head.getNext() == null)) {
            return;
        }

        Queue<Node> queue = new LinkedList<>();
        queue.offer(head);
        while (!queue.isEmpty()) {
            Node current = queue.poll();
            Node previous = current;
            while (current != null) {
                Node currentChild = current.getChild();
                if (currentChild != null) {
                    queue.offer(currentChild);
                    current.setChild(null);
                }
                previous = current;
                current = current.getNext();
            }
            if (!queue.isEmpty()) {
                previous.setNext(queue.peek());
            }
        }
    }
}
