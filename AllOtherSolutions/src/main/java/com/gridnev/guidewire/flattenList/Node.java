package com.gridnev.guidewire.flattenList;

public class Node {
    private final int data;
    private Node next;
    private Node child;

    public Node(int d) {
        data = d;
    }

    public int getData() {
        return data;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node getChild() {
        return child;
    }

    public void setChild(Node child) {
        this.child = child;
    }
}
