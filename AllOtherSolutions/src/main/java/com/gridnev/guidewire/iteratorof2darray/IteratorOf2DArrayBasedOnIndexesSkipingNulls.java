package com.gridnev.guidewire.iteratorof2darray;

import java.util.Iterator;

public class IteratorOf2DArrayBasedOnIndexesSkipingNulls<T> implements Iterator<T> {


    private T[][] initialArray;

    private int rowIndex = -1;
    private int columnIndex = -1;

    public IteratorOf2DArrayBasedOnIndexesSkipingNulls(T[][] initialArray) {
        this.initialArray = initialArray;
        if (initialArray != null && initialArray.length != 0) {
            rowIndex = 0;
            columnIndex = 0;
        }
    }

    @Override
    public boolean hasNext() {
        if (rowIndex < 0 || rowIndex >= initialArray.length) {
            return false;
        }

        while (rowIndex < initialArray.length) {
            if (initialArray[rowIndex] == null ||
                    initialArray[rowIndex].length == 0 ||
                    columnIndex >= initialArray[rowIndex].length) {
                rowIndex++;
                columnIndex = 0;
                continue;
            }

            if (initialArray[rowIndex][columnIndex] == null) {
                columnIndex++;
                continue;
            }

            return true;
        }
        return false;
    }

    @Override
    public T next() {
        if (hasNext()) {
            T result = initialArray[rowIndex][columnIndex];
            columnIndex++;
            return result;
        }
        throw new IllegalStateException();
    }
}
