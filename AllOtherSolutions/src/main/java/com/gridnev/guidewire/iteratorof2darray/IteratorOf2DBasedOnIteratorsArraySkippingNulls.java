package com.gridnev.guidewire.iteratorof2darray;

import java.util.*;
import java.util.function.Consumer;


public class IteratorOf2DBasedOnIteratorsArraySkippingNulls<T> implements Iterator<T> {
    private Iterator<Collection<T>> rowIterator;
    private Iterator<T> columnIterator;
    private T valueToBeNext = null;

    public IteratorOf2DBasedOnIteratorsArraySkippingNulls(T[][] initialArray) {

        if (initialArray == null || initialArray.length == 0) {
            return;
        }

        Collection<Collection<T>> collectionOfCollections = new ArrayList<>(initialArray.length);

        for (T[] row : initialArray) {
            if (row == null) {
                collectionOfCollections.add(null);
                continue;
            }

            Collection<T> rowCollection = new ArrayList<>();
            collectionOfCollections.add(rowCollection);
            rowCollection.addAll(Arrays.asList(row));

        }

        initialize(collectionOfCollections);

    }

    public IteratorOf2DBasedOnIteratorsArraySkippingNulls(Collection<Collection<T>> collectionOfCollections) {
        if (collectionOfCollections == null || collectionOfCollections.isEmpty()) {
            return;
        }
        initialize(collectionOfCollections);
    }

    private void initialize(Collection<Collection<T>> collectionOfCollections) {
        rowIterator = collectionOfCollections.iterator();
        moveColumnIteratorToBeginningOfNextNotNullRow();
    }

    private boolean moveColumnIteratorToBeginningOfNextNotNullRow() {
        if (columnIterator == null || !columnIterator.hasNext()) {
            columnIterator = null;
            Collection<T> nextNonNullRow = null;
            while (rowIterator.hasNext() && nextNonNullRow == null) {
                nextNonNullRow = rowIterator.next();
            }
            if (nextNonNullRow != null) {
                columnIterator = nextNonNullRow.iterator();
            }
        }
        return columnIterator != null;
    }


    public boolean hasNext() {
        if (rowIterator == null) {
            return false;
        }

        if (valueToBeNext != null) {
            return true;
        }

        while (valueToBeNext == null) {
            boolean foundNextNotNullRow = moveColumnIteratorToBeginningOfNextNotNullRow();
            if (!foundNextNotNullRow) {
                return false;
            }

            if (columnIterator.hasNext()) {
                valueToBeNext = columnIterator.next();
            }
        }


        return true;
    }

    public T next() throws NoSuchElementException {
        if (hasNext()) {
            T result = this.valueToBeNext;
            this.valueToBeNext = null;
            return result;
        }
        throw new NoSuchElementException();
    }

    public void remove() {
    }

    public void forEachRemaining(Consumer<? super T> action) {
    }
}
