package com.gridnev.guidewire.iteratorof2darray;

import java.util.*;
import java.util.function.Consumer;


public class IteratorOfTwoDimensionalArray<T> implements Iterator<T> {
    private Iterator<Collection<T>> rowIterator;
    private Iterator<T> columnIterator;

    public IteratorOfTwoDimensionalArray(T[][] initialArray) {

        if (initialArray == null || initialArray.length == 0) {
            return;
        }

        Collection<Collection<T>> collectionOfCollections = new ArrayList<>(initialArray.length);

        for (T[] row : initialArray) {
            Collection<T> rowCollection = new ArrayList<>();
            collectionOfCollections.add(rowCollection);
            rowCollection.addAll(Arrays.asList(row));
        }

        initialize(collectionOfCollections);

    }

    public IteratorOfTwoDimensionalArray(Collection<Collection<T>> collectionOfCollections) {
        if (collectionOfCollections == null || collectionOfCollections.isEmpty()) {
            return;
        }
        initialize(collectionOfCollections);
    }

    private void initialize(Collection<Collection<T>> collectionOfCollections) {
        rowIterator = collectionOfCollections.iterator();
        if (rowIterator.hasNext()) {
            columnIterator = rowIterator.next().iterator();
        }
    }


    public boolean hasNext() {
        if (columnIterator.hasNext()) {
            return true;
        }
        while (rowIterator.hasNext()) {
            columnIterator = rowIterator.next().iterator();
            if (columnIterator.hasNext()) {
                return true;
            }
        }
        return false;
    }

    public T next() throws NoSuchElementException {
        if (hasNext()) {
            return columnIterator.next();
        }
        throw new NoSuchElementException();
    }

    public void remove() {
    }

    public void forEachRemaining(Consumer<? super T> action) {
    }
}
