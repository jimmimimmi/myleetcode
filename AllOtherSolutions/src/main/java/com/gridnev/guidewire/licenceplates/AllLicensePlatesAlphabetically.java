package com.gridnev.guidewire.licenceplates;

import java.util.ArrayList;
import java.util.List;

public class AllLicensePlatesAlphabetically {

    public static List<String> generateAllPlates(int lettersAmount, int numbersAmount) {

        int[] maximumValues = new int[lettersAmount + numbersAmount];
        for (int i = 0; i < lettersAmount; i++) {
            maximumValues[i] = 25;
        }

        for (int i = lettersAmount; i < maximumValues.length; i++) {
            maximumValues[i] = 9;
        }

        int[] currentAlphanumericPlateRepresentation = new int[maximumValues.length];
        currentAlphanumericPlateRepresentation[currentAlphanumericPlateRepresentation.length - 1] = -1;

        int totalAmountOfPossiblePlates = (int) (Math.pow(26, lettersAmount) * Math.pow(10, numbersAmount));

        int currentAmountOfGeneratedPlates = 0;
        ArrayList<String> result = new ArrayList<>(totalAmountOfPossiblePlates);

        while (currentAmountOfGeneratedPlates < totalAmountOfPossiblePlates) {
            currentAmountOfGeneratedPlates++;
            incrementParticularIndex(maximumValues, currentAlphanumericPlateRepresentation, currentAlphanumericPlateRepresentation.length - 1);
            String plateNumber = getPlateNumber(maximumValues, currentAlphanumericPlateRepresentation);
            System.out.println("generated " + currentAmountOfGeneratedPlates + "plates. the last one is " + plateNumber);
            result.add(plateNumber);
        }
        return result;
    }

    private static String getPlateNumber(int[] maximumValues, int[] currentIndexes) {

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < currentIndexes.length; i++) {
            int currentIndex = currentIndexes[i];

            char currentChar = maximumValues[i] == 9 ?
                    (char) ('0' + currentIndex) :
                    (char) ('A' + currentIndex);
            stringBuilder.append(currentChar);
        }
        return stringBuilder.toString();
    }

    private static void incrementParticularIndex(int[] maximumValues, int[] currentIndexes, int indexToIncrement) {
        int nextExpectedValue = currentIndexes[indexToIncrement] + 1;
        if (nextExpectedValue <= maximumValues[indexToIncrement]) {
            currentIndexes[indexToIncrement] = nextExpectedValue;
            return;
        } else {
            if (indexToIncrement > 0) {
                currentIndexes[indexToIncrement] = 0;
                incrementParticularIndex(maximumValues, currentIndexes, indexToIncrement - 1);
            } else {
                throw new IllegalStateException("Overflowing");
            }
        }

    }
}
