package com.gridnev.guidewire.preparationtotest;

import java.util.Stack;

public class EvaluateReversePolishNotation {
    public int evalRPN(String[] tokens) {

        if (tokens == null || tokens.length == 0) {
            return 0;
        }
        Stack<Integer> stack = new Stack<Integer>();

        for (String token : tokens) {
            if (token.equals("+")) {
                Integer second = stack.pop();
                Integer first = stack.pop();
                Integer result = first + second;
                stack.push(result);
                continue;
            }

            if (token.equals("-")) {
                Integer second = stack.pop();
                Integer first = stack.pop();
                Integer result = first - second;
                stack.push(result);
                continue;
            }

            if (token.equals("/")) {
                Integer second = stack.pop();
                Integer first = stack.pop();
                Integer result = first / second;
                stack.push(result);
                continue;
            }

            if (token.equals("*")) {
                Integer second = stack.pop();
                Integer first = stack.pop();
                Integer result = first * second;
                stack.push(result);
                continue;
            }

            int currentValue = Integer.parseInt(token);
            stack.push(currentValue);
        }

        return stack.pop();
    }
}
