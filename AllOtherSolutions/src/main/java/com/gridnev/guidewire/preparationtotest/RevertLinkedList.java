package com.gridnev.guidewire.preparationtotest;

public class RevertLinkedList {
    public class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    public ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }

        ListNode headNext = head.next;
        if (headNext.next == null) {
            headNext.next = head;
            head.next = null;
            return headNext;
        }

        ListNode previous = head;
        ListNode current = headNext;

        while (current != null) {
            ListNode next = current.next;
            current.next = previous;
            previous = current;
            current = next;
        }
        head.next = null;
        return previous;
    }
}
