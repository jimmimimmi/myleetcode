package com.gridnev.guidewire.preparationtotest;

import java.util.ArrayList;
import java.util.List;

public class SpiralMatrix {
    public static List<Integer> spiralOrder(int[][] matrix) {

        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return new ArrayList<>();
        }
        int leftColumn = 0;
        int rightColumn = matrix[0].length - 1;
        int topRow = 0;
        int bottomRow = matrix.length - 1;

        int[][] directionMovements = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

        int directionNumber = 0;
        int movingRight = 0;
        int movingDown = 1;
        int movingLeft = 2;
        int movingUp = 3;

        ArrayList<Integer> result = new ArrayList<Integer>();
        int currentRow = 0;
        int currentColumn = 0;
        result.add(matrix[currentRow][currentColumn]);
        int totalElements = matrix.length * matrix[0].length;

        while (result.size() < totalElements) {
            while (true) {
                int nextRow = currentRow + directionMovements[directionNumber][0];
                int nextColumn = currentColumn + directionMovements[directionNumber][1];

                if (nextRow >= topRow && nextRow <= bottomRow && nextColumn >= leftColumn && nextColumn <= rightColumn) {
                    currentRow = nextRow;
                    currentColumn = nextColumn;
                    break;
                } else {
                    if (directionNumber == movingRight) {
                        topRow++;
                    }
                    if (directionNumber == movingDown) {
                        rightColumn--;
                    }
                    if (directionNumber == movingLeft) {
                        bottomRow--;
                    }
                    if (directionNumber == movingUp) {
                        leftColumn++;
                    }
                    directionNumber = (directionNumber + 1) % 4;
                }
            }
            result.add(matrix[currentRow][currentColumn]);
        }

        return result;
    }
}
