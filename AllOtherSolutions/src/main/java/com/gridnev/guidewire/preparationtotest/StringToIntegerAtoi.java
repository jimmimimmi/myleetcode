package com.gridnev.guidewire.preparationtotest;

public class StringToIntegerAtoi {
    public int myAtoi(String str) {

        if (str == null || str.length() == 0) {
            return 0;
        }
        String trimmedString = str.trim();
        if (trimmedString.length() == 0) {
            return 0;
        }
        Boolean sign = null;
        Integer indexOfFirstNumber = null;
        Integer indexOfLastNumber = null;


        char[] chars = trimmedString.toCharArray();
        if (chars[0] == '+') {
            sign = true;
        }
        if (chars[0] == '-') {
            sign = false;
        }

        for (int i = sign == null ? 0 : 1; i < chars.length; i++) {
            char aChar = chars[i];
            if (aChar - '0' >= 0 && aChar - '0' <= 9) {
                if (indexOfFirstNumber == null) {
                    indexOfFirstNumber = i;
                }
                indexOfLastNumber = i;
                continue;
            } else {
                break;
            }
        }

        if (indexOfFirstNumber == null) {
            return 0;
        }

        sign = sign == null ? true : sign;
        long totalNumber = 0L;
        for (int i = indexOfFirstNumber; i <= indexOfLastNumber; i++) {
            totalNumber = totalNumber * 10 + Integer.parseInt("" + chars[i]);

            if (totalNumber > Integer.MAX_VALUE) {
                return sign ? Integer.MAX_VALUE : Integer.MIN_VALUE;
            }
        }
        return (int) totalNumber * (sign ? 1 : -1);

    }
}
