package com.gridnev.guidewire.preparationtotest;

public class Temperatures {
    private static final String[] SEASONS = new String[]{"WINTER", "SPRING", "SUMMER", "AUTUMN"};
    public String solution(int[] T) {
        // write your code in Java SE 8

        int amountOfMeasurementsWithinSeason = T.length / 4;
        int maxAmplitude = Integer.MIN_VALUE;
        String winner = null;

        for (int indexOfSeason = 0; indexOfSeason < SEASONS.length; indexOfSeason++) {
            String season = SEASONS[indexOfSeason];
            int offsetStart = indexOfSeason * amountOfMeasurementsWithinSeason;
            int offsetEnd = offsetStart + amountOfMeasurementsWithinSeason;
            int amplitude = getDifferenceBetweenMaxAndMin(T, offsetStart, offsetEnd);

            if (amplitude > maxAmplitude) {
                maxAmplitude = amplitude;
                winner = season;
            }
        }

        return winner;
    }

    private int getDifferenceBetweenMaxAndMin(int[] initialArray, int from, int to) {
        int maximum = Integer.MIN_VALUE;
        int minimum = Integer.MAX_VALUE;
        for (int i = from; i < to; i++) {
            int temperature = initialArray[i];
            maximum = temperature > maximum ? temperature : maximum;
            minimum = temperature < minimum ? temperature : minimum;
        }

        return Math.abs(maximum - minimum);
    }
}
