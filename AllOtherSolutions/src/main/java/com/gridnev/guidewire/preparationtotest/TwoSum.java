package com.gridnev.guidewire.preparationtotest;

import java.util.HashMap;

public class TwoSum {
    public int[] twoSum(int[] nums, int target) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int index = 0; index < nums.length; index++) {
            int currentValue = nums[index];
            int desiredValue = target - currentValue;

            Integer indexOfDesiredValue = map.get(desiredValue);
            if (indexOfDesiredValue != null) {
                return new int[]{indexOfDesiredValue + 1, index + 1};
            }
            map.put(currentValue, index);
        }
        return null;
    }
}
