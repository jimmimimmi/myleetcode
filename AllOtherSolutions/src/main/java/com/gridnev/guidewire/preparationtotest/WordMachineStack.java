package com.gridnev.guidewire.preparationtotest;

import java.util.Stack;

public class WordMachineStack {

    private static final int MAX_VALUE_EXCLUSIVE = 1 << 20; // (int) Math.pow(2,20)
    private static final int ERROR_VALUE = -1;

    public int solution(String S) {
        // write your code in Java SE 8

        if (S == null || S.length() == 0) {
            return ERROR_VALUE;
        }
        String[] operations = splitByWhitespaces(S);

        Stack<Integer> stack = new Stack<Integer>();

        for (int i = 0; i < operations.length; i++) {
            String operation = operations[i];

            if (operation.equals("POP")) {
                if (stack.empty()) {
                    return ERROR_VALUE;
                }
                stack.pop();
                continue;
            }

            if (operation.equals("DUP")) {
                if (stack.empty()) {
                    return ERROR_VALUE;
                }
                stack.push(stack.peek());
                continue;
            }

            if (operation.equals("+")) {
                if (stack.empty()) {
                    return ERROR_VALUE;
                }
                Integer firstValue = stack.pop();
                if (stack.empty()) {
                    return ERROR_VALUE;
                }
                Integer secondValue = stack.pop();

                int newValue = firstValue + secondValue;

                if (!isItCorrectValue(newValue)) {
                    return ERROR_VALUE;
                }
                stack.push(newValue);
                continue;
            }

            if (operation.equals("-")) {
                if (stack.empty()) {
                    return ERROR_VALUE;
                }
                Integer firstValue = stack.pop();
                if (stack.empty()) {
                    return ERROR_VALUE;
                }
                Integer secondValue = stack.pop();

                int newValue = firstValue - secondValue;

                if (!isItCorrectValue(newValue)) {
                    return ERROR_VALUE;
                }
                stack.push(newValue);
                continue;
            }

            try {
                int valueToPush = Integer.parseInt(operation);
                if (!isItCorrectValue(valueToPush)) {
                    return ERROR_VALUE;
                }
                stack.push(valueToPush);
            } catch (NumberFormatException e) {
                return ERROR_VALUE;
            }
        }

        return stack.empty() ? ERROR_VALUE : stack.pop();
    }


    private boolean isItCorrectValue(int value) {
        return value >= 0 && value < MAX_VALUE_EXCLUSIVE;
    }

    private String[] splitByWhitespaces(String s) {
        return s.split("\\s+");
    }

}
