package com.gridnev.guidewire.sorting;

import java.util.Comparator;
import java.util.PriorityQueue;

public class SortLinkedList {
    public static LinkedListNode sortList(LinkedListNode head) {
        if (head == null || head.getNext() == null) {
            return head;
        }

        LinkedListNode secondSublist = splitListInTwo(head);
        LinkedListNode sortedFirstSublist = sortList(head);
        LinkedListNode sortedSecondSublist = sortList(secondSublist);

        return mergeTwoSortedLists(sortedFirstSublist, sortedSecondSublist);

    }

    public static LinkedListNode splitListInTwo(LinkedListNode head) {
        LinkedListNode slowPointer = head;
        LinkedListNode fastPointer = head;
        LinkedListNode lastPointerOfFirstSublist = head;

        while (fastPointer != null && fastPointer.getNext() != null) {
            lastPointerOfFirstSublist = slowPointer;
            slowPointer = slowPointer.getNext();
            fastPointer = fastPointer.getNext().getNext();
        }
        lastPointerOfFirstSublist.setNext(null);
        return slowPointer;
    }

    public static LinkedListNode mergeTwoSortedLists(LinkedListNode leftHead, LinkedListNode rightHead) {
        LinkedListNode artificialHead = new LinkedListNode(0);
        LinkedListNode current = artificialHead;

        LinkedListNode left = leftHead;
        LinkedListNode right = rightHead;

        PriorityQueue<LinkedListNode> linkedListNodes = new PriorityQueue<>(Comparator.comparingInt(LinkedListNode::getValue));

        while (left != null && right != null) {
            if (left.getValue() < right.getValue()) {
                current.setNext(left);
                left = left.getNext();
            } else {
                current.setNext(right);
                right = right.getNext();
            }
            current = current.getNext();
        }

        if (left != null) {
            current.setNext(left);
        }

        if (right != null) {
            current.setNext(right);
        }
        return artificialHead.getNext();
    }
}
