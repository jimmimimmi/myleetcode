package com.gridnev.guidewire.sparsematrix;

import java.util.HashMap;
import java.util.Map;

public class SparseMatrixBasedOnHashmap<T> {

    private Map<Integer, Map<Integer, T>> innerMap;
    private int rowsAmount = 0;
    private int columnsAmount = 0;

    public SparseMatrixBasedOnHashmap(T[][] matrix) {
        innerMap = new HashMap<>();

        if (matrix == null || matrix.length <= 0) {
            return;
        }

        rowsAmount = matrix.length;
        for (int row = 0; row < matrix.length; row++) {
            if (matrix[row] == null || matrix[row].length <= 0) {
                continue;
            }
            columnsAmount = Math.max(columnsAmount, matrix[row].length);
            HashMap<Integer, T> columnsMapForParticularRow = new HashMap<>();
            innerMap.put(row, columnsMapForParticularRow);
            for (int column = 0; column < matrix[row].length; column++) {
                columnsMapForParticularRow.put(column, matrix[row][column]);
            }
        }
    }

    public T get(int row, int column) {
        if (row < 0 || row > rowsAmount - 1 || column < 0 || column > columnsAmount - 1)
            throw new IllegalArgumentException();

        Map<Integer, T> columnMapForParticularRow = innerMap.get(row);
        if (columnMapForParticularRow == null) {
            return null;
        }
        return columnMapForParticularRow.get(column);
    }

    public void set(int row, int column, T value) {
        if (row < 0 || row > rowsAmount - 1 || column < 0 || column > columnsAmount - 1)
            throw new IllegalArgumentException();

        Map<Integer, T> columnMapForParticularRow = innerMap.get(row);
        if (columnMapForParticularRow == null) {
            columnMapForParticularRow = new HashMap<>();
            innerMap.put(row, columnMapForParticularRow);
        }

        columnMapForParticularRow.put(column, value);
    }
}
