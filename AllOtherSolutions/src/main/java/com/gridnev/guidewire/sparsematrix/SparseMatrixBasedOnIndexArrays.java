package com.gridnev.guidewire.sparsematrix;

import java.util.ArrayList;
import java.util.Arrays;

public class SparseMatrixBasedOnIndexArrays<T> {

    private int[] amountOfNonNullValuesUpToParticularRowExclusive;
    private ArrayList<Integer> columns;
    private ArrayList<T> values;

    public SparseMatrixBasedOnIndexArrays(T[][] matrix) {
        amountOfNonNullValuesUpToParticularRowExclusive = new int[matrix.length + 1];
        columns = new ArrayList<>();
        values = new ArrayList<>();

        int amountOfValues = 0;
        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[0].length; column++) {
                if (matrix[row][column] != null) {
                    columns.add(column);
                    values.add(matrix[row][column]);
                    amountOfValues++;
                }
            }
            amountOfNonNullValuesUpToParticularRowExclusive[row + 1] = amountOfValues;
        }
    }

    public T get(int row, int column) {
        int amountOfNonNullValuesWithinRow =
                amountOfNonNullValuesUpToParticularRowExclusive[row + 1] -
                        amountOfNonNullValuesUpToParticularRowExclusive[row];
        if (amountOfNonNullValuesWithinRow == 0) {
            return null;
        }

        int searchedIndex = Arrays.binarySearch(
                columns.subList(
                        amountOfNonNullValuesUpToParticularRowExclusive[row],
                        amountOfNonNullValuesUpToParticularRowExclusive[row + 1])
                        .toArray(), column);
        return searchedIndex < 0 ?
                null :
                values.get(searchedIndex + amountOfNonNullValuesUpToParticularRowExclusive[row]);
    }

    public void set(int row, int column, T value) {
        int amountOfNonNullValuesWithinRow =
                amountOfNonNullValuesUpToParticularRowExclusive[row + 1] -
                        amountOfNonNullValuesUpToParticularRowExclusive[row];
        if (amountOfNonNullValuesWithinRow == 0) {
            if (value == null) {
                return;
            }
            columns.add(amountOfNonNullValuesUpToParticularRowExclusive[row], column);
            values.add(amountOfNonNullValuesUpToParticularRowExclusive[row], value);

            for (int rowAfterParticular = row + 1;
                 rowAfterParticular < amountOfNonNullValuesUpToParticularRowExclusive.length;
                 rowAfterParticular++) {
                amountOfNonNullValuesUpToParticularRowExclusive[rowAfterParticular]++;
            }
            return;
        }


        int searchedIndex = Arrays.binarySearch(
                columns.subList(
                        amountOfNonNullValuesUpToParticularRowExclusive[row],
                        amountOfNonNullValuesUpToParticularRowExclusive[row + 1])
                        .toArray(), column);

        if (searchedIndex < 0) {
            if (value == null) {
                return;
            }
            int indexForNewValueInsertion = -(searchedIndex + 1) + amountOfNonNullValuesUpToParticularRowExclusive[row];
            columns.add(indexForNewValueInsertion, column);
            values.add(indexForNewValueInsertion, value);
            for (int rowForUpdateOfTotalAmount = row + 1;
                 rowForUpdateOfTotalAmount < amountOfNonNullValuesUpToParticularRowExclusive.length;
                 rowForUpdateOfTotalAmount++) {
                amountOfNonNullValuesUpToParticularRowExclusive[rowForUpdateOfTotalAmount]++;
            }
            return;
        }

        int indexForUpdating = searchedIndex + amountOfNonNullValuesUpToParticularRowExclusive[row];
        if (value != null) {
            values.set(indexForUpdating, value);
            return;
        }

        values.remove(indexForUpdating);
        columns.remove(indexForUpdating);
        for (int rowForUpdateOfTotalAmount = row + 1;
             rowForUpdateOfTotalAmount < amountOfNonNullValuesUpToParticularRowExclusive.length;
             rowForUpdateOfTotalAmount++) {
            amountOfNonNullValuesUpToParticularRowExclusive[rowForUpdateOfTotalAmount]--;
        }
    }
}
