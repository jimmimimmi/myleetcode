package com.gridnev.intervals;

import java.util.ArrayList;
import java.util.List;

public class Interval {
    int start;
    int end;

    Interval() {
        start = 0;
        end = 0;
    }

    Interval(int s, int e) {
        start = s;
        end = e;
    }

    @Override
    public String toString() {
        return "Interval{" +
                "start=" + start +
                ", end=" + end +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Interval interval = (Interval) o;

        if (start != interval.start) return false;
        return end == interval.end;
    }

    @Override
    public int hashCode() {
        int result = start;
        result = 31 * result + end;
        return result;
    }

    public static List<Interval> create(int[]... intervals) {

        ArrayList<Interval> result = new ArrayList<>();
        for (int i = 0; i < intervals.length; i++) {
            int[] givenInterval = intervals[i];
            Interval interval = new Interval();
            interval.start = givenInterval[0];
            interval.end = givenInterval[1];
            result.add(interval);
        }
        return result;
    }
}
