package com.gridnev.intervals;

import java.util.*;

public class MergeIntervals {
    public static class UnionFindSolution {
        private HashMap<Interval, List<Interval>> graph;

        private HashMap<Interval, List<Interval>> buildGraph(List<Interval> intervals) {
            HashMap<Interval, List<Interval>> graph = new HashMap<>();
            for (Interval interval : intervals) {
                ArrayList<Interval> adjacentIntervals = new ArrayList<>();
                for (Interval anotherInterval : intervals) {
                    if (interval != anotherInterval &&
                            isThereIntersection(interval, anotherInterval)) {
                        adjacentIntervals.add(anotherInterval);
                    }
                }
                graph.put(interval, adjacentIntervals);
            }
            return graph;
        }

        private boolean isThereIntersection(Interval one, Interval two) {
            return one.start <= two.end && two.start <= one.end;
        }

        private List<List<Interval>> group(HashMap<Interval, List<Interval>> graph) {
            HashSet<Interval> visited = new HashSet<>();
            HashMap<Integer, List<Interval>> groups = new HashMap<>();
            int groupIndex = 0;
            for (Interval interval : graph.keySet()) {
                if (!visited.contains(interval)) {
                    groupIndex++;
                    groups.put(groupIndex, new ArrayList<>());
                    Queue<Interval> queue = new LinkedList<>();
                    queue.offer(interval);
                    while (!queue.isEmpty()) {
                        Interval currentInterval = queue.poll();
                        if (!visited.contains(currentInterval)) {
                            visited.add(currentInterval);
                            groups.get(groupIndex).add(currentInterval);
                            graph.get(currentInterval).forEach(queue::offer);
                        }
                    }
                }
            }

            ArrayList<List<Interval>> result = new ArrayList<>();
            for (List<Interval> intervals : groups.values()) {
                result.add(intervals);
            }
            return result;

        }

        private Interval mergeIntersected(List<Interval> intervals) {
            Interval result = new Interval();
            result.end = intervals.get(0).end;
            result.start = intervals.get(0).start;

            for (Interval interval : intervals) {
                if (interval.start < result.start) {
                    result.start = interval.start;
                }
                if (interval.end > result.end) {
                    result.end = interval.end;
                }
            }
            return result;
        }

        public List<Interval> merge(List<Interval> intervals) {
            if (intervals == null || intervals.size() <= 1) {
                return intervals;
            }

            HashMap<Interval, List<Interval>> graph = buildGraph(intervals);
            List<List<Interval>> groups = group(graph);
            ArrayList<Interval> result = new ArrayList<>();

            for (List<Interval> group : groups) {
                Interval interval = mergeIntersected(group);
                result.add(interval);
            }

            Collections.sort(result, Comparator.comparingInt(o -> o.start));

            return result;
        }
    }

    public static class SortingSolution {
        public List<Interval> merge(List<Interval> intervals) {
            if (intervals == null || intervals.size() <= 1) {
                return intervals;
            }

            Collections.sort(intervals, Comparator.comparingInt(value -> value.start));
            ArrayList<Interval> result = new ArrayList<>();

            Interval original = intervals.get(0);
            result.add(copy(original));
            for (int i = 1; i < intervals.size(); i++) {
                Interval interval = intervals.get(i);
                Interval lastMergedInterval = result.get(result.size() - 1);
                if (interval.start <= lastMergedInterval.end) {
                    if (interval.end > lastMergedInterval.end) {
                        lastMergedInterval.end = interval.end;
                    }
                } else {
                    result.add(copy(interval));
                }
            }
            return result;

        }

        private Interval copy(Interval original) {
            Interval result = new Interval();
            result.start = original.start;
            result.end = original.end;
            return result;
        }
    }
}
