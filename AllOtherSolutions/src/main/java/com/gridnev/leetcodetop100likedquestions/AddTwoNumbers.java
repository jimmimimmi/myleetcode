package com.gridnev.leetcodetop100likedquestions;

/*
https://leetcode.com/problems/add-two-numbers/description/

You are given two non-empty linked lists representing two non-negative integers.
The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Example

Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 0 -> 8
Explanation: 342 + 465 = 807.


 */

import com.gridnev.ListNode;

public class AddTwoNumbers {
    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        if (l1 == null) {
            return l2;
        }
        if (l2 == null) {
            return l1;
        }

        ListNode current1 = l1;
        ListNode current2 = l2;

        ListNode headResult = new ListNode(0);
        ListNode currentResult = headResult;
        int carry = 0;

        while (current1 != null || current2 != null) {
            int newValue = carry;
            carry = 0;

            if (current1 != null) {
                newValue += current1.val;
                current1 = current1.next;
            }

            if (current2 != null) {
                newValue += current2.val;
                current2 = current2.next;
            }

            carry = newValue / 10;
            newValue = newValue % 10;

            ListNode next = new ListNode(newValue);
            currentResult.next = next;
            currentResult = currentResult.next;
        }

        if (carry != 0) {
            ListNode next = new ListNode(carry);
            currentResult.next = next;
        }

        return headResult.next;

    }
}
