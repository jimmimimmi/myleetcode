package com.gridnev.leetcodetop100likedquestions;

import java.util.ArrayList;
import java.util.List;

public class FlattenListUtil {

    public static List<Integer> flatten(List<?> nestedList) {
        ArrayList<Integer> flattenedList = new ArrayList<>();

        fillFlattenedList(nestedList, flattenedList);

        return flattenedList;
    }

    private static void fillFlattenedList(List<?> nestedList, List<Integer> result) {
        if (nestedList == null) {
            throw new IllegalArgumentException("null element");
        }

        for (Object currentElement : nestedList) {
            if (currentElement instanceof Integer) {
                result.add((Integer) currentElement);
                continue;
            }

            if (currentElement instanceof List) {
                fillFlattenedList((List<?>) currentElement, result);
                continue;
            }

            throw new IllegalArgumentException("list contains not only integers and nested lists");
        }
    }
}
