package com.gridnev.leetcodetop100likedquestions;

/*
Design and implement a data structure for Least Recently Used (LRU) cache.
It should support the following operations: get and put.

get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
put(key, value) - Set or insert the value if the key is not already present.
When the cache reached its capacity, it should invalidate the least recently used item before inserting a new item.

Follow up:
Could you do both operations in O(1) time complexity?

Example:

LRUCache cache = new LRUCache( 2  capacity );

        cache.put(1, 1);
        cache.put(2, 2);
        cache.get(1);       // returns 1
        cache.put(3, 3);    // evicts key 2
        cache.get(2);       // returns -1 (not found)
        cache.put(4, 4);    // evicts key 1
        cache.get(1);       // returns -1 (not found)
        cache.get(3);       // returns 3
        cache.get(4);       // returns 4

Your LRUCache object will be instantiated and called as such:
LRUCache obj = new LRUCache(capacity);
int param_1 = obj.get(key);
obj.put(key,value);

 */

import java.util.HashMap;
import java.util.Map;


public class LRUCache {
    static class DualListNode {
        int val;
        int key;
        DualListNode prev;
        DualListNode next;
    }

    static class DualList {
        DualListNode head;
        DualListNode tail;

        public DualList() {
            head = new DualListNode();
            tail = new DualListNode();

            head.next = tail;
            tail.prev = head;
        }

        public void add(DualListNode node) {
            node.next = head.next;
            node.prev = head;
            node.next.prev = node;
            head.next = node;
        }

        public DualListNode getLastNode() {
            return tail.prev != head ? tail.prev : null;
        }

        public void remove(DualListNode node) {
            node.prev.next = node.next;
            node.next.prev = node.prev;
            node.next = null;
            node.prev = null;
        }
    }


    private final int capacity;
    private Map<Integer, DualListNode> map = new HashMap<>();
    private DualList list = new DualList();

    public LRUCache(int capacity) {
        this.capacity = capacity;
    }

    public int get(int key) {
        DualListNode dualListNode = map.get(key);
        if (dualListNode == null) {
            return -1;
        }
        list.remove(dualListNode);
        list.add(dualListNode);
        return dualListNode.val;
    }

    public void put(int key, int value) {
        DualListNode dualListNode = map.get(key);
        if (dualListNode != null) {
            dualListNode.val = value;
            list.remove(dualListNode);
            list.add(dualListNode);
            return;
        }
        dualListNode = new DualListNode();
        dualListNode.val = value;
        dualListNode.key = key;
        list.add(dualListNode);
        map.put(key, dualListNode);


        if (map.size() > capacity) {
            DualListNode lastNode = list.getLastNode();
            list.remove(lastNode);
            map.remove(lastNode.key);
        }
    }
}
