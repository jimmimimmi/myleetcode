package com.gridnev.leetcodetop100likedquestions;


/*
https://leetcode.com/problems/longest-palindromic-substring/description/

Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.

Example 1:

Input: "babad"
Output: "bab"
Note: "aba" is also a valid answer.
Example 2:

Input: "cbbd"
Output: "bb"

 */

public class LongestPalindromicSubstring {
    public String longestPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return s;
        }
        String result = s.substring(0, 1);
        for (int i = 0; i < s.length(); i++) {
            String current = getLongestPalindromeWithGivenCenter(i, s);
            if (current.length() > result.length()) {
                result = current;
            }
        }

        return result;

    }

    private int[] find(int left, int right, String originalString) {
        int leftResult = -1;
        int rightResult = -1;

        while (left >= 0 &&
                right < originalString.length() &&
                originalString.charAt(left) == originalString.charAt(right)) {
            leftResult = left;
            rightResult = right;
            left--;
            right++;
        }

        if (rightResult != -1) {
            return new int[]{leftResult, rightResult};
        }
        return null;
    }

    private String getLongestPalindromeWithGivenCenter(int center, String originalString) {

        int[] centerPalindrome = find(center, center, originalString);
        int[] betweenCenterPalindrome = find(center, center + 1, originalString);

        int length1 = centerPalindrome[1] - centerPalindrome[0];
        int length2 = betweenCenterPalindrome == null ? -1 : betweenCenterPalindrome[1] - betweenCenterPalindrome[0];

        if (length1 >= length2) {
            return originalString.substring(centerPalindrome[0], centerPalindrome[1] + 1);
        }
        return originalString.substring(betweenCenterPalindrome[0], betweenCenterPalindrome[1] + 1);
    }
}
