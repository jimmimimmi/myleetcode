package com.gridnev.leetcodetop100likedquestions;

/*
https://leetcode.com/problems/longest-substring-without-repeating-characters/description/


Given a string, find the length of the longest substring without repeating characters.

Examples:

Given "abcabcbb", the answer is "abc", which the length is 3.

Given "bbbbb", the answer is "b", with the length of 1.

Given "pwwkew", the answer is "wke", with the length of 3.
Note that the answer must be a substring, "pwke" is a subsequence and not a substring.

 */

public class LongestSubstringWithoutRepeatingCharacters {
    public int lengthOfLongestSubstring(String s) {

        if (s == null || s.isEmpty()) {
            return 0;
        }

        char[] charArray = s.toCharArray();
        int left = 0;
        int maxWidth = 0;
        Integer[] indexesOfCharsWithinGivenString = new Integer[256];

        for (int i = 0; i < charArray.length; i++) {
            char c = charArray[i];

            if (indexesOfCharsWithinGivenString[c] != null) {
                if (indexesOfCharsWithinGivenString[c] + 1 > left) {
                    left = indexesOfCharsWithinGivenString[c] + 1;
                }
            }

            indexesOfCharsWithinGivenString[c] = i;
            int currentWidth = i - left + 1;
            if (currentWidth > maxWidth) {
                maxWidth = currentWidth;
            }
        }
        return maxWidth;

    }
}
