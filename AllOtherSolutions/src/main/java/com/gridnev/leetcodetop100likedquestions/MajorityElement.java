package com.gridnev.leetcodetop100likedquestions;

/*
https://leetcode.com/problems/majority-element/description/

Given an array of size n, find the majority element. The majority element is the element that appears more than ⌊ n/2 ⌋ times.

You may assume that the array is non-empty and the majority element always exist in the array.

Credits:
Special thanks to @ts for adding this problem and creating all test cases.

 */

public class MajorityElement {
    public int majorityElement(int[] nums) {

        if (nums.length == 1) {
            return nums[0];
        }

        int count = 0;
        Integer currentValue = null;

        for (int num : nums) {
            if (currentValue == null) {
                currentValue = num;
                count = 1;
                continue;
            }

            if (num == currentValue) {
                count++;
            } else {
                count--;
                if (count == 0) {
                    currentValue = null;
                }
            }
        }
        return currentValue.intValue();
    }
}
