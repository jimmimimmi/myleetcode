package com.gridnev.leetcodetop100likedquestions;

/*
https://leetcode.com/problems/maximum-subarray/description/

Given an integer array nums,
find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

Example:

Input: [-2,1,-3,4,-1,2,1,-5,4],
Output: 6
Explanation: [4,-1,2,1] has the largest sum = 6.
Follow up:

If you have figured out the O(n) solution,
try coding another solution using the divide and conquer approach, which is more subtle.


 */

public class MaximumSubarray {
    public int maxSubArray(int[] nums) {
        Integer firstIndexOfPositiveValue = getFirstIndexOfPositiveValue(nums);
        if (firstIndexOfPositiveValue == null) {
            return getMax(nums);
        }
        int result = nums[firstIndexOfPositiveValue];
        if (firstIndexOfPositiveValue == nums.length - 1) {
            return result;
        }

        int currentSum = result;
        for (int i = firstIndexOfPositiveValue + 1; i < nums.length; i++) {
            int num = nums[i];
            currentSum += num;
            if (currentSum > result) {
                result = currentSum;
            }
            if (currentSum <= 0) {
                currentSum = 0;
            }
        }

        return result;
    }

    private Integer getFirstIndexOfPositiveValue(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            if (num > 0) {
                return i;
            }
        }
        return null;
    }

    private int getMax(int[] nums) {
        int result = nums[0];
        for (int num : nums) {
            if (num > result) {
                result = num;
            }
        }
        return result;
    }
}
