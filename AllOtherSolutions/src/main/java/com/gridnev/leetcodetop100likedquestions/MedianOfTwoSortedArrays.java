package com.gridnev.leetcodetop100likedquestions;
/*
https://leetcode.com/problems/median-of-two-sorted-arrays/description/


There are two sorted arrays nums1 and nums2 of size m and n respectively.

Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).

Example 1:
nums1 = [1, 3]
nums2 = [2]

The median is 2.0
Example 2:
nums1 = [1, 2]
nums2 = [3, 4]

The median is (2 + 3)/2 = 2.5

 */


public class MedianOfTwoSortedArrays {
    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int length1 = nums1 == null ? 0 : nums1.length;
        int length2 = nums2 == null ? 0 : nums2.length;

        int firstIndex = (length1 + length2 - 1) / 2;
        int secondIndex = (length1 + length2) / 2;

        if (length1 == 0) {
            return (nums2[firstIndex] + nums2[secondIndex]) / 2.0;
        }
        if (length2 == 0) {
            return (nums1[firstIndex] + nums1[secondIndex]) / 2.0;
        }


        int firstIndexValue = getKthElement(firstIndex + 1, nums1, 0, nums1.length - 1, nums2, 0, nums2.length - 1);


        int secondIndexValue = firstIndex == secondIndex ? firstIndexValue :
                getKthElement(secondIndex + 1, nums1, 0, nums1.length - 1, nums2, 0, nums2.length - 1);
        return (firstIndexValue + secondIndexValue) / 2.0;
    }

    private static int getKthElement(int k, int[] nums1, int left1, int right1, int[] nums2, int left2, int right2) {
        if (left1 > right1) {
            return nums2[left2 + k - 1];
        }

        if (left2 > right2) {
            return nums1[left1 + k - 1];
        }

        if (k == 1) {
            return nums1[left1] < nums2[left2] ? nums1[left1] : nums2[left2];
        }

        int middle1 = left1 + (k / 2) - 1;
        int middle2 = left2 + (k / 2) - 1;

        if (middle1 > right1) {
            middle1 = right1;
        }

        if (middle2 > right2) {
            middle2 = right2;
        }
        int length1 = middle1 - left1 + 1;
        int length2 = middle2 - left2 + 1;

        if (nums1[middle1] > nums2[middle2]) {
            return getKthElement(k - length2, nums1, left1, right1, nums2, middle2 + 1, right2);
        }
        return getKthElement(k - length1, nums1, middle1 + 1, right1, nums2, left2, right2);

    }
}
