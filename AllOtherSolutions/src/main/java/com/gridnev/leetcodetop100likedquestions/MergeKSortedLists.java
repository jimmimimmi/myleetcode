package com.gridnev.leetcodetop100likedquestions;

import com.gridnev.ListNode;

public class MergeKSortedLists {
    public ListNode mergeKLists(ListNode[] lists) {
        return mergeLists(lists, 0, lists.length - 1);
    }

    private ListNode mergeLists(ListNode[] lists, int left, int right) {
        if (left == right) {
            return lists[left];
        }
        if (left + 1 == right) {
            return new MergeTwoSortedLists().mergeTwoLists(lists[left], lists[right]);
        }

        int middle = left + (right - left) / 2;

        ListNode leftMergedList = mergeLists(lists, left, middle);
        ListNode rightMergedList = mergeLists(lists, middle + 1, right);
        return new MergeTwoSortedLists().mergeTwoLists(leftMergedList, rightMergedList);
    }
}
