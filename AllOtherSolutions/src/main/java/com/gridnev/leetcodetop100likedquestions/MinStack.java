package com.gridnev.leetcodetop100likedquestions;

import java.util.ArrayList;
import java.util.List;

public class MinStack {

    private List<int[]> stack = new ArrayList<>();

    public MinStack() {

    }

    public void push(int x) {
        if (stack.isEmpty()) {
            stack.add(new int[]{x, x});
            return;
        }

        int currentMin = getMin();
        int newMin = x < currentMin ? x : currentMin;
        stack.add(new int[]{x, newMin});
    }

    public void pop() {
        if (!stack.isEmpty()) {
            stack.remove(stack.size() - 1);
        }
    }

    public int top() {
        return stack.get(stack.size() - 1)[0];
    }

    public int getMin() {
        return stack.get(stack.size() - 1)[1];
    }
}
