package com.gridnev.leetcodetop100likedquestions;

/*
https://leetcode.com/problems/reverse-linked-list/description/

 */

import com.gridnev.ListNode;

public class ReverseLinkedList {
    public ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode previous = head;
        ListNode current = head.next;

        while (current != null) {
            ListNode next = current.next;
            current.next = previous;
            previous = current;
            current = next;
        }
        head.next = null;
        return previous;
    }
}
