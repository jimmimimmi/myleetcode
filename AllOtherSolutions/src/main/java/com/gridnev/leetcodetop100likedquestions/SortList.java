package com.gridnev.leetcodetop100likedquestions;

import com.gridnev.ListNode;

public class SortList {
    public ListNode sortList(ListNode head) {
        return sort(head);
    }

    private ListNode sort(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        if (head.next.next == null) {

            if (head.val < head.next.val) {
                return head;
            } else {
                ListNode next = head.next;
                head.next = null;
                next.next = head;
                return next;
            }

        }

        ListNode second = splitListOnTwoAndGetSecondHead(head);
        if (second == null) {
            return head;
        }


        ListNode sortedHead = sort(head);
        ListNode sortedSecond = sort(second);
        ListNode result = mergeTwoSortedLists(sortedHead, sortedSecond);
        return result;
    }

    private ListNode mergeTwoSortedLists(ListNode first, ListNode second) {
        if (first == null) {
            return second;
        }

        if (second == null) {
            return first;
        }
        ListNode head = new ListNode(0);
        ListNode current = head;

        while (first != null && second != null) {
            if (first.val < second.val) {
                current.next = new ListNode(first.val);
                first = first.next;
            } else {
                current.next = new ListNode(second.val);
                second = second.next;
            }
            current = current.next;
        }

        if (first != null) {
            current.next = first;
        }
        if (second != null) {
            current.next = second;
        }
        return head.next;

    }

    private ListNode splitListOnTwoAndGetSecondHead(ListNode head) {
        if (head == null || head.next == null) {
            return null;
        }

        ListNode slowPointer = head;
        ListNode fastPointer = head;

        ListNode previous = null;
        while (fastPointer != null && fastPointer.next != null) {
            previous = slowPointer;
            slowPointer = slowPointer.next;
            fastPointer = fastPointer.next.next;
        }

        previous.next = null;
        return slowPointer;
    }
}
