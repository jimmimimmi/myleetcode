package com.gridnev.leetcodetop100likedquestions;

/*

Given an array nums of n integers, are there elements a, b, c in nums such that a + b + c = 0?
Find all unique triplets in the array which gives the sum of zero.

Note:

The solution set must not contain duplicate triplets.

Example:

Given array nums = [-1, 0, 1, 2, -1, -4],

A solution set is:
[
  [-1, 0, 1],
  [-1, -1, 2]
]

 */

import java.util.*;

public class SumOf3 {
    public List<List<Integer>> threeSum(int[] nums) {
        if (nums == null || nums.length < 3) {
            return Collections.emptyList();
        }

        HashMap<Integer, Integer> map = new HashMap<>();


        for (int i = 0; i < nums.length; i++) {
            map.put(nums[i], i);
        }

        ArrayList<List<Integer>> result = new ArrayList<>();

        for (int i = 0; i <= nums.length - 3; i++) {
            int first = nums[i];
            for (int j = i + 1; j <= nums.length - 2; j++) {
                int second = nums[j];

                int expected = 0 - first - second;
                int actualIndex = get(map, expected);

                if (actualIndex >= j) {
                    result.add(Arrays.asList(first, second, expected));
                }
            }
        }
        return result;
    }

    private int get(HashMap<Integer, Integer> integerIntegerHashMap, int value) {
        if (integerIntegerHashMap.containsKey(value)) {
            return integerIntegerHashMap.get(value);
        }
        return -1;
    }
}
