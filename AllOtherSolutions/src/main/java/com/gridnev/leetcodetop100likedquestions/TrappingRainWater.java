package com.gridnev.leetcodetop100likedquestions;

/*

https://leetcode.com/problems/trapping-rain-water/description/

Given n non-negative integers representing an elevation map where the width of each bar is 1,
compute how much water it is able to trap after raining.

Example:

Input: [0,1,0,2,1,0,1,3,2,1,2,1]
Output: 6

 */

public class TrappingRainWater {
    public int trap(int[] height) {
        if (height == null || height.length < 3) {
            return 0;
        }
        int maxRight = Integer.MIN_VALUE;
        int maxLeft = Integer.MIN_VALUE;

        int left = 0;
        int right = height.length - 1;
        int result = 0;

        while (left < right) {
            if (height[left] > maxLeft) {
                maxLeft = height[left];
            }

            if (height[right] > maxRight) {
                maxRight = height[right];
            }

            if (height[left] <= height[right]) {
                result += Math.max(0, Math.min(maxRight, maxLeft) - height[left]);
                left++;
            } else {
                result += Math.max(0, Math.min(maxRight, maxLeft) - height[right]);
                right--;
            }
        }
        return result;
    }

}
