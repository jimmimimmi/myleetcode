package com.gridnev.leetcodetop100likedquestions;

/*
https://leetcode.com/problems/two-sum/description/

Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

Example:

Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].

 */

import java.util.HashMap;

public class TwoSum {
    public int[] twoSum(int[] nums, int target) {
        HashMap<Integer, Integer> valueIndexMap = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            int expectedValue = target - num;
            Integer expectedIndex = valueIndexMap.get(expectedValue);
            if (expectedIndex != null) {
                return new int[]{expectedIndex, i};
            }
            valueIndexMap.put(num, i);
        }
        return null;
    }
}
