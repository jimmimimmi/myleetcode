package com.gridnev.leetcodetop100likedquestions;

/*
https://leetcode.com/problems/valid-parentheses/description/


Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:

Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.
Note that an empty string is also considered valid.

Example 1:

Input: "()"
Output: true
Example 2:

Input: "()[]{}"
Output: true
Example 3:

Input: "(]"
Output: false
Example 4:

Input: "([)]"
Output: false
Example 5:

Input: "{[]}"
Output: true

 */


import java.util.Stack;

public class ValidParentheses {
    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        for (char c : s.toCharArray()) {
            if (isItOpenChar(c)) {
                stack.push(c);
                continue;
            }

            if (isItCloseChar(c)) {
                char respectedOpenChar = getRespectedOpenChar(c);
                if (stack.empty()) {
                    return false;
                }
                Character popChar = stack.pop();
                if (popChar != respectedOpenChar) {
                    return false;
                }
            }
        }
        return stack.empty();

    }

    private boolean isItOpenChar(char c) {
        return c == '{' || c == '(' || c == '[';
    }

    private boolean isItCloseChar(char c) {
        return c == '}' || c == ']' || c == ')';
    }

    private char getRespectedOpenChar(char closingChar) {
        switch (closingChar) {
            case '}':
                return '{';
            case ']':
                return '[';
            case ')':
                return '(';
            default:
                throw new IllegalArgumentException();
        }
    }

}
