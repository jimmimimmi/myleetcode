package com.gridnev.numberofislands;

import java.util.LinkedList;
import java.util.Queue;

public class NumberOfIslandsBFS {
    public static int numIslands(char[][] grid) {
        if (grid == null || grid.length == 0 || grid[0].length == 0) {
            return 0;
        }

        int result = 0;
        for (int rowIndex = 0; rowIndex < grid.length; rowIndex++) {
            for (int columnIndex = 0; columnIndex < grid[0].length; columnIndex++) {
                if (grid[rowIndex][columnIndex] == '1') {
                    result++;
                    Queue<int[]> queue = new LinkedList<>();
                    queue.offer(new int[]{rowIndex, columnIndex});
                    while (!queue.isEmpty()) {
                        int[] indexes = queue.poll();
                        int currentRow = indexes[0];
                        int currentColumn = indexes[1];
                        if (isItLand(grid, currentRow, currentColumn)) {
                            grid[currentRow][currentColumn] = '#';

                            queue.offer(new int[]{currentRow, currentColumn - 1});
                            queue.offer(new int[]{currentRow, currentColumn + 1});
                            queue.offer(new int[]{currentRow - 1, currentColumn});
                            queue.offer(new int[]{currentRow + 1, currentColumn});
                        }
                    }
                }
            }
        }

        return result;
    }

    private static boolean isItLand(char[][] grid, int row, int column) {
        if (row < 0 || column < 0 || row >= grid.length || column >= grid[0].length) {
            return false;
        }
        return grid[row][column] == '1';
    }
}
