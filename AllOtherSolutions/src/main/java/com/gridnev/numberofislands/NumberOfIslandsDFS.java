package com.gridnev.numberofislands;

public class NumberOfIslandsDFS {
    public static int numIslands(char[][] grid) {
        if (grid == null || grid.length == 0 || grid[0].length == 0) {
            return 0;
        }

        int result = 0;
        for (int rowIndex = 0; rowIndex < grid.length; rowIndex++) {
            for (int columnIndex = 0; columnIndex < grid[0].length; columnIndex++) {
                char c = grid[rowIndex][columnIndex];
                if (c == '1') {
                    result++;
                    dfs(grid, rowIndex, columnIndex);
                }
            }
        }
        return result;
    }

    private static void dfs(char[][] grid, int row, int column) {
        if (!isItLand(grid, row, column)) {
            return;
        }
        grid[row][column] = '#';
        dfs(grid, row, column - 1);
        dfs(grid, row, column + 1);
        dfs(grid, row - 1, column);
        dfs(grid, row + 1, column);
    }

    private static boolean isItLand(char[][] grid, int row, int column) {
        if (row < 0 || column < 0 || row >= grid.length || column >= grid[0].length) {
            return false;
        }
        return grid[row][column] == '1';
    }
}
