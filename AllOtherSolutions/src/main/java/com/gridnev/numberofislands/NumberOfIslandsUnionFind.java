package com.gridnev.numberofislands;

public class NumberOfIslandsUnionFind {
    private static class UnionFind {
        private int[] parent;
        private int[] rank;
        private int amountOfDisjointSets = 0;

        private UnionFind(char[][] grid) {
            int rowAmount = grid.length;
            int columnAmount = grid[0].length;
            int totalSize = rowAmount * columnAmount;
            parent = new int[totalSize];
            rank = new int[totalSize];
            for (int i = 0; i < totalSize; i++) {
                char c = grid[i / columnAmount][i % columnAmount];
                if (c == '1') {
                    amountOfDisjointSets++;
                    parent[i] = i;
                }
            }
        }

        public void union(int i, int j) {
            int rootI = findRootParentWithCompression(i);
            int rootJ = findRootParentWithCompression(j);
            if (rootI == rootJ) {
                return;
            }

            unionConsideringRank(rootI, rootJ);
            amountOfDisjointSets--;
        }

        private void unionConsideringRank(int rootI, int rootJ) {
            if (rank[rootI] == rank[rootJ]) {
                parent[rootJ] = rootI;
                rank[rootI]++;
                return;
            }

            if (rank[rootJ] < rank[rootI]) {
                parent[rootJ] = rootI;
                return;
            } else {
                unionConsideringRank(rootJ, rootI);
            }

        }

        private int findRootParent(int i) {
            int result = i;
            while (parent[result] != result) {
                result = parent[result];
            }
            return result;
        }

        private int findRootParentWithCompression(int i) {
            if (parent[i] != i) {
                parent[i] = findRootParentWithCompression(parent[i]);
            }
            return parent[i];
        }
    }


    public static int numIslands(char[][] grid) {
        UnionFind unionFind = new UnionFind(grid);

        int columnAmount = grid[0].length;
        int rowAmount = grid.length;
        for (int row = 0; row < rowAmount; row++) {
            for (int column = 0; column < columnAmount; column++) {
                char c = grid[row][column];
                if (c == '1') {
                    grid[row][column] = '#';

                    if (column - 1 >= 0 && grid[row][column - 1] == '1') {
                        unionFind.union(row * columnAmount + column, row * columnAmount + (column - 1));
                    }

                    if (column + 1 < columnAmount && grid[row][column + 1] == '1') {
                        unionFind.union(row * columnAmount + column, row * columnAmount + (column + 1));
                    }

                    if (row - 1 >= 0 && grid[row - 1][column] == '1') {
                        unionFind.union(row * columnAmount + column, (row - 1) * columnAmount + column);
                    }
                    if (row + 1 < rowAmount && grid[row + 1][column] == '1') {
                        unionFind.union(row * columnAmount + column, (row + 1) * columnAmount + column);
                    }
                }
            }
        }
        return unionFind.amountOfDisjointSets;
    }
}
