package com.gridnev.permutations;

import java.util.ArrayList;
import java.util.List;

public class Combinations {
    public List<List<Integer>> combine(int n, int k) {

        ArrayList<List<Integer>> result = new ArrayList<>();

        calculateAndUpdateResult(result, new ArrayList<>(), 1, n, k);
        return result;
    }

    private void calculateAndUpdateResult(
            List<List<Integer>> result,
            List<Integer> currentCombination,
            int currentElement,
            int totalN,
            int combinationSizeK) {

        if (currentCombination.size() == combinationSizeK) {
            result.add(new ArrayList<>(currentCombination));
            return;
        }

        for (int i = currentElement; i <= totalN; i++) {
            currentCombination.add(i);
            calculateAndUpdateResult(result, currentCombination, i + 1, totalN, combinationSizeK);
            currentCombination.remove(currentCombination.size() - 1);
        }

    }
}
