package com.gridnev.permutations;

import java.util.ArrayList;
import java.util.List;

public class GenerateParentheses {
    public List<String> generateParenthesis(int n) {
        ArrayList<String> result = new ArrayList<String>();
        dfs(result, "", n, n);
        return result;
    }


    private void dfs(ArrayList<String> result, String s, int left, int right) {
        String state = "s: " + s + ", left: " + left + ", right: " + right;
        System.out.println("ENTER: " + state);
        if (left > right) {
            System.out.println("LEAVING BECAUSE left > right: " + state);
            return;
        }


        if (left == 0 && right == 0) {
            result.add(s);
            System.out.println("LEAVING BECAUSE left == 0 && right == 0: " + state);
            return;
        }

        if (left > 0) {
            System.out.println("GO DOWN BECAUSE left > 0: " + state);
            dfs(result, s + "(", left - 1, right);
            System.out.println("COME UP after left > 0: " + state);
        }

        if (right > 0) {
            System.out.println("GO DOWN BECAUSE right > 0: " + state);
            dfs(result, s + ")", left, right - 1);
            System.out.println("COME UP after right > 0: " + state);
        }
        System.out.println("EXIT: " + state);
    }


}
