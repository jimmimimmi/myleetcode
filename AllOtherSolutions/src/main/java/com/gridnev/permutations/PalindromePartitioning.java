package com.gridnev.permutations;

import java.util.ArrayList;
import java.util.List;

public class PalindromePartitioning {
    public List<List<String>> partition(String s) {
        List<List<String>> result = new ArrayList<>();

        backTrack(result, new ArrayList<>(), s, 0);
        return result;
    }


    private void backTrack(List<List<String>> result, List<String> currentPartition, String originalString, int currentIndex) {

        if (currentIndex >= originalString.length()) {
            result.add(new ArrayList<>(currentPartition));
        } else {
            for (int i = currentIndex; i < originalString.length(); i++) {
                if (isPalindrome(originalString, currentIndex, i)) {
                    currentPartition.add(originalString.substring(currentIndex, i + 1));
                    backTrack(result, currentPartition, originalString, i + 1);
                    currentPartition.remove(currentPartition.size() - 1);
                }
            }
        }


    }


    private boolean isPalindrome(String s, int low, int high) {
        while (low < high)
            if (s.charAt(low++) != s.charAt(high--)) return false;
        return true;
    }
}
