package com.gridnev.permutations;

import java.util.ArrayList;
import java.util.List;

/*
 https://leetcode.com/problems/subsets/

 */
public class Permutations {
    public List<List<Integer>> permuteWithBacktracking(int[] nums) {

        ArrayList<List<Integer>> subsets = new ArrayList<>();

        backtrack(subsets, new ArrayList<>(), nums);
        return subsets;
    }

    private void backtrack(List<List<Integer>> subsets, List<Integer> currentSubset, int[] nums) {

        if (currentSubset.size() == nums.length) {
            subsets.add(new ArrayList<>(currentSubset));
        } else {
            for (int i = 0; i < nums.length; i++) {
                int num = nums[i];
                if (currentSubset.contains(num)) {
                    continue;
                }

                currentSubset.add(num);
                backtrack(subsets, currentSubset, nums);
                currentSubset.remove(currentSubset.size() - 1);
            }
        }
    }

    public ArrayList<ArrayList<Integer>> permute(int[] num) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<>();
        permute(num, 0, result);
        return result;
    }

    private void permute(int[] num, int start, ArrayList<ArrayList<Integer>> result) {

        if (start >= num.length) {
            ArrayList<Integer> item = convertArrayToList(num);
            result.add(item);
        }

        for (int j = start ; j < num.length; j++) {
            swap(num, start, j);
            permute(num, start + 1, result);
            swap(num, start, j);
        }
    }

    private ArrayList<Integer> convertArrayToList(int[] num) {
        ArrayList<Integer> item = new ArrayList<>();
        for (int h = 0; h < num.length; h++) {
            item.add(num[h]);
        }
        return item;
    }

    private void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
}
