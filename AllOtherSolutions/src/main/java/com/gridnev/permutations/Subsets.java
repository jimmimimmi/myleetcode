package com.gridnev.permutations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 https://leetcode.com/problems/subsets/

 */
public class Subsets {
    public List<List<Integer>> subsets(int[] nums) {

        ArrayList<List<Integer>> subsets = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(subsets, new ArrayList<>(), nums, 0);
        return subsets;
    }

    private void backtrack(List<List<Integer>> subsets, List<Integer> currentSubset, int[] nums, int currentStart) {
        subsets.add(new ArrayList<>(currentSubset));

        for (int i = currentStart; i < nums.length; i++) {
            int num = nums[i];

            currentSubset.add(num);
            backtrack(subsets, currentSubset, nums, i + 1);
            currentSubset.remove(currentSubset.size() - 1);
        }
    }
}
