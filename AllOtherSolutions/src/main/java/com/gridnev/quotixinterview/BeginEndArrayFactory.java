package com.gridnev.quotixinterview;

import java.util.ArrayList;

public class BeginEndArrayFactory {

    public BeginEndPair[] create(int[] excludeValues, int endExistedValue) {

        ArrayList<BeginEndPair> result = new ArrayList<BeginEndPair>();
        int expectedBegin = 0;

        for (int i = 0; i < excludeValues.length; i++) {
            int excludeValue = excludeValues[i];
            if (excludeValue != expectedBegin) {
                result.add(new BeginEndPair().setBegin(expectedBegin).setEnd(excludeValue - 1));
                expectedBegin = excludeValue + 1;
            } else {
                expectedBegin = excludeValue + 1;
            }
        }
        if (expectedBegin <= endExistedValue) {
            result.add(new BeginEndPair().setBegin(expectedBegin).setEnd(endExistedValue));
        }

        return result.toArray(new BeginEndPair[result.size()]);
    }
}
