package com.gridnev.quotixinterview;

public class BeginEndPair {
    private int begin;
    private int end;

    public int getBegin() {
        return begin;
    }

    public BeginEndPair setBegin(int begin) {
        this.begin = begin;
        return this;
    }

    public int getEnd() {
        return end;
    }

    public BeginEndPair setEnd(int end) {
        this.end = end;
        return this;
    }
}
