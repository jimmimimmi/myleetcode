package com.gridnev.quotixinterview;

import java.util.Arrays;

public class MultiplyExcept {


    private final int length;
    private Node head;

    public MultiplyExcept(int[] data) {

        length = data.length;
        Node[] lowLevel = new Node[length];
        for (int i = 0; i < data.length; i++) {
            Node leaf = new Node();
            leaf.startIndex = i;
            leaf.endIndex = i;
            leaf.product = data[i];
            lowLevel[i] = leaf;
        }

        while (lowLevel.length != 1) {
            Node[] upperLevel = new Node[lowLevel.length / 2 + lowLevel.length % 2];
            int currentLevelIndex = 0;
            int upperLevelIndex = 0;
            while (currentLevelIndex < lowLevel.length) {
                Node upperLevelNode = new Node();
                Node leftChild = lowLevel[currentLevelIndex];
                upperLevelNode.leftChild = leftChild;
                upperLevelNode.startIndex = leftChild.startIndex;
                upperLevelNode.endIndex = leftChild.endIndex;
                upperLevelNode.product = leftChild.product;

                if (currentLevelIndex + 1 < lowLevel.length) {
                    Node rightChild = lowLevel[currentLevelIndex + 1];
                    upperLevelNode.rightChild = rightChild;
                    upperLevelNode.endIndex = rightChild.endIndex;
                    upperLevelNode.product *= rightChild.product;
                }

                upperLevel[upperLevelIndex] = upperLevelNode;
                upperLevelIndex++;
                currentLevelIndex = currentLevelIndex + 2;
            }
            lowLevel = upperLevel;
        }

        head = lowLevel[0];
    }

    public long multiplyExcept(int... indexes) {
        Arrays.sort(indexes);

        BeginEndPair[] beginEndPairs = new BeginEndArrayFactory().create(indexes, length - 1);

        long result = 1L;
        for (int i = 0; i < beginEndPairs.length; i++) {
            BeginEndPair beginEnd = beginEndPairs[i];
            result = result * getProduct(beginEnd.getBegin(), beginEnd.getEnd(), head);
        }

        return result;
    }

    private long getProduct(int beginIndexInclusively, int endIndexInclusively, Node node) {

        if (node.startIndex == beginIndexInclusively && node.endIndex == endIndexInclusively) {
            return node.product;
        }

        if (beginIndexInclusively <= node.leftChild.endIndex &&
                endIndexInclusively >= node.rightChild.startIndex) {
            return getProduct(beginIndexInclusively, node.leftChild.endIndex, node.leftChild) *
                    getProduct(node.rightChild.startIndex, endIndexInclusively, node.rightChild);
        }

        if (beginIndexInclusively <= node.leftChild.endIndex &&
                endIndexInclusively <= node.leftChild.endIndex) {
            return getProduct(beginIndexInclusively, endIndexInclusively, node.leftChild);
        }

        return getProduct(beginIndexInclusively, endIndexInclusively, node.rightChild);
    }


    private static class Node {
        int startIndex;
        int endIndex;
        long product;
        Node leftChild;
        Node rightChild;
    }
}
