package com.gridnev;

/**
 * Created by a.gridnev on 26/02/2016.
 */
public class reverseBits {

    public int reverseBits(int n) {
        int answer = 0; // initializing answer
        String nToBinaryString = Integer.toBinaryString(n);
        for (int i = 0; i < 32; i++) { // 32 bit integers
            String answerToBinaryString = Integer.toBinaryString(answer);
            answer = answer << 1; // shifts answer over 1 to open a space
            answerToBinaryString = Integer.toBinaryString(answer);
            int i1 = n >> i;
            String i1ToBinaryString = Integer.toBinaryString(i1);
            int i2 = i1 & 1;
            String i2ToBinaryString = Integer.toBinaryString(i2);
            answer = answer | i2; // inserts bits from n
            answerToBinaryString = Integer.toBinaryString(answer);
        }
        return answer;
    }
}
