package com.gridnev.shortestDistanceFromAllBuildings;

import java.util.LinkedList;
import java.util.Queue;

public class ShortestDistanceBFSFromBuildingPoints {
    private static final int[] rowNeighbours = new int[]{0, -1, 0, 1};
    private static final int[] columnNeighbours = new int[]{-1, 0, 1, 0};

    public static int shortestDistance(int[][] grid) {

        int[][] totalDistanceFromAllBuildings = new int[grid.length][grid[0].length];
        int[][] amountOfReachableBuildings = new int[grid.length][grid[0].length];
        int totalAmountOfBuildings = 0;


        for (int row = 0; row < grid.length; row++) {
            for (int column = 0; column < grid[0].length; column++) {
                if (grid[row][column] != 1) {
                    continue;
                }
                boolean[][] visited = new boolean[grid.length][grid[0].length];
                totalAmountOfBuildings++;
                Queue<int[]> queue = new LinkedList<int[]>();

                int distanceFromParticularGivenBuilding = 1;
                putNeighboursIntoQueue(queue, row, column);
                while (!queue.isEmpty()) {
                    int queueSize = queue.size();
                    for (int i = 0; i < queueSize; i++) {
                        int[] currentRowAndColumn = queue.poll();
                        int currentRow = currentRowAndColumn[0];
                        int currentColumn = currentRowAndColumn[1];
                        if (currentRow < 0 ||
                                currentRow == grid.length ||
                                currentColumn < 0 ||
                                currentColumn == grid[0].length) {
                            continue;
                        }
                        if (visited[currentRow][currentColumn]) {
                            continue;
                        }

                        visited[currentRow][currentColumn] = true;

                        if (grid[currentRow][currentColumn] == 0) {
                            amountOfReachableBuildings[currentRow][currentColumn]++;
                            totalDistanceFromAllBuildings[currentRow][currentColumn] += distanceFromParticularGivenBuilding;

                            putNeighboursIntoQueue(queue, currentRow, currentColumn);

                        }
                    }
                    distanceFromParticularGivenBuilding++;
                }
            }
        }

        int minDistance = -1;
        for (int row = 0; row < grid.length; row++) {
            for (int column = 0; column < grid[0].length; column++) {
                if (grid[row][column] == 0) {
                    if (amountOfReachableBuildings[row][column] == totalAmountOfBuildings) {
                        minDistance = minDistance == -1 ?
                                totalDistanceFromAllBuildings[row][column] :
                                java.lang.Math.min(minDistance, totalDistanceFromAllBuildings[row][column]);
                    }
                }
            }
        }

        return minDistance;


    }

    private static void putNeighboursIntoQueue(Queue<int[]> queue, int currentRow, int currentColumn) {
        for (int nextPointNumber = 0; nextPointNumber < 4; nextPointNumber++) {
            int nextRow = currentRow + rowNeighbours[nextPointNumber];
            int nextColumn = currentColumn + columnNeighbours[nextPointNumber];
            queue.offer(new int[]{nextRow, nextColumn});
        }
    }

}
