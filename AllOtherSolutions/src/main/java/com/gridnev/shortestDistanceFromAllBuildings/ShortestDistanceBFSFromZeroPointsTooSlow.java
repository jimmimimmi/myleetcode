package com.gridnev.shortestDistanceFromAllBuildings;

/*

You want to build a house on an empty land which reaches all buildings in the shortest
amount of distance. You can only move up, down, left and right.
You are given a 2D grid of values 0, 1 or 2, where:

Each 0 marks an empty land which you can pass by freely.
Each 1 marks a building which you cannot pass through.
Each 2 marks an obstacle which you cannot pass through.
For example, given three buildings at (0,0), (0,4), (2,2), and an obstacle at (0,2):

1 - 0 - 2 - 0 - 1
|   |   |   |   |
0 - 0 - 0 - 0 - 0
|   |   |   |   |
0 - 0 - 1 - 0 - 0
The point (1,2) is an ideal empty land to build a house,
as the total travel distance of 3+3+1=7 is minimal. So return 7.

Note:
There will be at least one building.
If it is not possible to build such house according to the above rules, return -1.

 */


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ShortestDistanceBFSFromZeroPointsTooSlow {
    private static final int[] rowNeighbours = new int[]{0, -1, 0, 1};
    private static final int[] columnNeighbours = new int[]{-1, 0, 1, 0};


    public static int shortestDistance(int[][] grid) {
        int totalAmountOfBuildings = totalAmountOfBuildings(grid);
        int minDistance = -1;

        for (int row = 0; row < grid.length; row++) {
            for (int column = 0; column < grid[0].length; column++) {

                if (grid[row][column] != 0) {
                    continue;
                }
                List<Integer> distancesToAllExistedBuildings = distancesToAllExistedBuildings(grid, row, column);
                if (distancesToAllExistedBuildings.size() == totalAmountOfBuildings) {
                    int totalDistance = 0;
                    for (int index = 0; index < totalAmountOfBuildings; index++) {
                        totalDistance += distancesToAllExistedBuildings.get(index);
                    }

                    minDistance = minDistance == -1 ?
                            totalDistance :
                            Math.min(minDistance, totalDistance);
                }
            }
        }
        return minDistance;
    }


    private static List<Integer> distancesToAllExistedBuildings(int[][] grid, int row, int column) {

        Queue<Integer> rowsQueue = new LinkedList<Integer>();
        Queue<Integer> columnsQueue = new LinkedList<Integer>();
        Queue<Integer> distancesQueue = new LinkedList<Integer>();

        rowsQueue.offer(row);
        columnsQueue.offer(column);
        distancesQueue.offer(0);
        boolean[][] visitedPoints = new boolean[grid.length][grid[0].length];
        List<Integer> distances = new ArrayList<Integer>();

        while (!rowsQueue.isEmpty()) {

            Integer currentRow = rowsQueue.poll();
            Integer currentColumn = columnsQueue.poll();
            Integer currentDistance = distancesQueue.poll();

            if (currentRow < 0 || currentColumn < 0 || currentRow == grid.length || currentColumn == grid[0].length) {
                continue;
            }

            if (grid[currentRow][currentColumn] == 2) {
                continue;
            }

            if (visitedPoints[currentRow][currentColumn]) {
                continue;
            }

            visitedPoints[currentRow][currentColumn] = true;

            if (grid[currentRow][currentColumn] == 1) {
                distances.add(currentDistance);
                continue;
            }

            for (int i = 0; i < 4; i++) {
                int nextRow = currentRow + rowNeighbours[i];
                int nextColumn = currentColumn + columnNeighbours[i];

                rowsQueue.offer(nextRow);
                columnsQueue.offer(nextColumn);
                distancesQueue.offer(currentDistance + 1);
            }
        }

        return distances;
    }

    private static int totalAmountOfBuildings(int[][] grid) {
        int amount = 0;
        for (int row = 0; row < grid.length; row++) {
            for (int column = 0; column < grid[0].length; column++) {
                if (grid[row][column] == 1) {
                    amount++;
                }
            }
        }
        return amount;
    }
}
