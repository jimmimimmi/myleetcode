package com.gridnev.sortings;

public class CountSort {
    public static String sort(String initialString) {
        int[] countArray = new int[256];
        char[] chars = initialString.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            countArray[c]++;
        }

        for (int i = 1; i < countArray.length; i++) {
            countArray[i] = countArray[i] + countArray[i - 1];
        }

        char[] result = new char[initialString.length()];
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            int offset = countArray[c] - 1;
            result[offset] = c;
            countArray[c]--;
        }
        return String.valueOf(result);
    }
}
