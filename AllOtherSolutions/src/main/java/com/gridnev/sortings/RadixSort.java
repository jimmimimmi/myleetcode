package com.gridnev.sortings;

import java.util.Arrays;

public class RadixSort {
    public static int[] sort(int[] initialArray) {
        int maximum = Integer.MIN_VALUE;
        for (int i = 0; i < initialArray.length; i++) {
            int currentValue = initialArray[i];
            if (currentValue > maximum) {
                maximum = currentValue;
            }
        }

        int[] copyArray = Arrays.copyOf(initialArray, initialArray.length);
        for (int exp = 1; maximum / exp > 0; exp = exp * 10) {
            countSort(copyArray, exp);
        }
        return copyArray;
    }

    private static void countSort(int[] array, int exp) {
        int[] count = new int[10];
        for (int i = 0; i < array.length; i++) {
            int currentValue = array[i];
            count[(currentValue / exp) % 10]++;
        }

        for (int i = 1; i < count.length; i++) {
            count[i] = count[i] + count[i - 1];
        }

        int[] result = new int[array.length];
        for (int i = result.length - 1; i >= 0; i--) {
            int currentValue = array[i];
            int bucket = (currentValue / exp) % 10;
            int offset = count[bucket] - 1;
            result[offset] = currentValue;
            count[bucket]--;
        }

        for (int i = 0; i < result.length; i++) {
            array[i] = result[i];
        }
    }
}
