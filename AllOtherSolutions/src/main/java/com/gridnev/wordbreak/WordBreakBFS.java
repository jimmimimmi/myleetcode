package com.gridnev.wordbreak;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class WordBreakBFS {
    public static boolean wordBreak(String s, List<String> wordDict) {

        HashSet<String> set = new HashSet<>(wordDict);

        Queue<Integer> queue = new LinkedList<>();
        boolean[] visited = new boolean[s.length()];

        queue.offer(0);
        while (!queue.isEmpty()) {
            Integer start = queue.poll();
            if (!visited[start]) {
                visited[start] = true;
                for (int end = start + 1; end <= s.length(); end++) {
                    if (set.contains(s.substring(start, end))) {
                        if (end == s.length()) {
                            return true;
                        }
                        queue.offer(end);
                    }
                }
            }
        }
        return false;
    }
}
