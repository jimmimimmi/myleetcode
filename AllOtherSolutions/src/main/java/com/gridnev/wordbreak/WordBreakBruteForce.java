package com.gridnev.wordbreak;

import java.util.HashSet;
import java.util.List;

public class WordBreakBruteForce {
    public static boolean wordBreak(String s, List<String> wordDict) {
        return wordBreak(s, new HashSet<>(wordDict), 0);
    }

    private static boolean wordBreak(String s, HashSet<String> dict, int start) {
        if (start == s.length()) {
            return true;
        }

        for (int end = start + 1; end <= s.length(); end++) {
            String substringToBeSearched = s.substring(start, end);
            if (dict.contains(substringToBeSearched)) {
                if (wordBreak(s, dict, end)) {
                    return true;
                }
            }
        }
        return false;
    }
}
