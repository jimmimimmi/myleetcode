package com.gridnev.wordbreak;

import java.util.HashSet;
import java.util.List;

public class WordBreakBruteForceWithMemoization {
    public static boolean wordBreak(String s, List<String> wordDict) {
        return wordBreak(s, new HashSet<>(wordDict), 0, new Boolean[s.length()]);
    }

    private static boolean wordBreak(
            String s, HashSet<String> dict, int start, Boolean[] auxiliaryResults) {
        if (start == s.length()) {
            return true;
        }

        if (auxiliaryResults[start] != null) {
            return auxiliaryResults[start];
        }

        for (int end = start + 1; end <= s.length(); end++) {
            String substringToBeSearched = s.substring(start, end);
            if (dict.contains(substringToBeSearched)) {
                boolean subResult = wordBreak(s, dict, end, auxiliaryResults);
                auxiliaryResults[start] = subResult;
                if (subResult) {
                    return true;
                }
            }
        }
        auxiliaryResults[start] = false;
        return false;
    }


}
