package com.gridnev.wordbreak;

import java.util.HashSet;
import java.util.List;

public class WordBreakDP {
    public static boolean wordBreak(String s, List<String> wordDict) {
        HashSet<String> set = new HashSet<>(wordDict);
        boolean[] dp = new boolean[s.length() + 1];
        dp[0] = true;

        for (int i = 1; i <= s.length(); i++) {
            for (int pivot = 0; pivot < i; pivot++) {
                if (dp[pivot]) {
                    if (set.contains(s.substring(pivot, i))) {
                        dp[i] = true;
                        break;
                    }
                }
            }
        }
        return dp[dp.length - 1];
    }
}
