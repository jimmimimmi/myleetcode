package com.gridnev;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Created by a.gridnev on 19/01/2016.
 */
public class BullsAndCowsTest
        extends TestCase
{
    public BullsAndCowsTest(String testName )
    {
        super( testName );
    }

    public static Test suite()
    {
        return new TestSuite( BullsAndCowsTest.class );
    }

    public void test(){
        final BullsAndCows bullsAndCows = new BullsAndCows();

        assertEquals("1A3B",bullsAndCows.getHint("1807","7810"));
        assertEquals("1A1B",bullsAndCows.getHint("1123","0111"));
    }


    public void testSuite1() throws Exception {

    }

    public void testTest1() throws Exception {

    }
}
