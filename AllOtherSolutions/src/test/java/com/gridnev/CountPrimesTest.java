package com.gridnev;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Created by a.gridnev on 19/01/2016.
 */
public class CountPrimesTest
        extends TestCase
{
    public CountPrimesTest(String testName )
    {
        super( testName );
    }

    public static Test suite()
    {
        return new TestSuite( CountPrimesTest.class );
    }

    public void test1(){

        CountPrimes countPrimes = new CountPrimes();

        assertEquals(0,countPrimes.countPrimes(2));
        assertEquals(1,countPrimes.countPrimes(3));
        assertEquals(4,countPrimes.countPrimes(11));
        assertEquals(5,countPrimes.countPrimes(12));
    }
}
