package com.gridnev;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class FindKthStatisticTest {

    private FindKthStatistic sut;

    @BeforeMethod
    public void beforeMethod() throws Exception {
        sut = new FindKthStatistic();
    }

    @Test
    public void testName() {
//        assertEquals(1, sut.getKthElement(new int[]{1, 6, 2, 7, 8, 4, 6}, 1));
        assertEquals(1, sut.getKthElement(new int[]{1}, 1));
    }
}