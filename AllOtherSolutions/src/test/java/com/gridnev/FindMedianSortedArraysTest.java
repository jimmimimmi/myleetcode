package com.gridnev;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class FindMedianSortedArraysTest {
    @DataProvider
    public static Object[][] arrays() {
        return new Object[][]{
                {new int[]{1, 2, 3}, new int[0]},
                {new int[]{1, 2, 3, 4}, new int[0]},
                {new int[]{1, 2, 3, 4}, new int[]{1}},
                {new int[]{1, 2, 3, 4}, new int[]{1, 2}},
                {new int[]{1, 2, 3, 4}, new int[]{1, 2, 3}},
                {new int[]{1, 2, 3, 4}, new int[]{1, 2, 3, 4}},
                {new int[]{1, 2, 3, 4}, new int[]{1, 2, 3, 4, 5}},
                {new int[]{1, 2, 8, 9}, new int[]{3, 4, 5, 6, 7}},
                {new int[]{3, 4, 5, 6}, new int[]{1, 2, 7, 8, 9}},
                {new int[]{7, 8}, new int[]{1, 2, 3, 4, 5, 6, 7}},
                {new int[]{1, 2}, new int[]{3, 4, 5, 6, 7, 8, 9, 10}},
        };
    }


    @Test(dataProvider = "arrays")
    public void findMedianOfTwoSortedArrays(int[] array1, int[] array2) throws Exception {
        double expectedMedian = FindMedianSortedArrays.findMedianUsingSorting(array1, array2);
        double actualMedian = FindMedianSortedArrays.findMedianSortedArrays(array1, array2);

        Assert.assertEquals(actualMedian, expectedMedian);
    }
}