package com.gridnev;

import junit.framework.TestCase;

/**
 * Created by a.gridnev on 15/02/2016.
 */
public class FirstBadVersionTest extends TestCase {

    public void test() throws Exception {
        FirstBadVersion firstBadVersion = new FirstBadVersion();
        int badVersion = firstBadVersion.firstBadVersion(2);
        assertEquals(1, badVersion);

    }
}