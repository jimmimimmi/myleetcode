package com.gridnev;

import junit.framework.TestCase;

/**
 * Created by a.gridnev on 15/02/2016.
 */
public class FlipGameTest extends TestCase {

    public void test1() throws Exception {
//        assertEquals("--123", new FlipGame().generatePossibleNextMoves("++123").get(0));
//        assertEquals("123--", new FlipGame().generatePossibleNextMoves("123++").get(0));
        assertEquals("12--3++", new FlipGame().generatePossibleNextMoves("12++3++").get(0));
        assertEquals("12++3--", new FlipGame().generatePossibleNextMoves("12++3++").get(1));

    }
}