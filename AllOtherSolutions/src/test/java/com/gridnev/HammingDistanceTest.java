package com.gridnev;

import junit.framework.TestCase;


public class HammingDistanceTest extends TestCase {

    HammingDistance hammingDistance;

    public void setUp() throws Exception {
        super.setUp();
        hammingDistance = new HammingDistance();
    }

    public void test1() throws Exception {
        assertEquals(2, hammingDistance.hammingDistance(1, 4));
        assertEquals(31, hammingDistance.hammingDistance(0, Integer.MAX_VALUE));
        assertEquals(1, hammingDistance.hammingDistance(0, 8));
        assertEquals(0, hammingDistance.hammingDistance(8, 8));
        assertEquals(0, hammingDistance.hammingDistance(0, 0));
        assertEquals(3, hammingDistance.hammingDistance(0, 7));
    }
}