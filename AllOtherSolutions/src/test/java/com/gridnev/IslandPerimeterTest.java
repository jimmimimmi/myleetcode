package com.gridnev;


import junit.framework.TestCase;

/**
 * Created by a.gridnev on 14/12/2016.
 */
public class IslandPerimeterTest extends TestCase {

    public void testIslandPerimeter() throws Exception {
        assertEquals(16, new IslandPerimeter().islandPerimeter(new int[][]{{0, 1, 0, 0}, {1, 1, 1, 0}, {0, 1, 0, 0}, {1, 1, 0, 0}}));
    }

}