package com.gridnev;

import org.testng.annotations.Test;

import java.util.function.Function;

import static org.testng.Assert.assertEquals;

public class LongestSubstringWithoutRepeatingCharactersTest {

    @Test
    public void testLengthOfLongestSubstring_simpleSlidingWindow() throws Exception {
        Function<String, Integer> getLength = LongestSubstringWithoutRepeatingCharacters::lengthOfLongestSubstring_simpleSlidingWindow;

        assertEquals(getLength.apply(null).intValue(), 0);
        assertEquals(getLength.apply("").intValue(), 0);
        assertEquals(getLength.apply("q").intValue(), 1);
        assertEquals(getLength.apply("r").intValue(), 1);
        assertEquals(getLength.apply("rr").intValue(), 1);
        assertEquals(getLength.apply("rrr").intValue(), 1);
        assertEquals(getLength.apply("abab").intValue(), 2);
        assertEquals(getLength.apply("abcab").intValue(), 3);
        assertEquals(getLength.apply("abcabde").intValue(), 5);
    }

    @Test
    public void testLengthOfLongestSubstring_optimizedSlidingWindow() throws Exception {
        Function<String, Integer> getLength = LongestSubstringWithoutRepeatingCharacters::lengthOfLongestSubstring_OptimizedSlidingWindow;

//        assertEquals(getLength.apply(null).intValue(), 0);
//        assertEquals(getLength.apply("").intValue(), 0);
//        assertEquals(getLength.apply("q").intValue(), 1);
//        assertEquals(getLength.apply("r").intValue(), 1);
//        assertEquals(getLength.apply("rr").intValue(), 1);
//        assertEquals(getLength.apply("rrr").intValue(), 1);
//        assertEquals(getLength.apply("abab").intValue(), 2);
//        assertEquals(getLength.apply("abcab").intValue(), 3);
        assertEquals(getLength.apply("abcabde").intValue(), 5);
    }
}