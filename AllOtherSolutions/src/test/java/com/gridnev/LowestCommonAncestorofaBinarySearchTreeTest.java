package com.gridnev;

import com.gridnev.BinarySearch.BinarySearchFactory;
import com.gridnev.BinarySearch.TreeNode;
import junit.framework.TestCase;

/**
 * Created by a.gridnev on 18/02/2016.
 */
public class LowestCommonAncestorofaBinarySearchTreeTest extends TestCase {

    public void testName() throws Exception {
        LowestCommonAncestorofaBinarySearchTree lowestCommonAncestorofaBinarySearchTree = new LowestCommonAncestorofaBinarySearchTree();
        TreeNode create = BinarySearchFactory.Create(new Integer[]{6, 2, 8, 0, 4, 7, 9, null, null, 3, 5});

        TreeNode first = new TreeNode(3);
        TreeNode second = new TreeNode(5);


        TreeNode treeNode = lowestCommonAncestorofaBinarySearchTree.lowestCommonAncestor(create, first, second);
    }
}