package com.gridnev;

import junit.framework.TestCase;

public class MaxProfitOfStockTest extends TestCase {

    private MaxProfitOfStock sut;

    public void setUp() throws Exception {
        sut = new MaxProfitOfStock();
    }

    public void testName() throws Exception {
        assertEquals(0, sut.maxProfit(new int[]{7, 5, 4, 2}));
        assertEquals(1, sut.maxProfit(new int[]{7, 5, 4, 5}));
        assertEquals(2, sut.maxProfit(new int[]{7, 5, 4, 6}));
        assertEquals(0, sut.maxProfit(new int[]{7, 7, 7, 7}));
        assertEquals(0, sut.maxProfit(new int[]{}));
        assertEquals(0, sut.maxProfit(new int[]{1}));
    }
}