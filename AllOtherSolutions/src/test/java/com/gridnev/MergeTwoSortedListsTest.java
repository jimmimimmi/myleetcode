package com.gridnev;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Created by a.gridnev on 19/01/2016.
 */
public class MergeTwoSortedListsTest
        extends TestCase {
    public MergeTwoSortedListsTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        return new TestSuite(MergeTwoSortedListsTest.class);
    }

    public void test1() {

        ListNode list1 = ListBuilder.build(new ListNode(1), new ListNode(2), new ListNode(3));
        ListNode list2 = ListBuilder.build(new ListNode(4), new ListNode(5), new ListNode(6));

        ListNode listNode = new MergeTwoSortedLists().mergeTwoLists(list1, list2);
        for (int i = 1; i <= 6; i++) {
            assertEquals(i, listNode.val);
            listNode = listNode.next;
        }
    }
}
