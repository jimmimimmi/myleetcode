package com.gridnev;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Created by a.gridnev on 19/01/2016.
 */

class ListBuilder {

    public static ListNode build(ListNode... listNodes) {
        for (int i = 0; i < listNodes.length - 1; i++) {

            listNodes[i].next = listNodes[i + 1];
        }
        return listNodes[0];
    }
}

public class PalindromLinkedListTest
        extends TestCase {
    public PalindromLinkedListTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        return new TestSuite(PalindromLinkedListTest.class);
    }

    public void test1() {
        ListNode first = new ListNode(1);
        ListNode second = new ListNode(0);
        ListNode third = new ListNode(0);
        assertFalse(new PalindromeLinkedList().isPalindrome(ListBuilder.build(first, second, third)));
    }
}
