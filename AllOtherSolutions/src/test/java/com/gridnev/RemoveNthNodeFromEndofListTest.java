package com.gridnev;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class RemoveNthNodeFromEndofListTest
        extends TestCase {
    public RemoveNthNodeFromEndofListTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        return new TestSuite(RemoveNthNodeFromEndofListTest.class);
    }

    public void test1() {
        ListNode first = new ListNode(1);
        ListNode second = new ListNode(0);
        assertEquals(second,new RemoveNthNodeFromEndofList().removeNthFromEnd(ListBuilder.build(first, second),2));
    }
}
