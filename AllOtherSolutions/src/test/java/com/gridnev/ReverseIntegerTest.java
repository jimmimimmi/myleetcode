package com.gridnev;

import junit.framework.TestCase;

public class ReverseIntegerTest extends TestCase {

    public void testName() throws Exception {
        ReverseInteger sut = new ReverseInteger();

        assertEquals(123, sut.reverse(321));
        assertEquals(-123, sut.reverse(-321));
        assertEquals(-1, sut.reverse(-1));
        assertEquals(1, sut.reverse(1));
        assertEquals(1, sut.reverse(10));
        assertEquals(0, sut.reverse(0));
        assertEquals(0, sut.reverse(sut.reverse(Integer.MAX_VALUE)));
        assertEquals(0, sut.reverse(1534236469));
    }
}