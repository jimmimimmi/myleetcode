package com.gridnev;

import junit.framework.TestCase;

public class ReverseVowelsTest extends TestCase {
    private ReverseVowels sut;

    public void setUp() throws Exception {
        super.setUp();
        sut = new ReverseVowels();
    }

    public void testName() throws Exception {
        assertEquals("", sut.reverseVowels(""));
        assertEquals("a", sut.reverseVowels("a"));
        assertEquals("ae", sut.reverseVowels("ea"));
        assertEquals("as", sut.reverseVowels("as"));
        assertEquals("afe", sut.reverseVowels("efa"));
        assertEquals("aoeu", sut.reverseVowels("ueoa"));
    }
}