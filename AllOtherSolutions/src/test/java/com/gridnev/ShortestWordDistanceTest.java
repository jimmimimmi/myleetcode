package com.gridnev;

import junit.framework.TestCase;

/**
 * Created by a.gridnev on 16/02/2016.
 */
public class ShortestWordDistanceTest extends TestCase {

    public void testName() throws Exception {

        String[] strings = {"a", "a", "a", "c", "c", "c", "b"};
        String word1 = "a";
        String word2 = "b";

        int actual = new ShortestWordDistance().shortestDistance(strings, word1, word2);
        assertEquals(4, actual);
    }
}