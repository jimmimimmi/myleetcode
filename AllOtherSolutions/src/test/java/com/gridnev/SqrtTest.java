package com.gridnev;

import junit.framework.TestCase;

public class SqrtTest extends TestCase {
    public void testName() throws Exception {
        Sqrt sqrt = new Sqrt();

        assertEquals(1, sqrt.getSqrt(1));
        assertEquals(2, sqrt.getSqrt(4));
        assertEquals(2, sqrt.getSqrt(5));
        assertEquals(2, sqrt.getSqrt(6));
        assertEquals(2, sqrt.getSqrt(7));
        assertEquals(2, sqrt.getSqrt(8));
        assertEquals(3, sqrt.getSqrt(9));
        assertEquals(0, sqrt.getSqrt(0));
        assertEquals(9, sqrt.getSqrt(81));
        assertEquals(46339, sqrt.getSqrt(2147395599));
    }
}