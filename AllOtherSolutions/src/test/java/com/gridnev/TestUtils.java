package com.gridnev;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class TestUtils {
    private static final String ALPHANUMERICS = "0123456789abcdefgihjklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static <T> boolean collectionsEquivalent(Collection<T> c1, Collection<T> c2) {
        if (c1 == null && c2 == null) {
            return true;
        }

        if (c1 == null || c2 == null) {
            return false;
        }

        if (c1.isEmpty() && c2.isEmpty()) {
            return true;
        }

        ArrayList<T> list1 = new ArrayList<>(c1);
        ArrayList<T> list2 = new ArrayList<>(c1);
        return listsEquivalent(list1, list2);

    }

    public static <T> boolean listsEquivalent(List<T> c1, List<T> c2) {
        if (c1 == null && c2 == null) {
            return true;
        }

        if (c1 == null || c2 == null) {
            return false;
        }

        if (c1.isEmpty() && c2.isEmpty()) {
            return true;
        }

        if (c1.size() != c2.size()) {
            return false;
        }

        for (T t1 : c1) {
            boolean found = false;
            for (T t2 : c2) {
                if (t1.equals(t2)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                return false;
            }
        }

        for (T t2 : c2) {
            boolean found = false;
            for (T t1 : c1) {
                if (t2.equals(t1)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                return false;
            }
        }
        return true;

    }

    public static <T> boolean arraysEquivalent(T[] ar1, T[] ar2) {
        ArrayList<T> l1 = new ArrayList<>();
        ArrayList<T> l2 = new ArrayList<>();

        for (T t : ar1) {
            l1.add(t);
        }

        for (T t : ar2) {
            l2.add(t);
        }

        return listsEquivalent(l1, l2);
    }


    public static boolean arraysEquivalent(int[] ar1, int[] ar2) {
        ArrayList<Integer> l1 = new ArrayList<>();
        ArrayList<Integer> l2 = new ArrayList<>();

        for (int t : ar1) {
            l1.add(t);
        }

        for (int t : ar2) {
            l2.add(t);
        }

        return listsEquivalent(l1, l2);
    }

    public static char[][] copy(char[][] grid) {
        if (grid == null) {
            return null;
        }
        if (grid.length == 0 || grid[0].length == 0) {
            return new char[0][];
        }

        char[][] copy = new char[grid.length][grid[0].length];

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                copy[i][j] = grid[i][j];
            }
        }
        return copy;
    }

    public static char[][] createRandomOneZeroCharGrid(int rowAmount, int columnAmount) {
        char[][] grid = new char[rowAmount][columnAmount];
        Random random = new Random();
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                grid[i][j] = random.nextBoolean() ? '1' : '0';
            }
        }
        return grid;
    }

    public static String randomAlphanumeric(int lenght) {
        Random random = new Random();
        char[] result = new char[lenght];
        for (int i = 0; i < result.length; i++) {
            char randomChar = ALPHANUMERICS.charAt(random.nextInt(ALPHANUMERICS.length()));
            result[i] = randomChar;
        }
        return String.valueOf(result);
    }

    public static int[] randomArray(int length, int topBound) {
        int[] result = new int[length];
        Random random = new Random();
        for (int i = 0; i < result.length; i++) {
            result[i] = random.nextInt(topBound);
        }
        return result;
    }

    public static ListNode createList(int... array) {
        if (array == null || array.length == 0) {
            return null;
        }

        ListNode head = new ListNode(0);
        ListNode current = head;

        for (int i = 0; i < array.length; i++) {
            int val = array[i];
            current.next = new ListNode(val);
            current = current.next;
        }
        return head.next;
    }
}
