package com.gridnev;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Created by a.gridnev on 19/01/2016.
 */
public class TwoSumIIITest
        extends TestCase
{
    public TwoSumIIITest(String testName )
    {
        super( testName );
    }

    public static Test suite()
    {
        return new TestSuite( TwoSumIIITest.class );
    }

    public void testEmpty(){
        final TwoSumIII twoSumIII = new TwoSumIII();

        assertFalse(twoSumIII.find(1));
    }

    public void test1(){
        final TwoSumIII twoSumIII = new TwoSumIII();

        twoSumIII.add(1);
        assertFalse(twoSumIII.find(1));
    }

    public void test2(){
        final TwoSumIII twoSumIII = new TwoSumIII();

        twoSumIII.add(1);
        twoSumIII.add(3);
        assertTrue(twoSumIII.find(4));
    }

    public void test3(){
        final TwoSumIII twoSumIII = new TwoSumIII();

        twoSumIII.add(1);
        twoSumIII.add(3);
        twoSumIII.add(4);
        assertTrue(twoSumIII.find(4));
        assertTrue(twoSumIII.find(7));
        assertTrue(twoSumIII.find(5));
        assertFalse(twoSumIII.find(6));
    }
}
