package com.gridnev;

import junit.framework.TestCase;

public class TwoSumInputedArraySortedTest extends TestCase {

    public void testName() throws Exception {
        TwoSumInputedArraySorted sut = new TwoSumInputedArraySorted();

        int[] result_33 = sut.twoSum(new int[]{1, 3, 5, 9, 16, 17, 18, 20, 32,}, 22);
        assertEquals(3, result_33[0]);
        assertEquals(6, result_33[1]);
    }
}