package com.gridnev;

import junit.framework.TestCase;

public class ValidPalindromTest extends TestCase {

    private ValidPalindrom sut;

    public void setUp() throws Exception {
        sut = new ValidPalindrom();
    }

    public void testName() throws Exception {
        assertTrue(sut.isPalindrome(""));
        assertTrue(sut.isPalindrome(",."));
        assertTrue(sut.isPalindrome(".A"));
        assertTrue(sut.isPalindrome("1.A.,1,,,  "));
    }
}