package com.gridnev;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Created by a.gridnev on 19/01/2016.
 */
public class ValidSudokuTest
        extends TestCase {
    public ValidSudokuTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        return new TestSuite(ValidSudokuTest.class);
    }

    public void test1() {
        final ValidSudoku validSudoku = new ValidSudoku();

        char[][] board = new char[][]{
                {'1', '.', '.', '.', '.', '.', '.', '.', '.'},
                {'.', '2', '.', '.', '.', '.', '.', '.', '.'},
                {'.', '.', '3', '.', '.', '.', '.', '.', '.'},
                {'.', '.', '.', '4', '.', '.', '.', '.', '.'},
                {'.', '.', '.', '.', '5', '.', '.', '.', '.'},
                {'.', '.', '.', '.', '.', '6', '.', '.', '.'},
                {'.', '.', '.', '.', '.', '.', '7', '.', '.'},
                {'.', '.', '.', '.', '.', '.', '.', '8', '.'},
                {'.', '.', '.', '.', '.', '.', '.', '.', '9'}
        };

        final boolean validSudoku1 = validSudoku.isValidSudoku(board);
        assertTrue(validSudoku1);
    }
}
