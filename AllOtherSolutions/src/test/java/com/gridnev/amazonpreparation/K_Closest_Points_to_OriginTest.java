package com.gridnev.amazonpreparation;

import junit.framework.TestCase;

public class K_Closest_Points_to_OriginTest extends TestCase {

    public void testKClosest1() {
        K_Closest_Points_to_Origin suite = new K_Closest_Points_to_Origin();

        int[][] ints = suite.kClosest(new int[][]{{1, 3}, {-2, 2}}, 1);
        assertEquals(1, ints.length);
        System.out.println();
        for (int i = 0; i < ints.length; i++) {
            System.out.println(ints[i][0] + ", " + ints[i][1]);
        }
    }

    public void testKClosest2() {
        K_Closest_Points_to_Origin suite = new K_Closest_Points_to_Origin();

        int[][] ints = suite.kClosest(new int[][]{{3, 3}, {5, -1}, {-2, 4}}, 2);

        assertEquals(2, ints.length);
        System.out.println();

        for (int i = 0; i < ints.length; i++) {
            System.out.println(ints[i][0] + ", " + ints[i][1]);
        }
    }
}