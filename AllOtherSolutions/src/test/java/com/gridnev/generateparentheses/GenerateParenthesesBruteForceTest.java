package com.gridnev.generateparentheses;

import com.gridnev.TestUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class GenerateParenthesesBruteForceTest {
    @DataProvider
    public static Object[][] amountOfN() {
        return new Object[][]{{1}, {2}, {3}, {4}, {5}, {6}};
    }

    @Test(dataProvider = "amountOfN")
    public void testGenerate(int n) throws Exception {
        assertTrue(
                TestUtils.listsEquivalent(
                        GenerateParenthesesBruteForce.generate(n),
                        GenerateParenthesesBacktracking.generate(n)));
    }
}