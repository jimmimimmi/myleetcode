package com.gridnev.guidewire;

import junit.framework.TestCase;

public class FindDuplicatePhoneNumberTest extends TestCase {

    private FindDuplicatePhoneNumber sut;

    public void setUp() throws Exception {
        sut = new FindDuplicatePhoneNumber();
    }

    public void testLongs() {
        assertFalse(sut.isThereAnyDuplicates(new long[]{1}));
        assertFalse(sut.isThereAnyDuplicates(new long[]{Integer.MAX_VALUE
                , 2, 3}));
        assertFalse(sut.isThereAnyDuplicates(new long[]{}));
        assertFalse(sut.isThereAnyDuplicates((long[]) null));

        assertTrue(sut.isThereAnyDuplicates(new long[]{1, 2, 3, 2}));
        assertTrue(sut.isThereAnyDuplicates(new long[]{1, 0, Long.MAX_VALUE, Long.MAX_VALUE}));
        assertTrue(sut.isThereAnyDuplicates(new long[]{Long.MAX_VALUE, 1, 0, Long.MAX_VALUE, Long.MAX_VALUE}));
    }

    public void testStrings() {
        assertFalse(sut.isThereAnyDuplicates(new String[]{1 + ""}));
        assertFalse(sut.isThereAnyDuplicates(new String[]{1 + "", 2 + "", 3 + ""}));
        assertFalse(sut.isThereAnyDuplicates(new String[]{}));
        assertFalse(sut.isThereAnyDuplicates((String[]) null));

        assertTrue(sut.isThereAnyDuplicates(new String[]{1 + "", 2 + "", 3 + "", 2 + ""}));
        assertTrue(sut.isThereAnyDuplicates(new String[]{1 + "", 0 + "", Long.MAX_VALUE + "", Long.MAX_VALUE + ""}));
        assertTrue(sut.isThereAnyDuplicates(new String[]{Long.MAX_VALUE + "", 1 + "", 0 + "", Long.MAX_VALUE + "", Long.MAX_VALUE + ""}));
    }
}