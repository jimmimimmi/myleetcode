package com.gridnev.guidewire.binarytree;

import junit.framework.TestCase;

public class InOrderTraversalTest extends TestCase {
    private InOrderTraversal inOrderTravesral;

    public void setUp() throws Exception {
        inOrderTravesral = new InOrderTraversal();
    }

    public void testName() throws Exception {
        TreeNode rootNode = TreeNodeFactory.create();
        System.out.println(inOrderTravesral.inOrderIterative(rootNode));
        System.out.println(inOrderTravesral.inOrderRecursive(rootNode));
    }

}