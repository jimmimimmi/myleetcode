package com.gridnev.guidewire.binarytree;

import junit.framework.TestCase;

public class PostOrderTraversalTest extends TestCase {
    private PostOrderTraversal postOrderTravesral;

    public void setUp() throws Exception {
        postOrderTravesral = new PostOrderTraversal();
    }

    public void testName() throws Exception {
        TreeNode rootNode = TreeNodeFactory.create();
        System.out.println(postOrderTravesral.postOrderRecursive(rootNode));
        System.out.println(postOrderTravesral.postOrderIterative(rootNode));
    }
}