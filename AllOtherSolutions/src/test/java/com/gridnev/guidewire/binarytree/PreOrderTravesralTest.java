package com.gridnev.guidewire.binarytree;

import junit.framework.TestCase;

public class PreOrderTravesralTest extends TestCase {

    private PreOrderTravesral preOrderTravesral;

    public void setUp() throws Exception {
        preOrderTravesral = new PreOrderTravesral();
    }

    public void testName() throws Exception {
        TreeNode rootNode = TreeNodeFactory.create();
        System.out.println(preOrderTravesral.preOrderIterative(rootNode));
        System.out.println(preOrderTravesral.preOrderRecursive(rootNode));
    }
}