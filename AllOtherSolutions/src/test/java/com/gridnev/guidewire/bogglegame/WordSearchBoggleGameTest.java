package com.gridnev.guidewire.bogglegame;

import org.testng.annotations.Test;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class WordSearchBoggleGameTest {
    @Test
    public void testName() throws Exception {
        char[][] board = {
                {'o', 'a', 'a', 'n'},
                {'e', 't', 'a', 'e'},
                {'i', 'h', 'k', 'r'},
                {'i', 'f', 'l', 'v'}
        };
        String[] words = {"oath", "pea", "eat", "rain"};

        List<String> stringList = WordSearchBoggleGame.findWords(board, words);
        assertEquals(stringList.size(), 2);
        assertTrue(stringList.contains("oath"));
        assertTrue(stringList.contains("eat"));
        System.out.println(stringList);
    }

    @Test
    public void testName2() throws Exception {
        char[][] board = {{'a', 'a'}};
        String[] words = {"a"};

        List<String> stringList = WordSearchBoggleGame.findWords(board, words);
        assertEquals(stringList.size(), 1);
        assertTrue(stringList.contains("a"));
        System.out.println(stringList);
    }

    @Test
    public void testName3() throws Exception {
        char[][] board = {
                {'a', 'b', 'c'},
                {'a', 'e', 'd'},
                {'a', 'f', 'g'}
        };


        String[] words = {"abcdefg", "gfedcbaaa", "eaabcdgfa", "befa", "dgc", "ade"};
//        String[] words = { "eaabcdgfa"};

        List<String> stringList = WordSearchBoggleGame.findWords(board, words);
        System.out.println(stringList);
        assertEquals(stringList.size(), 4);

        assertTrue(stringList.contains("abcdefg"));
        assertTrue(stringList.contains("befa"));
        assertTrue(stringList.contains("eaabcdgfa"));
        assertTrue(stringList.contains("gfedcbaaa"));
    }
}