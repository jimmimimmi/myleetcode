package com.gridnev.guidewire.flattenList;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FlattenLinkedListsTest extends TestCase {

    public void testNoQueue() {
        Node head = createList();

        FlattenLinkedLists.flattenListNoQueue(head);
        List<Integer> printList = FlattenLinkedListUtil.printList(head);
        List<Integer> integers = Arrays.asList(10, 5, 12, 7, 11, 4, 20, 13, 17, 6, 2, 16, 9, 8, 3, 19, 15);

        assertLists(integers, printList);
    }

    public void testWithQueue() {
        Node head = createList();

        FlattenLinkedLists.flattenListWithQueue(head);
        List<Integer> printList = FlattenLinkedListUtil.printList(head);
        List<Integer> integers = Arrays.asList(10, 5, 12, 7, 11, 4, 20, 13, 17, 6, 2, 16, 9, 8, 3, 19, 15);

        assertLists(integers, printList);
    }

    public void testNoQueueVertical() {
        int arr1[] = new int[]{2};
        int arr2[] = new int[]{16};
        int arr3[] = new int[]{3};

        Node head1 = FlattenLinkedListUtil.createList(arr1);
        Node head2 = FlattenLinkedListUtil.createList(arr2);
        Node head3 = FlattenLinkedListUtil.createList(arr3);

        head1.setChild(head2);
        head2.setChild(head3);

        FlattenLinkedLists.flattenListNoQueue(head1);
        List<Integer> printList = FlattenLinkedListUtil.printList(head1);
    }

    public void testName() {
        String regex = "\\d{1,2}?";
        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher("123abc567");

        while (matcher.find()){
            System.out.println(matcher.group());
        }
    }

    private void assertLists(List<Integer> expected, List<Integer> actual) {

        assertEquals(actual.size(), expected.size());

        for (int i = 0; i < actual.size(); i++) {
            assertEquals(actual.get(i), expected.get(i));
        }

    }

    private Node createList() {
        int arr1[] = new int[]{10, 5, 12, 7, 11};
        int arr2[] = new int[]{4, 20, 13};
        int arr3[] = new int[]{17, 6};
        int arr4[] = new int[]{9, 8};
        int arr5[] = new int[]{19, 15};
        int arr6[] = new int[]{2};
        int arr7[] = new int[]{16};
        int arr8[] = new int[]{3};

        /* create 8 linked lists */
        Node head1 = FlattenLinkedListUtil.createList(arr1);
        Node head2 = FlattenLinkedListUtil.createList(arr2);
        Node head3 = FlattenLinkedListUtil.createList(arr3);
        Node head4 = FlattenLinkedListUtil.createList(arr4);
        Node head5 = FlattenLinkedListUtil.createList(arr5);
        Node head6 = FlattenLinkedListUtil.createList(arr6);
        Node head7 = FlattenLinkedListUtil.createList(arr7);
        Node head8 = FlattenLinkedListUtil.createList(arr8);

        /* modify child pointers to create the list shown above */
        head1.setChild(head2);
        head1.getNext().getNext().getNext().setChild(head3);
        head3.setChild(head4);
        head4.setChild(head5);
        head2.getNext().setChild(head6);
        head2.getNext().getNext().setChild(head7);
        head7.setChild(head8);

        /* Return head pointer of first linked list.  Note that all nodes are
         reachable from head1 */
        return head1;
    }
}