package com.gridnev.guidewire.iteratorof2darray;

import junit.framework.TestCase;

import java.util.ArrayList;

public class IteratorOf2DBasedOnIteratorsArraySkippingNullsTest extends TestCase {
    public void testNullArray() {
        IteratorOf2DBasedOnIteratorsArraySkippingNulls<String> nullIterator = new IteratorOf2DBasedOnIteratorsArraySkippingNulls<>((String[][]) null);
        assertFalse(nullIterator.hasNext());
    }

    public void testEmptyArray() {
        assertFalse(new IteratorOf2DBasedOnIteratorsArraySkippingNulls<>(new String[0][]).hasNext());
        assertFalse(new IteratorOf2DBasedOnIteratorsArraySkippingNulls<>(new String[10][]).hasNext());
        assertFalse(new IteratorOf2DBasedOnIteratorsArraySkippingNulls<>(new String[10][2]).hasNext());
    }

    public void testGeneral() {
        IteratorOf2DBasedOnIteratorsArraySkippingNulls<String> iterator = new IteratorOf2DBasedOnIteratorsArraySkippingNulls<>(new String[][]{
                null,
                {},
                {"a", null, "b"},
                {},
                {null, "c", null, "d"},
                {null, null, "e"},
                {null},
        });

        ArrayList<String> result = new ArrayList<>();
        while (iterator.hasNext()) {
            result.add(iterator.next());
        }

        assertEquals(result.size(), 5);
        assertEquals(result.get(0), "a");
        assertEquals(result.get(1), "b");
        assertEquals(result.get(2), "c");
        assertEquals(result.get(3), "d");
        assertEquals(result.get(4), "e");
    }
}