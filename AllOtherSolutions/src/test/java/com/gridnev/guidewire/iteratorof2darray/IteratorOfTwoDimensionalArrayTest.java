package com.gridnev.guidewire.iteratorof2darray;

import com.gridnev.guidewire.iteratorof2darray.IteratorOfTwoDimensionalArray;
import junit.framework.TestCase;

import java.util.ArrayList;

public class IteratorOfTwoDimensionalArrayTest extends TestCase {
    public void testEmptyArray() {
//        assertFalse(new IteratorOfTwoDimensionalArray<String>(new String[0][]).hasNext());
//        assertFalse(new IteratorOfTwoDimensionalArray<String>(new String[10][]).hasNext());
        assertFalse(new IteratorOfTwoDimensionalArray<String>(new String[10][2]).hasNext());
    }

    public void testGeneral() {
        IteratorOfTwoDimensionalArray<String> iterator = new IteratorOfTwoDimensionalArray<>(new String[][]{
                {},
                {"a", null, "b"},
                {},
                {null, "c", null, "d"},
                {null, null, "e"},
                {null},
        });

        ArrayList<String> result = new ArrayList<>();
        while (iterator.hasNext()) {
            result.add(iterator.next());
        }

        assertEquals(result.size(), 11);
        assertEquals(result.get(0), "a");
        assertEquals(result.get(1), null);
        assertEquals(result.get(2), "b");
        assertEquals(result.get(3), null);
        assertEquals(result.get(4), "c");
        assertEquals(result.get(5), null);
        assertEquals(result.get(6), "d");
        assertEquals(result.get(7), null);
        assertEquals(result.get(8), null);
        assertEquals(result.get(9), "e");
    }
}