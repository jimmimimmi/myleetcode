package com.gridnev.guidewire.preparationtotest;

import org.testng.annotations.Test;

import java.util.List;

public class SpiralMatrixTest {

    @Test
    public void testSpiralOrder() throws Exception {

        List<Integer> integers = SpiralMatrix.spiralOrder(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});

        System.out.println(integers);
    }
}