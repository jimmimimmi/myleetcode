package com.gridnev.guidewire.preparationtotest;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class StringToIntegerAtoiTest {

    @Test
    public void testMyAtoi() throws Exception {
        int result = new StringToIntegerAtoi().myAtoi("-1");

        assertEquals(result, -1);
    }
}