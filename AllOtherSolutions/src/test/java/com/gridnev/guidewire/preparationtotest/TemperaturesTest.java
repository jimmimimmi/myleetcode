package com.gridnev.guidewire.preparationtotest;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static junit.framework.Assert.assertEquals;

/*
public class SolutionTest {
    private Solution solution;

    @BeforeMethod
    public void setUp() throws Exception {
        solution = new Solution();
    }

    @Test
    public void testFromGivenDescription1() throws Exception {
        assertEquals("SUMMER", solution.solution(new int[]{-3, -14, -5, 7, 8, 42, 8, 3}));
    }

    @Test
    public void testFromGivenDescription2() throws Exception {
        assertEquals("AUTUMN", solution.solution(new int[]{2, -3, 3, 1, 10, 8, 2, 5, 13, -5, 3, -18}));
    }

    @Test
    public void onlyNegativeValues() throws Exception {
        assertEquals("AUTUMN", solution.solution(new int[]{-2, -12, -2, -13, -2, -14, -2, -15}));
    }

    @Test
    public void negativeAmplitudeIsHigherThenPositive() throws Exception {
        assertEquals("SPRING", solution.solution(new int[]{2, 12, -2, -13, 2, 4, 2, 5}));
    }
}

*/