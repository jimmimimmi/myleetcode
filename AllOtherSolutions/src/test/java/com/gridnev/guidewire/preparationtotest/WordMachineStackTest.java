package com.gridnev.guidewire.preparationtotest;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static junit.framework.Assert.assertEquals;

public class WordMachineStackTest {

    private WordMachineStack sut;

    @BeforeMethod
    public void setUp() throws Exception {
        sut = new WordMachineStack();
    }

    @Test
    public void testEmpty() throws Exception {
        assertEquals(-1, sut.solution(null));
        assertEquals(-1, sut.solution(""));
        assertEquals(-1, sut.solution(" "));
    }

    @Test
    public void errorDuringDoublePop() throws Exception {
        String str = "13 POP POP";
        assertEquals(-1, sut.solution(str));
    }

    @Test
    public void errorDuringEmptyDup() throws Exception {
        String str = "13 POP DUP";
        assertEquals(-1, sut.solution(str));
    }

    @Test
    public void negativePush() throws Exception {
        String str = "13 -2";
        assertEquals(-1, sut.solution(str));
    }

    @Test
    public void plus() throws Exception {
        String str = "13 2 +";
        assertEquals(15, sut.solution(str));
    }

    @Test
    public void negativeValueAfterSubtraction() throws Exception {
        String str = "13 2 -";
        assertEquals(-1, sut.solution(str));
    }

    @Test
    public void minus() throws Exception {
        String str = "13 24 -";
        assertEquals(11, sut.solution(str));
    }

    @Test
    public void onePush() throws Exception {
        String str = "13";
        assertEquals(13, sut.solution(str));
    }

    @Test
    public void severalPushes() throws Exception {
        String str = "13 12 11";
        assertEquals(11, sut.solution(str));
    }

    @Test
    public void duplicate() throws Exception {
        String str = "13 DUP POP";
        assertEquals(13, sut.solution(str));
    }

    @Test
    public void sunnyDayTest() throws Exception {
        String stringFromInitialSolutionDescription = "13 DUP 4 POP 5 DUP + DUP + -";
        assertEquals(7, sut.solution(stringFromInitialSolutionDescription));
    }


    @Test
    public void pushMaxValue() throws Exception {
        int maxAllowedValue = (1 << 20) - 1;
        String str = maxAllowedValue + "";
        assertEquals(maxAllowedValue, sut.solution(str));
    }

    @Test
    public void pushMaxValueIncreasedByOne() throws Exception {
        int maxAllowedValue = (1 << 20) - 1;
        String str = maxAllowedValue + 1 + "";
        assertEquals(-1, sut.solution(str));
    }

    @Test
    public void maxValueSubtraction() throws Exception {
        String str = "1048574 1 + +";
        assertEquals(-1, sut.solution(str));
    }

    @Test
    public void overflowAfterAddOperation() throws Exception {
        int maxAllowedValue = (1 << 20) - 1;
        String str = maxAllowedValue + " 1 +";
        assertEquals(-1, sut.solution(str));
    }

    @Test
    public void fewTypesPop() throws Exception {
        String str = "13 POP 14 15 POP POP 16";
        assertEquals(16, sut.solution(str));
    }
}