package com.gridnev.guidewire.sparsematrix;

import junit.framework.TestCase;

public class SparseMatrixBasedOnHashmapTest extends TestCase {

    public void testName() {
        SparseMatrixBasedOnHashmap<Integer> matrix = new SparseMatrixBasedOnHashmap<>(
                new Integer[][]{{1, null, null}, {null, 2, null}}
        );

        assertNull(matrix.get(0, 1));
        assertNull(matrix.get(0, 2));
        assertNull(matrix.get(1, 0));
        assertNull(matrix.get(1, 2));

        assertEquals((int) matrix.get(0, 0), 1);
        assertEquals((int) matrix.get(1, 1), 2);

        boolean exceptionWasThrown = false;
        try {
            matrix.get(1, 3);
        } catch (IllegalArgumentException e) {
            exceptionWasThrown = true;
        }

        assertTrue(exceptionWasThrown);

    }
}