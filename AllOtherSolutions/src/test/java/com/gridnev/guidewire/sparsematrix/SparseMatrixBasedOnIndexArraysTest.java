package com.gridnev.guidewire.sparsematrix;

import junit.framework.TestCase;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Random;

public class SparseMatrixBasedOnIndexArraysTest extends TestCase {

    private Integer[][] originalMatrix1;
    private Integer[][] originalMatrix2;
    private SparseMatrixBasedOnIndexArrays<Integer> sparseMatrix1;
    private SparseMatrixBasedOnIndexArrays<Integer> sparseMatrix2;

    @BeforeMethod
    public void setUp() throws Exception {
        super.setUp();

        originalMatrix1 = new Integer[][]{
                {1, 2, null, null, null},
                {3, null, null, 4, null},
                {null, null, null, null, null},
                {null, null, null, null, 5},
                {6, null, null, null, null}
        };
        originalMatrix2 = new Integer[][]{
                {null, null, null, null, null},
                {3, null, null, 4, null},
                {5, null, null, null, null},
                {null, null, null, null, 6},
                {null, null, null, null, null}
        };
        sparseMatrix1 = new SparseMatrixBasedOnIndexArrays<>(originalMatrix1);
        sparseMatrix2 = new SparseMatrixBasedOnIndexArrays<>(originalMatrix2);
    }

    @Test
    public void testConstructorAndGetter() {
        assertMatrices(originalMatrix1, sparseMatrix1);
        assertMatrices(originalMatrix2, sparseMatrix2);
    }

    @Test
    public void testDummyReplaceNullByNull() {
        sparseMatrix1.set(0, 2, null);
        sparseMatrix2.set(0, 0, null);

        assertMatrices(originalMatrix1, sparseMatrix1);
        assertMatrices(originalMatrix2, sparseMatrix2);
    }
    String a;

    @Test
    public void testReplaceValueByAnotherValue() {


        System.out.println(a);



    }

    @Test
    public void testReplaceNullByNotNull() {
        sparseMatrix1.set(0, 2, 4);
        originalMatrix1[0][2] = 4;

        assertMatrices(originalMatrix1, sparseMatrix1);
    }

    @Test
    public void testReplaceValueByNull() {
        sparseMatrix1.set(1, 0, null);
        originalMatrix1[1][0] = null;

        assertMatrices(originalMatrix1, sparseMatrix1);
    }

    @Test
    public void testReplaceNullByNotNullAndBack() {
        sparseMatrix1.set(0, 2, 4);
        originalMatrix1[0][2] = 4;

        assertMatrices(originalMatrix1, sparseMatrix1);

        sparseMatrix1.set(0, 2, null);
        originalMatrix1[0][2] = null;

        assertMatrices(originalMatrix1, sparseMatrix1);
    }

    @Test(invocationCount = 10)
    public void testComplexTest() {

        Integer[] newValues = {null, 1, 2, 3, 4, null, 5, 6, null};
        Random random = new Random();

        for (int i = 0; i < originalMatrix1.length; i++) {
            for (int j = 0; j < originalMatrix1[0].length; j++) {
                Integer newValue = newValues[random.nextInt(newValues.length)];
                sparseMatrix1.set(i, j, newValue);
                originalMatrix1[i][j] = newValue;
                assertEquals(originalMatrix1[i][j], sparseMatrix1.get(i, j));
            }
        }
    }

    private void assertMatrices(
            Integer[][] originalMatrix1, SparseMatrixBasedOnIndexArrays<Integer> sparseMatrix1) {
        for (int i = 0; i < originalMatrix1.length; i++) {
            for (int j = 0; j < originalMatrix1[0].length; j++) {
                assertEquals(originalMatrix1[i][j], sparseMatrix1.get(i, j));
            }
        }
    }
}