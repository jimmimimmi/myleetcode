package com.gridnev.intervals;

import com.gridnev.TestUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertTrue;

public class MergeIntervalsTest {

    @DataProvider
    public static Object[][] intervals() {
        return new Object[][]{
                {Interval.create(new int[]{1, 2})},
                {Interval.create(new int[]{1, 2}, new int[]{2, 3}, new int[]{3, 4})},
                {Interval.create(new int[]{1, 3}, new int[]{2, 4}, new int[]{5, 6})},
                {Interval.create(new int[]{2, 5}, new int[]{1, 4}, new int[]{5, 7})},
                {Interval.create(new int[]{2, 5}, new int[]{4, 8}, new int[]{5, 7})},
                {Interval.create(new int[]{2, 5}, new int[]{6, 9}, new int[]{5, 7}, new int[]{10, 12})},
                {Interval.create(new int[]{1, 9}, new int[]{2, 5}, new int[]{19, 20}, new int[]{10, 11}, new int[]{12, 20}, new int[]{0, 3}, new int[]{0, 1}, new int[]{0, 2})},
        };
    }

    @Test(dataProvider = "intervals")
    public void merge(List<Interval> intervals) throws Exception {

        List<Interval> unionFindResult = new MergeIntervals.UnionFindSolution().merge(intervals);
        List<Interval> sortingResult = new MergeIntervals.SortingSolution().merge(intervals);

        System.out.println("original: " + intervals);
        System.out.println("union find: " + unionFindResult);
        System.out.println("sorting: " + sortingResult);
        assertTrue(TestUtils.listsEquivalent(unionFindResult, sortingResult));
    }
}