package com.gridnev.leetcodetop100likedquestions;

import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class FlattenListUtilTest {

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void exceptionIfTheMainListIsNull() {
        FlattenListUtil.flatten(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void exceptionIfListContainsNull() {
        FlattenListUtil.flatten(asList(1, 2, null, 4));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void exceptionIfListContainsNotInteger() {
        FlattenListUtil.flatten(asList(1, 2, 3.0, 4));
    }

    @Test
    public void emptyResultOnEmptyList() {
        List<Integer> result = FlattenListUtil.flatten(Collections.emptyList());
        assertTrue(result.isEmpty());
    }

    @Test
    public void emptyResultIfListContainsOnlyEmptyNestedLists() {
        List<Integer> result = FlattenListUtil.flatten(
                asList(
                        Collections.emptyList(),
                        Collections.emptyList()));

        assertTrue(result.isEmpty());
    }

    @Test
    public void flattenListWhichContainsOnlyLists() {
        List<Integer> result =
                FlattenListUtil.flatten(
                        asList(
                                asList(1, 2, asList(3)),
                                Collections.emptyList(),
                                asList(4))
                );

        assertEquals(4, result.size());

        assertEquals(1, result.get(0).intValue());
        assertEquals(2, result.get(1).intValue());
        assertEquals(3, result.get(2).intValue());
        assertEquals(4, result.get(3).intValue());
    }

    @Test
    public void flattenListWhichContainsOnlyIntegers() {
        List<Integer> result = FlattenListUtil.flatten(asList(1, 2, 3, 4));

        assertEquals(4, result.size());

        assertEquals(1, result.get(0).intValue());
        assertEquals(2, result.get(1).intValue());
        assertEquals(3, result.get(2).intValue());
        assertEquals(4, result.get(3).intValue());
    }

    @Test
    public void flattenListWhichContainsBothNestedListsAndIntegers() {
        List<Integer> result = FlattenListUtil.flatten(
                asList(
                        1,
                        asList(2),
                        asList(3, asList(4)),
                        5));

        assertEquals(5, result.size());

        assertEquals(1, result.get(0).intValue());
        assertEquals(2, result.get(1).intValue());
        assertEquals(3, result.get(2).intValue());
        assertEquals(4, result.get(3).intValue());
        assertEquals(5, result.get(4).intValue());
    }
}