package com.gridnev.leetcodetop100likedquestions;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class LRUCacheTest {

    @Test
    public void testName() {
        LRUCache cache = new LRUCache(2);
        /*
        ["LRUCache","put","put","get","put","get","put","get","get","get"]
        [[2],[1,1],[2,2],[1],[3,3],[2],[4,4],[1],[3],[4]]
         */

        cache.put(1, 1);
        cache.put(2, 2);
        assertEquals(cache.get(1), 1);
        cache.put(3, 3);
        assertEquals(cache.get(2), 2);
        cache.put(4, 4);
        assertEquals(cache.get(1), -1);
        assertEquals(cache.get(3), 3);
        assertEquals(cache.get(4), 4);
    }
}