package com.gridnev.numberofislands;

import com.gridnev.TestUtils;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class NumberOfIslandsDFSTest {

    @DataProvider
    public static Object[][] grid() {


        return new Object[][]{
                {TestUtils.createRandomOneZeroCharGrid(5, 5)},
                {TestUtils.createRandomOneZeroCharGrid(4, 5)},
                {TestUtils.createRandomOneZeroCharGrid(3, 5)},
                {TestUtils.createRandomOneZeroCharGrid(10, 8)},
        };
    }

    @Test(dataProvider = "grid")
    public void testNumIslands(char[][] grid) throws Exception {
        int dfs = NumberOfIslandsDFS.numIslands(TestUtils.copy(grid));
        int bfs = NumberOfIslandsBFS.numIslands(TestUtils.copy(grid));
        int union = NumberOfIslandsUnionFind.numIslands(TestUtils.copy(grid));

        System.out.println("dfs: " + dfs + ", bfs: " + bfs + ", union find: " + union);

        Assert.assertEquals(dfs, bfs);
        Assert.assertEquals(dfs, union);
    }
}