package com.gridnev.quotixinterview;

import junit.framework.TestCase;

public class BeginEndArrayFactoryTest extends TestCase {
    private BeginEndArrayFactory sut;

    public void setUp() throws Exception {
        super.setUp();
        sut = new BeginEndArrayFactory();
    }

    public void testExcludeOneIndexInBeginning() throws Exception {
        BeginEndPair[] beginEndPairs = sut.create(new int[]{0}, 8);

        assertEquals(beginEndPairs.length, 1);
        assertEquals(beginEndPairs[0].getBegin(), 1);
        assertEquals(beginEndPairs[0].getEnd(), 8);
    }

    public void testExcludeTwoConsecutiveIndexesInBeginning() throws Exception {
        BeginEndPair[] beginEndPairs = sut.create(new int[]{0, 1}, 8);

        assertEquals(beginEndPairs.length, 1);
        assertEquals(beginEndPairs[0].getBegin(), 2);
        assertEquals(beginEndPairs[0].getEnd(), 8);
    }

    public void testExcludeThreeConsecutiveIndexesInBeginning() throws Exception {
        BeginEndPair[] beginEndPairs = sut.create(new int[]{0, 1, 2}, 8);

        assertEquals(beginEndPairs.length, 1);
        assertEquals(beginEndPairs[0].getBegin(), 3);
        assertEquals(beginEndPairs[0].getEnd(), 8);
    }


    public void testExcludeOneIndexInEnd() throws Exception {
        BeginEndPair[] beginEndPairs = sut.create(new int[]{8}, 8);

        assertEquals(beginEndPairs.length, 1);
        assertEquals(beginEndPairs[0].getBegin(), 0);
        assertEquals(beginEndPairs[0].getEnd(), 7);
    }

    public void testExcludeTwoConsecutiveIndexesInEnd() throws Exception {
        BeginEndPair[] beginEndPairs = sut.create(new int[]{7, 8}, 8);

        assertEquals(beginEndPairs.length, 1);
        assertEquals(beginEndPairs[0].getBegin(), 0);
        assertEquals(beginEndPairs[0].getEnd(), 6);
    }

    public void testExcludeThreeConsecutiveIndexesInEnd() throws Exception {
        BeginEndPair[] beginEndPairs = sut.create(new int[]{6, 7, 8}, 8);

        assertEquals(beginEndPairs.length, 1);
        assertEquals(beginEndPairs[0].getBegin(), 0);
        assertEquals(beginEndPairs[0].getEnd(), 5);
    }

    public void testExcludeOneIndexStartingWith1nd() throws Exception {
        BeginEndPair[] beginEndPairs = sut.create(new int[]{1}, 8);

        assertEquals(beginEndPairs.length, 2);
        assertEquals(beginEndPairs[0].getBegin(), 0);
        assertEquals(beginEndPairs[0].getEnd(), 0);

        assertEquals(beginEndPairs[1].getBegin(), 2);
        assertEquals(beginEndPairs[1].getEnd(), 8);
    }

    public void testExcludeTwoConsecutiveIndexesStartingWith1nd() throws Exception {
        BeginEndPair[] beginEndPairs = sut.create(new int[]{1, 2}, 8);

        assertEquals(beginEndPairs.length, 2);
        assertEquals(beginEndPairs[0].getBegin(), 0);
        assertEquals(beginEndPairs[0].getEnd(), 0);

        assertEquals(beginEndPairs[1].getBegin(), 3);
        assertEquals(beginEndPairs[1].getEnd(), 8);
    }

    public void testExcludeThreeConsecutiveIndexesStartingWith1nd() throws Exception {
        BeginEndPair[] beginEndPairs = sut.create(new int[]{1, 2, 3}, 8);

        assertEquals(beginEndPairs.length, 2);
        assertEquals(beginEndPairs[0].getBegin(), 0);
        assertEquals(beginEndPairs[0].getEnd(), 0);

        assertEquals(beginEndPairs[1].getBegin(), 4);
        assertEquals(beginEndPairs[1].getEnd(), 8);
    }

    public void testExcludeOneIndexInMiddle() throws Exception {
        BeginEndPair[] beginEndPairs = sut.create(new int[]{2}, 8);

        assertEquals(beginEndPairs.length, 2);
        assertEquals(beginEndPairs[0].getBegin(), 0);
        assertEquals(beginEndPairs[0].getEnd(), 1);

        assertEquals(beginEndPairs[1].getBegin(), 3);
        assertEquals(beginEndPairs[1].getEnd(), 8);
    }

    public void testExcludeTwoConsecutiveIndexesInMiddle() throws Exception {
        BeginEndPair[] beginEndPairs = sut.create(new int[]{2, 3}, 8);

        assertEquals(beginEndPairs.length, 2);
        assertEquals(beginEndPairs[0].getBegin(), 0);
        assertEquals(beginEndPairs[0].getEnd(), 1);

        assertEquals(beginEndPairs[1].getBegin(), 4);
        assertEquals(beginEndPairs[1].getEnd(), 8);
    }

    public void testExcludeThreeConsecutiveIndexesInMiddle() throws Exception {
        BeginEndPair[] beginEndPairs = sut.create(new int[]{2, 3, 4}, 8);

        assertEquals(beginEndPairs.length, 2);
        assertEquals(beginEndPairs[0].getBegin(), 0);
        assertEquals(beginEndPairs[0].getEnd(), 1);

        assertEquals(beginEndPairs[1].getBegin(), 5);
        assertEquals(beginEndPairs[1].getEnd(), 8);
    }

    public void testExcludeTwoRandomIndexes() throws Exception {
        BeginEndPair[] beginEndPairs = sut.create(new int[]{2,5}, 8);

        assertEquals(beginEndPairs.length, 3);
        assertEquals(beginEndPairs[0].getBegin(), 0);
        assertEquals(beginEndPairs[0].getEnd(), 1);

        assertEquals(beginEndPairs[1].getBegin(), 3);
        assertEquals(beginEndPairs[1].getEnd(), 4);

        assertEquals(beginEndPairs[2].getBegin(), 6);
        assertEquals(beginEndPairs[2].getEnd(), 8);
    }
}