package com.gridnev.quotixinterview;

import junit.framework.TestCase;

public class MultiplyExceptTest extends TestCase {

    public void testExceptOneIndex() throws Exception {
        assertEquals(new MultiplyExcept(new int[]{1, 2}).multiplyExcept(0), 2L);
        assertEquals(new MultiplyExcept(new int[]{1, 2}).multiplyExcept(1), 1L);
        assertEquals(new MultiplyExcept(new int[]{1, 2, 3}).multiplyExcept(2), 2L);
    }

    public void testExceptTwoIndexes() throws Exception {
        MultiplyExcept threeElements = new MultiplyExcept(new int[]{1, 2, 3});
        MultiplyExcept fourElements = new MultiplyExcept(new int[]{1, 2, 3, 4});

        assertEquals(threeElements.multiplyExcept(0, 1), 3L);
        assertEquals(threeElements.multiplyExcept(0, 2), 2L);
        assertEquals(fourElements.multiplyExcept(0, 2), 8L);
        assertEquals(fourElements.multiplyExcept(0, 3), 6L);
        assertEquals(fourElements.multiplyExcept(1, 3), 3L);
        assertEquals(fourElements.multiplyExcept(1, 2), 4L);
    }

    public void testExceptFewIndexes() throws Exception {
        MultiplyExcept tenElements = new MultiplyExcept(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9});
        assertEquals(tenElements.multiplyExcept(0, 1, 2), 4L * 5 * 6 * 7 * 8 * 9);
        assertEquals(tenElements.multiplyExcept(1, 0, 2), 4L * 5 * 6 * 7 * 8 * 9);
        assertEquals(tenElements.multiplyExcept(2, 1, 0), 4L * 5 * 6 * 7 * 8 * 9);

        assertEquals(tenElements.multiplyExcept(0, 4, 8), 2L * 3 * 4 * 6 * 7 * 8);
        assertEquals(tenElements.multiplyExcept(8, 0, 4), 2L * 3 * 4 * 6 * 7 * 8);
        assertEquals(tenElements.multiplyExcept(2, 3, 4), 1L * 2 * 6 * 7 * 8 * 9);
    }
}