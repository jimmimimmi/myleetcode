package com.gridnev.sortings;

import com.gridnev.TestUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;

import static org.testng.Assert.assertEquals;

public class CountSortTest {

    @DataProvider
    public static Object[][] Strings() {
        return new Object[][]{
                {TestUtils.randomAlphanumeric(10)},
                {TestUtils.randomAlphanumeric(10)},
                {TestUtils.randomAlphanumeric(10)},
                {TestUtils.randomAlphanumeric(10)},
                {TestUtils.randomAlphanumeric(10)}
        };
    }

    @Test(dataProvider = "Strings")
    public void testSort(String s) throws Exception {

        char[] initialArray = s.toCharArray();
        Arrays.sort(initialArray);
        String expectedResult = String.valueOf(initialArray);
        assertEquals(expectedResult, CountSort.sort(s));
    }
}