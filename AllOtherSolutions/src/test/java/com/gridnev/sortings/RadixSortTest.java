package com.gridnev.sortings;

import com.gridnev.TestUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;

import static org.testng.Assert.assertTrue;

public class RadixSortTest {

    @DataProvider
    public static Object[][] arrays() {
        return new Object[][]{
                {TestUtils.randomArray(50, 10)},
                {TestUtils.randomArray(50, 10)},
                {TestUtils.randomArray(50, 10)}
        };
    }

    @Test(dataProvider = "arrays")
    public void testSort(int[] array) throws Exception {
        int[] copy = Arrays.copyOf(array, array.length);
        Arrays.sort(copy);

        int[] sortedArray = RadixSort.sort(array);

        assertTrue(TestUtils.arraysEquivalent(sortedArray, copy));
    }
}