package com.gridnev.wordbreak;

import org.testng.annotations.Test;

import static java.util.Arrays.asList;
import static junit.framework.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class WordBreakBruteForceTest {

    @Test
    public void testWordBreak() throws Exception {
        assertTrue(WordBreakBruteForce.wordBreak("leetcode", asList("leet", "code")));
        assertTrue(WordBreakBruteForce.wordBreak("leetcodecode", asList("leet", "code")));
        assertFalse(WordBreakBruteForce.wordBreak("leetcodeleecode", asList("leet", "code")));
        assertTrue(WordBreakBruteForce.wordBreak("leetcodeleecode", asList("leet", "code","lee")));

        assertTrue(WordBreakBruteForceWithMemoization.wordBreak("leetcode", asList("leet", "code")));
        assertTrue(WordBreakBruteForceWithMemoization.wordBreak("leetcodecode", asList("leet", "code")));
        assertFalse(WordBreakBruteForceWithMemoization.wordBreak("leetcodeleecode", asList("leet", "code")));
        assertTrue(WordBreakBruteForceWithMemoization.wordBreak("leetcodeleecode", asList("leet", "code","lee")));

        assertTrue(WordBreakBFS.wordBreak("leetcode", asList("leet", "code")));
        assertTrue(WordBreakBFS.wordBreak("leetcodecode", asList("leet", "code")));
        assertFalse(WordBreakBFS.wordBreak("leetcodeleecode", asList("leet", "code")));
        assertTrue(WordBreakBFS.wordBreak("leetcodeleecode", asList("leet", "code","lee")));

        assertTrue(WordBreakDP.wordBreak("leetcode", asList("leet", "code")));
        assertTrue(WordBreakDP.wordBreak("leetcodecode", asList("leet", "code")));
        assertFalse(WordBreakDP.wordBreak("leetcodeleecode", asList("leet", "code")));
        assertTrue(WordBreakDP.wordBreak("leetcodeleecode", asList("leet", "code","lee")));
    }
}