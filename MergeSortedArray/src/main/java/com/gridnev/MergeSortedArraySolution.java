package com.gridnev;

/**
 * Hello world!
 * https://leetcode.com/problems/merge-sorted-array/
 */
public class MergeSortedArraySolution {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int index1 = m - 1;
        int index2 = n - 1;
        int resultIndex = m + n - 1;

        while (index1 >= 0 && index2 >= 0) {

            if (nums1[index1] == nums2[index2]) {
                nums1[resultIndex] = nums1[index1];
                resultIndex--;
                nums1[resultIndex] = nums1[index1];
                resultIndex--;
                index1--;
                index2--;
                continue;
            }

            if (nums1[index1] > nums2[index2]) {
                nums1[resultIndex] = nums1[index1];
                resultIndex--;
                index1--;
                continue;
            }

            if (nums1[index1] < nums2[index2]) {
                nums1[resultIndex] = nums2[index2];
                resultIndex--;
                index2--;
                continue;
            }
        }

        for (; index1 >= 0; index1--, resultIndex--) {
            nums1[resultIndex] = nums1[index1];
        }

        for (; index2 >= 0; index2--, resultIndex--) {
            nums1[resultIndex] = nums2[index2];
        }
    }

    public void mergeWihUsingExtraSpace(int[] nums1, int m, int[] nums2, int n) {


        final int[] result = new int[m + n];

        int index1 = 0;
        int index2 = 0;
        int resultIndex = 0;

        while (index1 < m && index2 < n) {
            if (nums1[index1] == nums2[index2]) {
                result[resultIndex] = nums1[index1];
                resultIndex++;
                result[resultIndex] = nums1[index1];
                resultIndex++;
                index1++;
                index2++;
                continue;
            }

            if (nums1[index1] < nums2[index2]) {
                result[resultIndex] = nums1[index1];
                resultIndex++;
                index1++;
                continue;
            }

            if (nums1[index1] > nums2[index2]) {
                result[resultIndex] = nums2[index2];
                resultIndex++;
                index2++;
                continue;
            }
        }


        for (; index1 < m; index1++, resultIndex++) {
            result[resultIndex] = nums1[index1];
        }

        for (; index2 < n; index2++, resultIndex++) {
            result[resultIndex] = nums2[index2];
        }

        for (int i = 0; i < result.length; i++) {
            nums1[i] = result[i];
        }
    }
}
