package com.gridnev;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Unit test for simple MergeSortedArraySolution.
 */
public class MergeSortedArraySolutionTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public MergeSortedArraySolutionTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(MergeSortedArraySolutionTest.class);
    }

    public void testFirstArrayIsEmpty() {

        final MergeSortedArraySolution mergeSortedArraySolution = new MergeSortedArraySolution();

        final int[] nums1 = {1, 3, 5};
        final int[] nums2 = {2, 4, 6};
        mergeSortedArraySolution.merge(nums1, 0, nums2, 3);

        assertThat(nums1, is(new int[]{2, 4, 6}));
    }

    public void testSecondArrayIsEmpty() {

        final MergeSortedArraySolution mergeSortedArraySolution = new MergeSortedArraySolution();

        final int[] nums1 = {1, 3, 5};
        final int[] nums2 = {2, 4, 6};
        mergeSortedArraySolution.merge(nums1, 3, nums2, 0);

        assertThat(nums1, is(new int[]{1, 3, 5}));
    }

    public void testGeneralCase() {

        final MergeSortedArraySolution mergeSortedArraySolution = new MergeSortedArraySolution();

        final int[] nums1 = {1, 3, 5, 6, 7, 8, 0, 0, 0};
        final int[] nums2 = {2, 4, 6};
        mergeSortedArraySolution.merge(nums1, 6, nums2, 3);

        assertThat(nums1, is(new int[]{1, 2, 3, 4, 5, 6, 6, 7, 8}));
    }


    public void testGeneralCase2() {

        final MergeSortedArraySolution mergeSortedArraySolution = new MergeSortedArraySolution();

        final int[] nums1 = {1, 3, 5, 6, 7, 8, 0, 0, 0, 0};
        final int[] nums2 = {2, 4, 5, 5};
        mergeSortedArraySolution.merge(nums1, 6, nums2, 4);

        assertThat(nums1, is(new int[]{1, 2, 3, 4, 5, 5, 5, 6, 7, 8}));
    }
}
