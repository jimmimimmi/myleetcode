package com.gridnev;

/**
 * Hello world!
 */
public class MoveZeroesSolution {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }


    public void moveZeroes(int[] nums) {

        if (nums.length == 0) {
            return;
        }

        int zeroPointerIndex = 0;


        for (zeroPointerIndex = 0; zeroPointerIndex < nums.length; zeroPointerIndex++) {
            if (nums[zeroPointerIndex] == 0) {
                break;
            }
        }

        if (zeroPointerIndex >= nums.length - 1) {
            return;
        }

        int nonZeroPointerIndex = 0;

        for (nonZeroPointerIndex = zeroPointerIndex + 1; nonZeroPointerIndex < nums.length; nonZeroPointerIndex++) {
            if (nums[nonZeroPointerIndex] != 0) {
                break;
            }
        }

        if (zeroPointerIndex >= nums.length) {
            return;
        }

        while (nonZeroPointerIndex < nums.length) {
            int nonZeroTemp = nums[nonZeroPointerIndex];
            nums[nonZeroPointerIndex] =  nums[zeroPointerIndex];
            nums[zeroPointerIndex] = nonZeroTemp;




            for (; zeroPointerIndex < nums.length; zeroPointerIndex++) {
                if (nums[zeroPointerIndex] == 0) {
                    break;
                }
            }


            for (; nonZeroPointerIndex < nums.length; nonZeroPointerIndex++) {
                if (nums[nonZeroPointerIndex] != 0) {
                    break;
                }
            }
        }
    }
}
