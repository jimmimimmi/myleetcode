package com.gridnev;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Unit test for simple MoveZeroesSolution.
 */
public class MoveZeroesSolutionTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public MoveZeroesSolutionTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(MoveZeroesSolutionTest.class);
    }

    public void testEmpty() {
        final MoveZeroesSolution moveZeroesSolution = new MoveZeroesSolution();

        final int[] nums = {};
        moveZeroesSolution.moveZeroes(nums);

        assertEquals(0, nums.length);
    }

    public void testNonZero() {
        final MoveZeroesSolution moveZeroesSolution = new MoveZeroesSolution();

        final int[] nums = {1, 2, 3};
        moveZeroesSolution.moveZeroes(nums);

        assertEquals(3, nums.length);
        assertEquals(1, nums[0]);
        assertEquals(2, nums[1]);
        assertEquals(3, nums[2]);
    }

    public void testZeroes() {
        final MoveZeroesSolution moveZeroesSolution = new MoveZeroesSolution();

        final int[] nums = {0, 0, 0};
        moveZeroesSolution.moveZeroes(nums);

        assertEquals(3, nums.length);
        assertEquals(0, nums[0]);
        assertEquals(0, nums[1]);
        assertEquals(0, nums[2]);
    }

    public void testTheLastZero() {
        final MoveZeroesSolution moveZeroesSolution = new MoveZeroesSolution();

        final int[] nums = {1, 2, 3, 0};
        moveZeroesSolution.moveZeroes(nums);

        assertEquals(4, nums.length);

        assertEquals(1, nums[0]);
        assertEquals(2, nums[1]);
        assertEquals(3, nums[2]);
        assertEquals(0, nums[3]);

        assertThat(nums, is(new int[]{1, 2, 3, 0}));
    }

    public void testTheFirstZero() {
        final MoveZeroesSolution moveZeroesSolution = new MoveZeroesSolution();

        final int[] nums = {0, 1, 2, 3, 0};
        moveZeroesSolution.moveZeroes(nums);

        assertThat(nums, is(new int[]{1, 2, 3, 0, 0}));
    }

    public void testGeneralCase() {
        final MoveZeroesSolution moveZeroesSolution = new MoveZeroesSolution();

        final int[] nums = {0, 1, 0, 0, 0, 2, 3, 0, 4, 5, 6, 0, 7, 0};
        moveZeroesSolution.moveZeroes(nums);

        assertThat(nums, is(new int[]{1, 2, 3, 4, 5, 6, 7, 0, 0, 0, 0, 0, 0, 0}));
    }

}
