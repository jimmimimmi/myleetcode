package com.gridnev;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Hello world!
 * https://leetcode.com/problems/pascals-triangle-ii/
 */
public class PascalTriangleIISolution {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }


    public List<Integer> getRow(int rowIndex) {
        rowIndex++;
        final List<Integer> lists = new ArrayList<Integer>();
        if (rowIndex <= 0) {
            return   new ArrayList<Integer>();
        }

        lists.add(1);

        if (rowIndex == 1) {
            return Arrays.asList(1);
        }

        if (rowIndex == 2) {
            return Arrays.asList(1, 1);
        }

        List<Integer> lastList = Arrays.asList(1, 1);

        for (int i = 3; i <= rowIndex; i++) {
            int lengthOfNewList = i;
            int amountOfCalculatableCells = lengthOfNewList - 2;

            final List<Integer> currentRowList = new ArrayList<Integer>();
            currentRowList.add(1);

            for (int j = 1; j <= amountOfCalculatableCells; j++) {
                currentRowList.add(lastList.get(j - 1) + lastList.get(j));
            }

            currentRowList.add(1);

            lastList = currentRowList;
        }

        return lastList;
    }
}
