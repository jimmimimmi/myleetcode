package com.gridnev;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.List;

/**
 * Unit test for simple PascalTriangleIISolution.
 */
public class PascalTriangleIISolutionTest
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public PascalTriangleIISolutionTest(String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( PascalTriangleIISolutionTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }

    public void test1(){
        final List<Integer> generate = new PascalTriangleIISolution().getRow(3);

        assertEquals(1, (int)generate.get(0));
        assertEquals(3, (int)generate.get(1));
        assertEquals(3, (int)generate.get(2));
        assertEquals(1, (int)generate.get(3));
    }
}
