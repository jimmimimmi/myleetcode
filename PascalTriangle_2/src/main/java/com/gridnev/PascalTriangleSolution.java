package com.gridnev;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Hello world!
 */
public class PascalTriangleSolution {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

    public List<List<Integer>> generate(int numRows) {
        final ArrayList<List<Integer>> lists = new ArrayList<List<Integer>>();
        if (numRows <= 0) {
            return lists;
        }

        lists.add(Arrays.asList(1));

        if (numRows == 1) {
            return lists;
        }

        lists.add(Arrays.asList(1, 1));


        if (numRows == 2) {
            return lists;
        }
        List<Integer> lastList = lists.get(1);

        for (int i = 3; i <= numRows; i++) {
            int lengthOfNewList = i;
            int amountOfCalculatableCells = lengthOfNewList - 2;

            final List<Integer> currentRowList = new ArrayList<Integer>();
            currentRowList.add(1);

            for (int j = 1; j <= amountOfCalculatableCells; j++) {
                currentRowList.add(lastList.get(j - 1) + lastList.get(j));
            }

            currentRowList.add(1);

            lists.add(currentRowList);
            lastList = currentRowList;
        }

        return lists;
    }
}
