package com.gridnev;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.List;

/**
 * Unit test for simple PascalTriangleSolution.
 */
public class PascalTriangleSolutionTest
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public PascalTriangleSolutionTest(String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( PascalTriangleSolutionTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }

    public void testEmpty(){
        final List<List<Integer>> generate = new PascalTriangleSolution().generate(0);

        assertEquals(0, generate.size());
    }

    public void test1(){
        final List<List<Integer>> generate = new PascalTriangleSolution().generate(1);

        assertEquals(1, generate.size());
        assertEquals(1, generate.get(0).size());
        assertEquals(1, (int)generate.get(0).get(0));
    }

    public void test2(){
        final List<List<Integer>> generate = new PascalTriangleSolution().generate(2);

        assertEquals(2, generate.size());

        assertEquals(1, generate.get(0).size());
        assertEquals(2, generate.get(1).size());

        assertEquals(1, (int)generate.get(0).get(0));
        assertEquals(1, (int)generate.get(1).get(0));
        assertEquals(1, (int)generate.get(1).get(1));

    }


    public void test3(){
        final List<List<Integer>> generate = new PascalTriangleSolution().generate(3);

        assertEquals(3, generate.size());

        assertEquals(1, generate.get(0).size());
        assertEquals(2, generate.get(1).size());
        assertEquals(3, generate.get(2).size());

        assertEquals(1, (int)generate.get(0).get(0));
        assertEquals(1, (int)generate.get(1).get(0));
        assertEquals(1, (int)generate.get(1).get(1));

        assertEquals(1, (int)generate.get(2).get(0));
        assertEquals(2, (int)generate.get(2).get(1));
        assertEquals(1, (int)generate.get(2).get(2));

    }
}
