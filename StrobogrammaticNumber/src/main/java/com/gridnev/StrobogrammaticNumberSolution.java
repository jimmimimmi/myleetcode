package com.gridnev;

import java.util.HashMap;

/**
 * Hello world!
 * https://leetcode.com/problems/strobogrammatic-number/
 */
public class StrobogrammaticNumberSolution {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }


    private HashMap<Character, Character> validTransformations = new HashMap<Character, Character>();

    {
        validTransformations.put('1', '1');
        validTransformations.put('0', '0');
        validTransformations.put('8', '8');
        validTransformations.put('6', '9');
        validTransformations.put('9', '6');
    }

    public boolean isStrobogrammatic(String num) {
        int firstIndex = 0;
        int lastIndex = num.length() - 1;

        while (firstIndex <= lastIndex) {
            final char firstChar = num.charAt(firstIndex);
            final char lastChar = num.charAt(lastIndex);
            if (!validTransformations.containsKey(firstChar) || !validTransformations.containsKey(lastChar)) {
                return false;
            }

            if (validTransformations.get(firstChar) != lastChar) {
                return false;
            }
            firstIndex++;
            lastIndex--;
        }

        return true;
    }
}
