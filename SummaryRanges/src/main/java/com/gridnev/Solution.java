package com.gridnev;

import java.util.*;

/**
 * https://leetcode.com/problems/summary-ranges/
 */
public class Solution {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

    private void add(List<String> array, int value1, int value2) {

        final String arrow = "->";

        if (value1 == value2) {
            array.add("" + value1);
        } else {
            array.add(value1 + arrow + value2);
        }
    }

    public List<String> summaryRanges(int[] nums) {

        if (nums.length == 0) {
            return Arrays.asList(new String[]{});
        }

        if (nums.length == 1) {
            return Arrays.asList("" + nums[0]);
        }

        ArrayList<String> result = new ArrayList<String>();

        int firstChar = nums[0];
        int secondChar = nums[0];

        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == nums[i - 1] + 1) {
                secondChar = nums[i];
                continue;
            }
            add(result, firstChar, secondChar);

            firstChar = secondChar = nums[i];
        }

        final int lastIndex = nums.length - 1;
        if (nums[lastIndex] == nums[nums.length - 1 - 1] + 1) {
            secondChar = nums[lastIndex];

        }
        add(result, firstChar, secondChar);

        return result;
    }
}
