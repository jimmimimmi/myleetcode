package com.gridnev;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.List;

/**
 * Unit test for simple Solution.
 */
public class SolutionTest
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public SolutionTest(String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( SolutionTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }


    public void testSingleElement(){
        Solution solution = new Solution();
        List<String> strings = solution.summaryRanges(new int[]{0});

        assertEquals("0", strings.get(0));
        assertEquals(1, strings.size());
    }

    public void test1(){
        Solution solution = new Solution();
        List<String> strings = solution.summaryRanges(new int[]{0, 1, 2, 3, 4});

        assertEquals("0->4", strings.get(0));
        assertEquals(1, strings.size());
    }

    public void test2(){
        Solution solution = new Solution();
        List<String> strings = solution.summaryRanges(new int[]{0, 2, 4, 5, 7});

        assertEquals(4, strings.size());
        assertEquals("0", strings.get(0));
        assertEquals("2", strings.get(1));
        assertEquals("4->5", strings.get(2));
        assertEquals("7", strings.get(3));

    }
}
