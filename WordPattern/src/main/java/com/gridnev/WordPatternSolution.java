package com.gridnev;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Hello world!
 * https://leetcode.com/problems/word-pattern/
 */
public class WordPatternSolution {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

    public boolean wordPattern(String pattern, String str) {

        final HashMap<Character, String> hashMap = new HashMap<Character, String>();
        final HashSet<String> mappedWords = new HashSet<String>();

        final String[] words = str.split("\\s+");
        if (words.length != pattern.length()) {
            return false;
        }

        if (words.length == 0) {
            return true;
        }

        for (int i = 0; i < pattern.length(); i++) {
            final char symbolInPattern = pattern.charAt(i);

            final String word = words[i];
            if (hashMap.containsKey(symbolInPattern)) {
                final String correspondedWordForThisSymbol = hashMap.get(symbolInPattern);
                if (!correspondedWordForThisSymbol.equals(word)) {
                    return false;
                }
            } else {

                if(mappedWords.contains(word)){
                    return false;
                }
                hashMap.put(symbolInPattern, word);
                mappedWords.add(word);
            }
        }

        return true;

    }
}
