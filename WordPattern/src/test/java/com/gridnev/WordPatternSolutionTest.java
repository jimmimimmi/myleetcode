package com.gridnev;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple WordPatternSolution.
 */
public class WordPatternSolutionTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public WordPatternSolutionTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(WordPatternSolutionTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp() {
        assertTrue(true);
    }

    public void test1() {
        final WordPatternSolution wordPatternSolution = new WordPatternSolution();

        assertTrue(wordPatternSolution.wordPattern("abba", "dog cat cat dog"));
        assertFalse(wordPatternSolution.wordPattern("abba", "dog cat cat fish"));
        assertFalse(wordPatternSolution.wordPattern("aaaa", "dog cat cat dog"));
        assertFalse(wordPatternSolution.wordPattern("abba", "dog dog dog dog"));
    }
}
